<header class="header">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52414425-1', 'auto');
  ga('send', 'pageview');

</script>
    <a class="logo header-logo" href="<?php echo Yii::app()->homeUrl ?>"></a>

    <h1 class="title">
        <span class="title__fline"><img class="logo-title" src="/i/spartak-logo.png" alt="Spartak"></span>
        <span class="title__sline">Tickets</span>
    </h1>

    <ul class="vcard" itemscope itemtype="http://schema.org/Organization">
        <li class="vcard__item">
            <p class="text white-text">Билетный отдел:</p>
            <a class="link" itemprop="email" href="mailto:tickets@spartak.com ">tickets@spartak.com</a>
        </li>

        <li class="vcard__item">
            <p class="text">тел.:&nbsp;<span itemprop="telephone">+7-495-111-1922</span>;</p>
            <p class="text">по будням, с 9-00 до 20-00</p>
        </li>
    </ul>
</header>