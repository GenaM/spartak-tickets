<?php
    class ErrorController extends Controller {

        public $layout = '//layouts/error';

        public $loadBasket = false;

        public function actionIndex()   {
            $error = Yii::app()->errorHandler->error;
            if( ! $error['code'] || $error['code'] == 400 ) {
                $error['code'] = 404;
            }
            if( Yii::app()->request->isAjaxRequest )    {
                echo CJSON::encode( array( 'status' => $error['code'] , 'redirect' => Yii::app()->createUrl( 'error/index' ) ) );
            }
            $this->errorCode = $error['code'];
            $this->render( 'error' . $error['code'] );
        }

        public function actionUser() {
            unset( $this->session );
            Yii::app()->user->logout();
            $this->render('user');
        }

    }