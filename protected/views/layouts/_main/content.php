<div class="content clearfix">
    <ul class="step-menu">
        <li class="step-menu__item <?php echo $this->action->id == 'stadium' || $this->action->id == 'sector' ? 'step-menu__item_state_active' : null ?>">
            <span class="step-menu__title">Билетная система</span>
        </li>
        <li class="step-menu__item <?php echo $this->action->id == 'login' ? 'step-menu__item_state_active' : null ?>">
            <span class="step-menu__title">Регистрация</span>
        </li>
        <li class="step-menu__item <?php echo $this->action->id == 'reservation' ? 'step-menu__item_state_active' : null ?>">
            <span class="step-menu__title">Бронирование</span>
        </li>
        <li class="step-menu__item <?php echo $this->action->id == 'order' ? 'step-menu__item_state_active' : null ?>">
            <span class="step-menu__title">Оформление покупки</span>
        </li>
        <li class="step-menu__item <?php echo $this->action->id == 'result' ? 'step-menu__item_state_active' : null ?>">
            <span class="step-menu__title">Получение билета</span>
        </li>
    </ul>
    <?php echo $content ?>
    <?php $this->renderPartial( '//layouts/_main/sidebar' ); ?>
</div>