<!DOCTYPE html>
<html class="page error-page error-<?php echo $this->errorCode ?>">
    <head>
        <title>Spartak Tickets | Страница не найдена</title>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

        <!-- Проектные стили -->
        <link href="/css/main.css" rel="stylesheet" />
    </head>

    <body class="page__body">
        <?php echo $content ?>
    </body>

</html>