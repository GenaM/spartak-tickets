<?php if( $model ) { ?>
    <?php
    /**
     * @var $form CActiveForm
     */
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'update-event-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>

    <div class="body">
        <div class="row form-group">
            <label>Название события: <?php echo $model->name ?></label>
        </div>

        <div class="row form-group">
            <label>Дата начала: <?php echo $model->start_time ?></label>
        </div>

        <div class="row form-group">
            <label>Терминал</label>
            <?php echo $form->textField($model, 'rapida_terminal', array('class' => 'form-control col-md-6')) ?>
        </div>

        <div class="row form-group">
            <label>Ссылка на внешний ресурс</label>
            <?php echo $form->textField($model, 'url', array('class' => 'form-control col-md-6')) ?>
        </div>

        <div class="row loaded image big form-group pull-left col-md-6" style="clear: both">
            <label>Большой баннер</label>
            <img style="width: 100px; height: 100px;" src="/uploads/<?php echo $model->id ?>/<?php echo $model->big_banner ?>" alt="" />

            <?php
            if( ! $model->getIsNewRecord() ) {
                $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                    'id'=>'uploadBFile',
                    'config'=>array(
                        'action'=>Yii::app()->createUrl('spartAdmin/uploadImage', array('id' => $model->id, 'big' => 1)),
                        'allowedExtensions'=>array("jpg","png","gif"),
                        'sizeLimit'=>10*1024*1024,
                        'minSizeLimit'=>1*1024,
                        'onComplete'=>"js:function(id, filename,r){
                        $('.loaded.image.big').find('img').attr('src', r.path);
                    }",
                    )
                ));
            }
            ?>
        </div>

        <div class="row loaded image little form-group col-md-6">
            <label>Маленький баннер</label>
            <img style="width: 100px; height: 100px;" src="/uploads/<?php echo $model->id ?>/<?php echo $model->little_banner ?>" alt="" />

            <?php
            if( ! $model->getIsNewRecord() ) {
                $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                    'id'=>'uploadLFile',
                    'config'=>array(
                        'action'=>Yii::app()->createUrl('spartAdmin/uploadImage', array('id' => $model->id, 'big' => 0)),
                        'allowedExtensions'=>array("jpg","png","gif"),
                        'sizeLimit'=>10*1024*1024,
                        'minSizeLimit'=>1*1024,
                        'onComplete'=>"js:function(id, filename,r){
                        $('.loaded.image.little').find('img').attr('src', r.path);
                    }",
                    )
                ));
            }
            ?>
        </div>


        <div class="row form-group">
            <label>Использовать большой баннер</label>
            <?php echo $form->checkBox($model, 'use_big_banner') ?>
        </div>

        <div class="row form-group">
            <label>Концерт?</label>
            <?php echo $form->checkBox($model, 'is_concert') ?>
        </div>

        <div class="row form-group">
            <label>Показать на главной?</label>
            <?php echo $form->checkBox($model, 'is_active') ?>
        </div>
    </div>

    <button class="btn" type="submit">Сохранить</button>

    <?php $this->endWidget(); ?>
<?php } ?>