<section class="error-info">
    <span class="error-info__title">Ошибка</span>

    <p class="error-info__text">
        <span class="error-info__1line">Страница</span>
        <span class="error-info__2line">не найдена</span>
        <span class="error-info__3line">Попробуйте начать с</span>
        <a class="error-info__4line link" href="<?php echo Yii::app()->homeUrl ?>">главной страницы</a>
    </p>
</section>