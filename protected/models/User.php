<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $email
 * @property string $crm_id
 * @property string $phone
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $card_number
 * @property string $full_info
 *
 * The followings are the available model relations:
 * @property Basket[] $baskets
 * @property Order[] $orders
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('crm_id, email', 'required'),
			array('id, crm_id, email, last_name, first_name, middle_name, phone, card_number', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'baskets' => array(self::HAS_MANY, 'Basket', 'user_id'),
			'orders' => array(self::HAS_MANY, 'Order', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'crm_id' => 'Crm ID',
			'email' => 'Email',
			'phone' => 'Phone',
			'last_name' => 'last_name',
			'first_name' => 'first_name',
			'middle_name' => 'middle_name',
			'full_info' => 'full Info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('crm_id',$this->crm_id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('full_info',$this->fullInfo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeSave() {
        $this->full_info = serialize( $this->full_info );
        return parent::beforeSave();
    }

    public function afterFind() {
        $this->full_info = unserialize( $this->full_info );
        return parent::afterFind();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     *  check user in DB and save it. rewrite email.
     * @param $attributes
     * @return bool|CActiveRecord|User
     */
    public static function addSpartakUser( $attributes )    {
        $params = array();

        if( ! empty( $attributes['mail'] ) ) {
            $params['email'] = $attributes['mail'];
        }
        if( ! empty( $attributes['customerId'] ) ) {
            $params['crm_id'] = $attributes['customerId'];
        }
        if( ! empty( $attributes['phone'] ) ) {
            $params['phone'] = $attributes['phone'];
        }
        if( ! empty( $attributes['lastName'] ) ) {
            $params['last_name'] = $attributes['lastName'];
        }
        if( ! empty( $attributes['firstName'] ) ) {
            $params['first_name'] = $attributes['firstName'];
        }
        if( ! empty( $attributes['middleName'] ) ) {
            $params['middle_name'] = $attributes['middleName'];
        }
        if( ! empty( $attributes['cardNumber'] ) ) {
            $params['card_number'] = $attributes['cardNumber'];
        }
        //if( ! empty( $attributes['fullInfo'] ) ) {
        //    $params['full_info'] = $attributes['fullInfo'];
        //}

        if( empty( $params['crm_id'] )  /*|| !  $params['email']*/ )  {
            return false;
        }

        if( ! $user = User::model()->findByAttributes( array( 'crm_id' => $params['crm_id'] ) ) )    {
            $user = new User();
        }
        $user->attributes = $params;
        if( $user->save() )  {
            return $user;
        }
        return false;
	}
}
