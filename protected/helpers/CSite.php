<?php
    class CSite {

        /**
         * @param array $items
         * @param null $field
         * @param int $sum
         * @return int
         */
        public static function getTotalSum( array $items , $field = null , $sum = 0 ) {
            foreach( $items as $item )  {
                if( $field )    {
                    $sum += $item[ $field ];
                } else {
                    $sum ++;
                }
            }
            return $sum;
        }

        /**
         * @param $number
         * @param array $strings
         * @return string
         */
        public static function getCorrectStr( $number , array $strings )   {
            $val = $number % 100;
            if ($val > 10 && $val < 20) {
                return $strings[3];
            } else {
                $val = $number % 10;
                if ($val == 1) {
                    return $strings[1];
                } elseif ($val > 1 && $val < 5) {
                    return $strings[2];
                } else {
                    return $strings[3];
                }
            }
        }

        /**
         * @param $to
         * @param $body
         * @param string $subject
         * @param array  $attachments
         * @return bool
         */
        public static function sendMail($to, $body, $subject = '', array $attachments = array() ) {
            $message = new YiiMailMessage;
            $message->subject = $subject;
            $message->setBody($body, 'text/html');
            if( is_array( $to ) ) {
                $message->addTo( $to[0] );
                unset( $to[0] );
                foreach($to as $address) {
                    $message->AddBCC( $address );
                }
            } else {
                $message->addTo($to);
            }
            $message->from = Yii::app()->params['noreplyEmail'];
            if( $attachments )  {
                foreach( $attachments as $attachment )  { /* @var Swift_Attachment $attachment */
                    $message->attach( $attachment , $attachment->getContentType() );
                }
            }
            return (bool) Yii::app()->mail->send($message);

        }

        /**
         * @param $htmls
         * @param $fileName
         * @param int $output
         * @param array $options
         * @return string
         */
        public static function  generatePDF( $htmls , $fileName , $output = 0, $options = array())    {
            $path = self::getTempDir() . $fileName . '.pdf';
            $pdf = new WkHtmlToPdf(array('tmp'=>self::getTempDir(), 'enableEscaping' => false));
            if( $options )  {
                $pdf->setOptions( $options );
            }
            if( ! is_array( $htmls ) ) {
                $htmls = array( $htmls );
            }
            foreach( $htmls as $html ) {
                $pdf->addPage( $html );
            }
            if( $output )   {
                $pdf->send( $fileName . '.pdf' );
                var_dump($pdf->getError());
                Yii::app()->end();
            }
            $pdf->saveAs( $path );
            return $path;
        }

        /**
         * @param $barcode
         * @param int $angle
         * @param int $width
         * @param int $height
         * @param int $fontSize
         * @return mixed
         */
        public static function generateBarcode( $barcode , $angle = 90 , $width = 100 , $height = 400 , $fontSize = 14 ) {
            if( ! $barcode )   {
                return false;
            }

            $fileName = $barcode;

            $path = self::getTempDir() . $fileName . '.gif';

            $x = $width - $fontSize - 10;
            $y = $height/2;
            $font = Yii::app()->basePath . '/../web/css/fonts/pfsynchpro-regular-webfont.ttf';

            $im     = imagecreatetruecolor( $width , $height );

            imagefilledrectangle($im, 0, 0, $width, $height, ImageColorAllocate($im,0xff,0xff,0xff) );

            $data = Barcode::gd($im, ImageColorAllocate($im,0x00,0x00,0x00), $x, $y, $angle, 'int25', array( 'code' => $barcode, 'crc' => false ) , 2.6 , $width );

            $box = imagettfbbox( $fontSize , 0 , $font , $barcode );
            $len = $box[2] - $box[0];

            Barcode::rotate( -$len/2 , ( $data['height']/2 ) + $fontSize , $angle , $xt , $yt );

            imagettftext( $im , $fontSize , 360- $angle , 4 , $y * 0.5 , ImageColorAllocate($im,0x00,0x00,0x00) , $font , $barcode );
            imagepng( $im , $path );
            imagedestroy($im);

            return $fileName;
        }

        public static function generateQRcode($data) {
            $fileName = 'qr_' . $data . '.png';

            Yii::app()->controller->widget('application.extensions.qrcode.QRCodeGenerator',array(
                'data' => $data,
                'filename' => $fileName,
                'filePath' => CSite::getTempDir(),
                'subfolderVar' => false,
                'displayImage'=>false, // default to true, if set to false display a URL path
                'errorCorrectionLevel'=>'L', // available parameter is L,M,Q,H
                'matrixPointSize'=>8, // 1 to 10 only
            ));

            return $fileName;
        }

        /**
         * @return string
         */
        public static function getTempDir() {
            $directory = Yii::app()->runtimePath . '/temp/';
            if ( ! file_exists( $directory ) ) {
                mkdir( $directory );
            }
            return $directory;
        }

        /**
         * @return string
         */
        public static function getTempDirForView()  {
            return '/protected/runtime/temp/';
        }

        /**
         * @param string $format
         * @param integer $time
         * @return bool|string
         */
        public static function getNow( $time = 0 , $format = 'Y-m-d H:i:s' ) {
            return date( $format , time() + $time );
        }

        public static function getDatesDiff($d1, $d2 = null) {
            $date1 = strtotime($d1);
            if($d2) {
                $date2 = strtotime($d2);
            } else {
                $date2 = time();
            }
            return $date2 - $date1;
        }
    }