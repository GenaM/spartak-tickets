<?php
/**
 * This is the model class for table "sector".
 *
 * The followings are the available columns in table 'basket':
 * @property integer $id
 * @property integer $stadium_id
 * @property integer $event_id
 * @property integer $sector_id
 * @property string $name
 * @property string $description
 * @property integer $capacity
 * @property string $tribune
 * @property integer $upper
 * @property integer $free_seats
 * @property integer $available
 * @property integer $scheme_saved
 *
 * The followings are the available model relations:
 * @property User $user
 * @property BasketOrder[] $basketOrders
*/

class Sector extends CActiveRecord  {

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sector';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('stadium_id, event_id, sector_id, name, available', 'required'),
            array('stadium_id, event_id, sector_id,', 'numerical', 'integerOnly'=>true),
            array('id, primaryKey, stadium_id, event_id, sector_id, name, description, capacity, tribune, upper, free_seats, available, scheme_saved', 'safe')
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Basket the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}