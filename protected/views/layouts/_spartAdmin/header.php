<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('spartAdmin/index') ?>"><?php echo Yii::app()->name ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Заказы<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li title="пользователь перешел на страницу оплаты "><a href="<?php echo Yii::app()->createUrl('spartAdmin/viewOrders', array('status' => 1)) ?>"><span>В оплате</span></a></li>
                        <li title="'проходящий' статус"><a href="<?php echo Yii::app()->createUrl('spartAdmin/viewOrders', array('status' => 2)) ?>"><span>Оплата подтверждена</span></a></li>
                        <li title="пользователь не стал оплачивать, а отменил на странице рапиды или просто не успел за отведенное время"><a href="<?php echo Yii::app()->createUrl('spartAdmin/viewOrders', array('status' => 5)) ?>"><span>Вышло время на оплату</span></a></li>
                        <li title="заказы, по которым были отправлены билеты"><a href="<?php echo Yii::app()->createUrl('spartAdmin/viewOrders', array('status' => 4)) ?>"><span>Успешно. Переданные к отправке.</span></a></li>
                        <li title="Всевозможные ошибки (1, 15, 122 ...) для методов ReservationPayed2 и SellReservation"><a href="<?php echo Yii::app()->createUrl('spartAdmin/viewOrders', array('status' => 101)) ?>"><span>TicketSoft: ошибки или нет ответа</span></a></li>
                        <li title="Не все оплаченные билеты были отправлены"><a href="<?php echo Yii::app()->createUrl('spartAdmin/viewOrdersUnFull', array('status' => 4)) ?>"><span>TicketSoft удержал часть заказа</span></a></li>
                    </ul>
                </li>
                <li title="Список ссылок на страницы редактирования информации о матчах"><a href="<?php echo Yii::app()->createUrl('spartAdmin/index') ?>"><span>События</span></a></li>
            </ul>
        </div>
    </div>
</nav>