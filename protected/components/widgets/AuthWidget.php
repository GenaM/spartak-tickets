<?php
    class AuthWidget extends CWidget    {

        public function run()   {
            mb_internal_encoding("UTF-8");
            $this->render( 'authWidget' , array( 'model' => new LoginForm ) );
        }

    }