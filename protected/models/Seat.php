<?php
/**
 * This is the model class for table "seat".
 *
 * The followings are the available columns in table 'basket':
 * @property integer $id
 * @property integer $stadium_id
 * @property integer $event_id
 * @property integer $sector_id
 * @property integer $seat_id
 * @property string $row_text
 * @property string $seat_text
 * @property integer $x
 * @property integer $y
 * @property integer $width
 * @property integer $height
 * @property integer $zone_id
 * @property integer $status
 *
 * The followings are the available model relations:
*/

class Seat extends CActiveRecord  {

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'seat';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('stadium_id, event_id, sector_id', 'required'),
            array('stadium_id, event_id, sector_id,', 'numerical', 'integerOnly'=>true),
            array('row_text, seat_text, x, y, width, height, zone_id, status', 'safe')
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Basket the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}