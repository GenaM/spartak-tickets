<?php
    $colors = array();
    $cnt    = 0;
    if( $sector['price'] ) {
        foreach( $sector['price'] as $priceId => $price ) {
            if( ! empty( $price->price ) ) {
                $colors[$priceId] = ++$cnt;
            }
        }
    }
?>

<div class="section">
    <?php $this->widget( 'SelectEventWidget' , array( 'events' => $this->events['all'] , 'prevEvent' => $this->events['prev'] , 'nextEvent' => $this->events['next'] , 'activeEvent' => $event ) ) ?>

    <div class="input-group clearfix">
        <a href="<?php echo Yii::app()->createUrl( 'tickets/stadium' , array( 'eventId' => $event['event_id'] ) ) ?>" class="btn control-size-s l-float" type="button">Назад</a>
        <div class="sector-name"><?php echo $sector['tribune'] ?><br /> <?php echo $sector['name'] ?> <?php /*echo $sector['upper'] ? 'Верх' : 'Низ'*/ ?></div>
<!--        <a href="--><?php //echo Yii::app()->createUrl( 'tickets/reservation' ) ?><!--" class="btn control-size-s r-float" type="button">Вперед</a>-->
    </div>

    <section class="scheme sector"
             data-addurl="<?php echo Yii::app()->createUrl( 'site/addToBasket' , array(
                 'stadiumId'    => $event['stadium_id'] ,
                 'eventId'      => $event['event_id'] ,
                 'sectorId'     => $sector['id'] ,
                 'sectorName'   => $sector['name'],
                 'tribune'      => $sector['tribune'],
                 'rowName'      => '_rowName_',
                 'seatId'       => '_seatId_',
                 'seatName'     => '_seatName_',
                 'upper'        => $sector['upper'],
             ) ) ?>"
             data-removeurl="<?php echo Yii::app()->createUrl( 'site/removeFromBasket' , array(
                 'eventId'      => $event['event_id'],
                 'seatId'       => '_seatId_',
             ) ) ?>"
             data-statusurl="<?php echo Yii::app()->createUrl( 'tickets/getSeatsStatus' , array(
                 'stadiumId'    => $event['stadium_id'] ,
                 'eventId'      => $event['event_id'] ,
                 'sectorId'     => $sector['id'],
             ) ) ?>">
        <table class="seats-table">
            <tbody class="seats-table__body">
                <?php foreach( $sector['seats'] as $y => $row ) { ?>
                    <?php if( ! empty( $lastY ) && $lastY < ( $y - 1 ) ) { ?>
                        <?php for( $i = 1 ; $i < $y - $lastY ; $i ++ ) { ?>
                            <td class="seats-table__cell"></td>
                        <?php } ?>
                        <?php $lastY = $y; ?>
                    <?php } ?>

                    <tr class="seats-table__row">
                        <?php $firstSeat = current($row); ?>
                        <td class="seats-table__cell seat-row"><?php echo $firstSeat['rowName'] ?></td>
                        <?php for( $i = 1 ; $i < key( $row ) ; $i ++ ) { ?>
                            <td class="seats-table__cell"></td>
                        <?php } ?>
                        <?php foreach( $row as $x => $seat )  { ?>
                            <?php if( ! empty( $lastX ) && $lastX < ( $x - 1 ) ) { ?>
                                <?php for( $i = 1 ; $i < $x - $lastX ; $i ++ ) { ?>
                                    <td class="seats-table__cell"></td>
                                <?php } ?>
                                <?php $lastX = $x; ?>
                            <?php } ?>
                            <?php
                                if( $seat['status'] == 3
                                    && isset( $activeSeats[ $event['event_id'] ][ $event['stadium_id'] ][ $sector['id'] ] )
                                    && in_array( $seat['id'] , $activeSeats[ $event['event_id'] ][ $event['stadium_id'] ][ $sector['id'] ] )
                                    && ! empty( $sector['price'][$seat['zoneId']]->price )) {
                                    $class = 'seat-active';
                                } elseif( $seat['status'] == 1 ) {
                                    $class = 'seat-free';
                                } else {
                                    $class = 'seat-busy';
                                }
                                $lastX = $x;
                            ?>
                            <td class="seats-table__cell <?php echo $class ?> seat-price-<?php echo ! empty( $colors[ $seat['zoneId'] ] ) ? $colors[ $seat['zoneId'] ] : '' ?>" data-status="<?php echo $seat['status'] ?>" data-row="<?php echo $firstSeat['rowName'] ?>" data-seat="<?php echo $seat['id'] ?>" data-seatname="<?php echo $seat['name'] ?>" ><?php echo $seat['name'] ?></td>
                        <?php } ?>
                    </tr>
                    <?php $lastY = $y ?>
                <?php } ?>
            </tbody>
        </table>
        <footer class="scheme__footer">поле</footer>
    </section>

    <?php if( $sector['price'] ) { ?>
        <?php foreach( $sector['price'] as $price ) { ?>
            <?php if( ! empty( $price->price ) ) { ?>
                <div class="ticket-price"><span class="seat-price-<?php echo $colors[$price->id] ?>">&diams; &diams; &diams;</span> Цена билета в этом секторе: <span class="red-text"><?php echo $price->price ?></span> р.</div>
            <?php } ?>
        <?php } ?>
    <?php } ?>
</div>


