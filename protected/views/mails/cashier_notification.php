<html>
    <head>
        <style>
            table {border-collapse: collapse}
            td, th { border-bottom: 1px solid #a1a1a1; padding: 5px; text-align: center; font-size: 14px; }
            th {font-weight: bold;}
            h1 {text-align: center;}
        </style>
    </head>
    <body>
        <hr>
        <h1 >Заказ оплачен</h1>
        <hr>

        <h3>Информация о заказе</h3>
        <div>
            <table>
                <tr>
                    <th>e-mail</th>
                    <th>ФИО</th>
                    <th>стоимость</th>
                </tr>
                <tr>
                    <td><?php echo $user->email ?></td>
                    <td><?php echo $user->last_name . ' ' . $user->first_name  . ' ' . $user->middle_name ?></td>
                    <td><?php echo $order->rapidaRequest->AMOUNT ?></td>
                </tr>
            </table>
        </div>
        <?php foreach( $order->basketOrders as $basketOrder )   { ?>
            <?php $basket = $basketOrder->basket; ?>
            <?php $event = SpartakMainHelper::checkEvent( $basket->event_id ) ?>
            <?php if( in_array($basket->status, array(Basket::STATUS_PAYED, Basket::STATUS_SOLD))) { ?>
                <div>
                    <table>
                        <tr>
                            <th>Номер заказа</th>
                            <th>Трибуна</th>
                            <th>Сектор</th>
                            <th>Ряд</th>
                            <th>Место</th>
                        </tr>
                        <tr>
                            <td><?php echo $basket['reservation_number'] ?></td>
                            <td><?php echo $basket['tribune_name'] ?></td>
                            <td><?php echo $basket['sector_name'] ?></td>
                            <td><?php echo $basket['row_name'] ?></td>
                            <td><?php echo $basket['seat_name'] ?></td>
                        </tr>
                    </table>
                </div>
            <?php } ?>
        <?php } ?>
    </body>
</html>