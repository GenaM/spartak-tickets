<footer class="footer">
<!--    <nav class="navigation">-->
<!--        <a class="link" href="--><?php //echo Yii::app()->createUrl( 'site/info' ) ?><!--">Правила и условия</a>-->
<!--        <a class="link" href="--><?php //echo Yii::app()->createUrl( 'site/seasonTicket' ) ?><!--">Абонементная программа и цены</a>-->
<!--	<a class="link" href="--><?php //echo Yii::app()->createUrl( 'site/howToBuy' ) ?><!--">Как купить абонемент</a>-->
<!--        <a class="link" href="--><?php //echo Yii::app()->createUrl( 'site/address' ) ?><!--">Как добраться</a>-->
<!--    </nav>-->

    <div class="footer__wrapper">
        <ul class="partners">
            <li class="partners__group">
                <a class="partner-logo partner-lu" href="http://www.lukoil.ru/" target="_blank"></a>
                <a class="partner-logo partner-nike" href="http://www.nike.com/nikefootball/home/?locale=ru_RU" target="_blank"></a>
            </li>
            <li class="partners__group">
                <!-- <a class="partner-logo partner-kia" href="http://www.kia.ru/" target="_blank"></a> -->
                <a class="partner-logo partner-ob" href="http://www.open.ru/" target="_blank"></a>
                <a class="partner-logo partner-pa" href="http://panatlanticexploration.com/" target="_blank"></a>
                <a class="partner-logo partner-ifd" href="http://www.ifdk.com/" target="_blank"></a>
                <a class="partner-logo partner-kuk" href="http://www.kapital-am.ru" target="_blank"></a>
            </li>
            <li class="partners__group">
	    
	    </li>
	    <li class="partners__group">
                <a class="partner-logo partner-rr" href="http://rusradio.ru/" target="_blank"></a>
                <!-- <a class="partner-logo partner-pepsi" href="http://www.pepsi.ru/" target="_blank"></a> -->
                <a class="partner-logo partner-chemp" href="http://www.championat.ru/" target="_blank"></a>
                <a class="partner-logo partner-se" href="http://www.sport-express.ru/" target="_blank"></a>
                <a class="partner-logo partner-fors" href="http://www.fors.ru/" target="_blank"></a>
		<a class="partner-logo partner-dummy" href="#"></a>
                <a class="partner-logo partner-ssg" href="http://www.sogaz.ru/" target="_blank"></a>
                <a class="partner-logo partner-mts" href="http://www.mts.ru/" target="_blank"></a>
            </li>
        </ul>

        <a class="logo footer-logo" href="http://spartak.com"></a>
    </div>

    <div class="copyright">Все права защищены &#169; 2001-<?php echo date('Y') ?></div>
</footer>