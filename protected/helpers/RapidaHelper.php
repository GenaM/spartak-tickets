<?php
    class RapidaHelper  {

        private static $payUrl      = 'https://3ds.payment.ru/cgi-bin/cgi_link';
        private static $checkUrl    = 'https://rs.psbank.ru:4443/cgi-bin/ecomm_check';

        public static function generatePSign( $params , $hmac = null )  {
            return strtoupper( hash_hmac( 'sha1' , $hmac ? $hmac : RapidaHelper::generateHMAC( $params ) , pack( 'H*' , RapidaHelper::generateKey() ) ) );
        }

        public static function generateHMAC( $p )  {
            return strlen( $p['AMOUNT'] ) . $p['AMOUNT'] .
                strlen( $p['CURRENCY'] ) . $p['CURRENCY'] .
                strlen( $p['ORDER'] ) . $p['ORDER'] .
                strlen( $p['MERCH_NAME'] ) . $p['MERCH_NAME'] .
                strlen( $p['MERCHANT'] ) . $p['MERCHANT'] .
                strlen( $p['TERMINAL'] ) . $p['TERMINAL'] .
                strlen( $p['EMAIL'] ) . $p['EMAIL'] .
                strlen( $p['TRTYPE'] ) . $p['TRTYPE'] .
                strlen( $p['TIMESTAMP'] ) . $p['TIMESTAMP'] .
                strlen( $p['NONCE'] ) . $p['NONCE'] .
                strlen( $p['BACKREF'] ) . $p['BACKREF'];
        }

        public static function generateKey()    {
            return strtoupper(
                implode(
                    unpack(
                        "H32" ,
                        pack( "H32" , '92624B259D4FAD304CB2F6002EAECE68' ) ^ pack( "H32" , '3AEA9526108DD2C4D8E3A704D292C253' )
                    )
                )
            );
        }

        /**
         * @param $orderId
         * @param $description
         * @param $terminal
         * @return array|null
         */
        public static function saveRapidaRequest($orderId, $description, $terminal)  { //TODO: hardcode
            if( ! $terminal )   {
                $terminal = 24009505;
            }
            $rapidaRequest = new RapidaRequest();

            $params = array(
                'AMOUNT'        => Yii::app()->controller->getTotalAmount(),
                'CURRENCY'      => 'RUB',
                'ORDER'         => $orderId,
                'DESC'          => substr( $description, 0, 50 ),
                'TERMINAL'      => trim($terminal),
                'TRTYPE'        => 1,
                'MERCH_NAME'    => 'SPARTAK',
                'MERCHANT'      => '000792224009505',
                'EMAIL'         => Yii::app()->user->mail,
                'TIMESTAMP'     => gmdate("YmdHis", time()),
                'NONCE'         => md5( gmdate("YmdHis", time()) ),
                'BACKREF'       => Yii::app()->request->hostInfo . Yii::app()->createUrl( 'tickets/payed' ),
            );
            $params['P_SIGN'] = RapidaHelper::generatePSign( $params );

            $rapidaRequest->attributes = $params;

            if( $rapidaRequest->save() )    {
                SpartakMainHelper::writeLog( print_r( $params, true ), 'RapidaRequest' );
                $order              = new Order();
                $order->attributes  = array( 'request_id'    => $rapidaRequest->id );
                $order->save();

                SpartakMainHelper::addBasketOrders( $order->id );

                return array(
                    'paymentUrl'    => self::$payUrl,
                    'orderedAt'     => date( 'YmdHis' , time() ),
                    'fields'        => $rapidaRequest->attributes
                );
            }
            return null;
        }

        /**
         * @param $params
         * @param TicketSoftHelper $ticketsClient
         * @return bool
         * @throws CHttpException
         */
        public static function processRapidaResponse( $params, $ticketsClient ) {
            SpartakMainHelper::writeLog( print_r( $params, true ), 'RapidaResponse' );
            $rapidaResponse = new RapidaResponse();
            $rapidaResponse->attributes = $params;
            if( $rapidaResponse->save() ) {
                $rapidaRequest = RapidaRequest::model()->find( new CDbCriteria( array(
                    'condition' => 't.`ORDER` = :order AND t.`AMOUNT` = :amount AND t.`EMAIL` = :email',
                    'params'    => array( ':order' => $rapidaResponse->ORDER, ':amount' => $rapidaResponse->AMOUNT, ':email' => $rapidaResponse->EMAIL ),
                    'with'      => array(
                        'order' => array(
                        )
                    )
                ) ) );

                if( ! empty( $rapidaRequest->order ) )    {
                    if( RapidaHelper::checkParams( $rapidaResponse->attributes ) )   { //save params and check it
                        $rapidaRequest->order->attributes = array(
                            'response_id'   => $rapidaResponse->id,
                            'status'        => Order::STATUS_PAYED
                        );
                        $rapidaRequest->order->save();

                        foreach( $rapidaRequest->order->basketOrders as $basketOrder )  {
                            if( $basketOrder->basket->status != Basket::STATUS_SOLD ) {
                                $basketOrder->basket->attributes = array(
                                    'status' => Basket::STATUS_PAYED
                                );
                                $basketOrder->basket->save();
                            }
                        }
                    } else {
                        $rapidaRequest->order->attributes = array(
                            'response_id'   => $rapidaResponse->id,
                            'status'        => Order::STATUS_ERROR
                        );
                        $rapidaRequest->order->save();
                    }
                    if( $rapidaRequest->order->save() && $rapidaRequest->order->status == Order::STATUS_PAYED ) {
                        $order = $rapidaRequest->order;
                        $order->rapidaRequest = RapidaRequest::model()->findByPk( $order->request_id );
                        $user = User::model()->findByPK( $order->user_id );
                        SpartakMainHelper::sendCashierNotification( $order , $user , 'Заказ №' . $order->id . ' оплачен' );
                        foreach( $order->basketOrders as $basketOrder )   {
                            $basket = $basketOrder->basket;
                            if( $basket->status == Basket::STATUS_PAYED ) {
                                $reservationPayedResult = $ticketsClient->SetReservationPayed($basket->stadium_id, $basket->reservation_id, $basket->amount, 1 . $basket->id);
                                $basket->attributes = array(
                                    'set_reservation_payed' => $reservationPayedResult
                                );
                                if ((bool)$reservationPayedResult) {
                                    SpartakMainHelper::sendCashierNotification($order, $user, 'Ошибка бронирвоания TicketSoft (ReservationPayed2: ' . $reservationPayedResult . ')');
                                    $basket->attributes = array(
                                        'status' => Basket::STATUS_TS_ERROR
                                    );
                                    $basket->save();
                                    continue;
                                }

                                if( $basket->event_type == TicketSoftHelper::TYPE_EVENT )   {
                                    $reservationInfo = $ticketsClient->SellReservation( $basket->stadium_id , $basket->reservation_id , 2 . $basket->id );
                                    if(is_array($reservationInfo)) {
                                        $barcode = CSite::generateBarcode($reservationInfo['barcode']);
                                        if ($barcode) {
                                            $basket->attributes = array(
                                                'barcode' => $barcode,
                                                'ticket_number' => $reservationInfo['ticketNumber'],
                                                'ticket_serial' => $reservationInfo['ticketSerial'],
                                                'sell_reservation_result' => 0,
                                                'status' => Basket::STATUS_SOLD,
                                            );
                                        }
                                        $qrcode = CSite::generateQRcode($reservationInfo['barcode']);
                                    } else {
                                        $basket->attributes = array(
                                            'status' => Basket::STATUS_BARCODE_ERROR,
                                            'sell_reservation_result' => $reservationInfo
                                        );
                                    }
                                    $basket->save();
                                }
                            }
                        }

                        if( SpartakMainHelper::sendTickets( $order , $user ) )  {
                            $order->attributes = array(
                                'status' => Order::STATUS_SENT
                            );
                        } else {
                            $order->attributes = array(
                                'status' => Order::STATUS_ERROR_PAYED
                            );
                        }
                        $order->save();
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * @param $params
         * @return string
         */
        public static function checkTest( $params ) {
            return file_get_contents( self::$checkUrl . '?' . http_build_query( $params ) );
        }

        public static function checkParams( $params )    {
            try {
                if( ! $params || ! isset( $params['RESULT'] ) || (int) $params['RESULT'] !== 0 ) {
                    return false;
                }
                $response   = array();
                foreach( $params as $key => $value )    {
                    $response[ $key ] = $value;
                }

                $hmac_vars = array("AMOUNT","CURRENCY","ORDER","MERCH_NAME","MERCHANT","TERMINAL","EMAIL","TRTYPE","TIMESTAMP","NONCE","BACKREF","RESULT","RC","RCTEXT","AUTHCODE","RRN","INT_REF");
                $hmac = '';

                foreach ( $hmac_vars as $param ){
                    if( strlen( $response[$param] ) != 0 ){
                        $hmac .= strlen( $response[$param] ) . $response[$param];
                    } else {
                        $hmac .= "-";
                    }
                }

                if( strcmp( self::generatePSign( $response , $hmac ) , strtoupper( $response['P_SIGN'] ) ) == 0 ) {
                    return true;
                }
                return false;
            } catch( Exception $e ) {
                throw new CHttpException( 500 );
            }
        }

    }