<div id="additional-operations">
    <div>
        <div>
            <label>Удаление броней старше 1 часа</label>
        </div>
        <div class="btn btn-primary clearReservations" data-toggle="modal" data-target="#operation-started-modal" data-url="<?php echo Yii::app()->createUrl('spartAdmin/clearReservations') ?>">Удалить</div>
    </div>
    <br />
    <div>
        <div>
            <label>Удаление броней старше</label>
            <input id="hours" type="text" name="hours" value="01" />
            <label>часов(удалить игнорируя ТS = принудительная очистка корзины)</label>
        </div>
        <div class="btn btn-primary clearReservations" data-toggle="modal" data-target="#operation-started-modal" data-with="#hours" data-url="<?php echo Yii::app()->createUrl('spartAdmin/clearReservations', array('ignoreTS' => 0)) ?>">Удалить</div>
        <div class="btn btn-primary clearReservations" data-toggle="modal" data-target="#operation-started-modal" data-with="#hours" data-url="<?php echo Yii::app()->createUrl('spartAdmin/clearReservations', array('ignoreTS' => 1)) ?>">Удалить игнорируя TS</div>
    </div>
    <div>
        <pre>
            <?php //echo $CRLog ?>
        </pre>
    </div>
</div>