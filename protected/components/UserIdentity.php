<?php
    class UserIdentity extends CUserIdentity    {

        const ERROR_CAN_LOGIN                   = 401;
        const ERROR_CONTACT_NOT_FOUND           = 100021;
        const ERROR_NO_ACTIVE_PASSWORD          = 100023;
        const ERROR_PASSWORD_TRIES_LIMIT        = 100024;
        const ERROR_PASSWORD_EXPIRES            = 100027;
        const ERROR_USERNAME_BLOCKED            = 100028;

        const ERROR_USERNAME_PASSWORD_INVALID   = 100680;
        const ERROR_SESSION_TIME_EXPIRED        = 100881;

        public function authenticate() {
            $SUH    = new SpartakUsersHelper();
            $user = $SUH->auth( $this->username , $this->password );
            if( ! empty( $user['error'] ) ) {
                $this->errorCode = $user['error'];
                return !$this->errorCode;
            }

            if( $dbUser = User::addSpartakUser( $user ) ) { //if saved new user or user there
                $this->errorCode=self::ERROR_NONE;
                $this->setState( 'id' , $dbUser->id );
                foreach( $user as $key => $value )   {
                    $this->setState( $key , $value );
                }
            }

            return !$this->errorCode;
        }
    }
