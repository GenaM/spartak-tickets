<p>Спасибо за покупку, <?php echo $notFullName ? $notFullName : null ?></p>

<p>Свой абонемент Вы можете получить в кассах стадиона «Открытие Арена» с  11 часов дня Москва,<br />
    Волоколамское шоссе, вл. 67  <a href="http://tickets.spartak.com/address">http://tickets.spartak.com/address.</a></p>
<p><b>Покупатели абонементов GOLD 1,2 и SILVER 1,2</b> кроме касс стадиона также могут получить  абонемент в офисе Клуба,<br />
    <b> при условии уведомления билетного отдела (ФИО и номер заказа) по электронной почте  order@spartak.com сразу после осуществления покупки.</b> </p>
<p><a href="http://spartak.com/main/club/contacts/">http://spartak.com/main/club/contacts/.</a> Москва, Краснопресненская набережная, д. 10 стр.4 </p>
<p><b><i><u>При себе необходимо иметь высланный Вам номер заказа, паспорт, а также заполненную и подписанную анкету.</u></i></b></p>

<p>Телефон контакт-центра +7 495 777-42-00</p>

<?php if( ! empty( $tickets ) ) { ?>
    <?php $this->renderPartial('//mails/_tickets_info_list', array('tickets' => $tickets, 'fullName' => $fullName)); ?>
<?php } ?>

<p></p>
<p>В редких случаях проход по электронному билету не является возможным.  Об этих неприятных ситуациях мы хотели бы написать, чтобы вы могли их избежать заранее.</p>

<p> <b>1) Распечатка билета из режима «просмотр» в любом интернет-браузере.</b> <br />
    При распечатке билета из режима «просмотр» в любом Интернет-браузере не соблюдаются пропорции бланка билета. Он значительно меньше оригинала. Штрих-код становится менее читаемым.
    Мы настоятельно просим вас открывать присылаемые электронные билеты в соответствующих программах (Adobe Acrobat Reader, Foxit Editor) и печатать из нее, не уменьшая пропорции и проценты печати присланного файла.</p>
<p> <b>2)    Печать на цветном принтере.</b> <br />
    При печати на цветном принтере изменяются свойства видимости штрих-кода. Печатать присланные электронные билеты необходимо в хорошем качестве на лазерном или струйном принтере с разрешением не ниже 600dpi. При печати выбрать режим черно-белой печати, без цветов.</p>
<p> <b>3) Плохое качество печати.</b> <br />
    Присланные билеты необходимо распечатывать на лазерном или струйном принтере с разрешением не ниже 600 dpi.</p>