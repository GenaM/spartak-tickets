<?php
    if( file_exists( Yii::app()->basePath . '/../web/js/stadiums/' . $this->events['active']['stadium_id'] . '.js' ) )  {
        Yii::app()->clientScript->registerScriptFile( '/js/raphael.min.js' , CClientScript::POS_END );
        Yii::app()->clientScript->registerScriptFile( '/js/stadiums/' . $this->events['active']['stadium_id'] . '.js' , CClientScript::POS_END );
        Yii::app()->clientScript->registerScriptFile( '/js/stadiums/init.js' , CClientScript::POS_END );
    } else {
        Yii::app()->clientScript->registerScriptFile( '/js/stadiums/simple.js' , CClientScript::POS_END );
    }
?>
<div class="section">
    <?php $this->widget( 'SelectEventWidget' , array( 'events' => $this->events['all'] , 'prevEvent' => $this->events['prev'] , 'nextEvent' => $this->events['next'] , 'activeEvent' => $this->events['active'] ) ) ?>
    <div id="stadium"
         data-width="643"
         data-height="509"
         data-stadium="<?php echo $this->events['active']['stadium_id'] ?>"
         data-getlist="<?php echo Yii::app()->createUrl( 'tickets/getSectorList' , array( 'stadiumId' => $this->events['active']['stadium_id'] , 'eventId' => $this->events['active']['event_id'] ) ) ?>"
         data-sectorurl="<?php echo Yii::app()->createUrl( 'tickets/sector' , array( 'sectorId' => '_sectorId_', 'eventId' => $this->events['active']['event_id'] ) ) ?>">
        <i class="preloader preloader_visibility_yes">
            <span>Ожидаем ответ от билетной системы</span>
        </i>
     </div>
</div>