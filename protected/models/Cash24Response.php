<?php

/**
 * This is the model class for table "cash24_response".
 *
 * The followings are the available columns in table 'cash24_response':
 * @property integer $id
 * @property string $amount
 * @property string $currency
 * @property string $order
 * @property string $url
 * @property string $invoice_expires_on
 * @property string $method
 * @property string $wallet
 * @property string $reference
 * @property string $client_paid_amount
 * @property string $status
 * @property string $reason
 * @property string $salt
 * @property string $sign
 */
class Cash24Response extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cash24_response';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, amount, currency, order, url, invoice_expires_on, method, wallet, reference, client_paid_amount, status, salt, sign', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('reason', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, amount, currency, order, url, invoice_expires_on, method, wallet, reference, client_paid_amount, status, reason, salt, sign', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'amount' => 'Amount',
			'currency' => 'Currency',
			'order' => 'Order',
			'url' => 'Url',
			'invoice_expires_on' => 'Invoice Expires On',
			'method' => 'Method',
			'wallet' => 'Wallet',
			'reference' => 'Reference',
			'client_paid_amount' => 'Client Paid Amount',
			'status' => 'Status',
			'reason' => 'Reason',
			'salt' => 'Salt',
			'sign' => 'Sign',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('order',$this->order,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('invoice_expires_on',$this->invoice_expires_on,true);
		$criteria->compare('method',$this->method,true);
		$criteria->compare('wallet',$this->wallet,true);
		$criteria->compare('reference',$this->reference,true);
		$criteria->compare('client_paid_amount',$this->client_paid_amount,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('sign',$this->sign,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cash24Response the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
