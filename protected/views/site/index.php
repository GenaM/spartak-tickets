<div class="section">
    <?php if(empty($events)) { ?>
    <p>Нет активных событий.</p>
    <?php } else { ?>
        <?php foreach($events as $key => $event) { ?>
            <?php if(!$event['use_big_banner']) { continue; } ?>
            <?php
                $url = Yii::app()->createUrl( 'tickets/stadium' , array( 'eventId' => $event['event_id'] ) );
                if(! empty($event['url']))  {
                    $url = $event['url'];
                }
            ?>
                    <section class="game-preview" data-href="<?php echo $url ?>">
                <img src="/uploads/<?php echo $event['id'] ?>/<?php echo $event['big_banner'] ?>" style="width: 100%; height: 100%;" />
            </section>
            <?php unset($events[$key]) ?>
        <?php } ?>

        <ul class="preview-list clearfix">
            <?php foreach( $events as $event ) { ?>
                <?php
                $url = Yii::app()->createUrl( 'tickets/stadium' , array( 'eventId' => $event['event_id'] ) );
                if(! empty($event['url']))  {
                    $url = $event['url'];
                }
                ?>
                <li class="preview-list__item" data-href="<?php echo $url ?>">
                    <img class="img" src="/uploads/<?php echo $event['id'] ?>/<?php echo $event['little_banner'] ?>">
                </li>
            <?php } ?>
        </ul>
    <?php } ?>
</div>