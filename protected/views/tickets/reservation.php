<div class="section">
     <section class="widget pay-systems-widget">
         <header class="widget__header">Выбрать способ оплаты билетов:</header>
         <div class="widget__body">
             <form id="pay-form" class="form pay-form" action="<?php echo $paymentUrl ?>" method="post">

                <?php foreach( $fields as $name => $value ) { ?>
                     <input type="hidden" name="<?php echo $name ?>" value="<?php echo $value ?>">
                <?php } ?>

                 <label class="pay-system">
                     <span class="pay-system__logo"><i class="ps-logo ps-vm"></i></span>
                     <input class="radiobox" type="radio" name="pay-system" value="vm" tabindex="2">
                     <span class="pay-system__name">Visa / MasterCard</span>
                 </label>

             </form>
         </div>
     </section>
</div>