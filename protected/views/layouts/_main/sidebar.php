
<aside class="sidebar">
    <?php $this->widget( 'AuthWidget' ) ?>
    <?php if( $this->showBasket ) { ?>
        <?php $this->widget( 'BasketWidget' , array( 'basketList' => $this->basket , 'eventList' => $this->events ) ) ?>
    <?php } ?>

    <section class="widget">
        <header class="widget__header">Информация</header>

        <div class="widget__body">
            <p class="text">
        Как добраться до стадиона «Открытие Арена»
                смотрите подробное описание <a class="link red-text" href="<?php echo Yii::app()->createUrl( 'site/address' ) ?>">ЗДЕСЬ</a>
            </p>
        </div>
    </section>

<!--    <section class="widget clickable" data-href="/stadium?eventId=3357385">-->
<!--        <img class="banner" src="/i/banners/боковой_абонемент.png" alt="Продажа абонементов">-->
<!--    </section>-->
</aside>