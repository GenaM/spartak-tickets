<div class="section">
    <section class="widget">
        <header class="widget__header">Выбрать способ доставки билетов:</header>
        <div class="widget__body">
            <form class="form order-form" action="<?php echo Yii::app()->createUrl( 'tickets/result' ) ?>" method="post">
                <div class="check-group">
                    <label class="check-label">
                        <input class="radiobox" type="radio" name="place" value="<?php echo Defaults::PLACE_INDEPEND ?>" tabindex="2" checked="checked">
                        Напечатать самостоятельно (стоимость – 0 руб.)
                    </label>
                </div>

                <button class="btn control-size-m r-float" type="submit">Получить билет</button>
            </form>
        </div>
    </section>
</div>