<?php
/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'basket':
 * @property integer $id
 * @property integer $event_id
 * @property string $name
 * @property string $description
 * @property string $start_time
 * @property string $end_time
 * @property integer $personal
 * @property integer $is_active
 * @property integer $is_reserving_enabled
 * @property string $st_containers
 * @property string $st_sectors
 * @property integer $type
 * @property integer $stadium_id
 * @property string $stadium_name
 * @property string $rapida_terminal
 * @property string $big_banner
 * @property string $little_banner
 * @property integer $use_big_banner
 * @property integer $is_concert
 * @property string $url
 *
 *
 * The followings are the available model relations:
 * @property User $user
 * @property BasketOrder[] $basketOrders
*/

class Event extends CActiveRecord  {

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'event';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('event_id, name, start_time, type, stadium_id, stadium_name', 'required'),
            array('event_id, stadium_id', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('st_containers, st_sectors, rapida_terminal, big_banner, little_banner, use_big_banner, is_concert, is_active, url', 'safe')
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Basket the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()    {
        $this->st_containers = serialize( $this->st_containers );
        $this->st_sectors = serialize( $this->st_sectors );
        return parent::beforeSave();
    }

    public function afterFind() {
        $this->getSTContainers();
        $this->getSTSectors();
        return parent::afterFind();
    }

    public function getSTContainers() {
        if( $this->st_containers ) {
            $this->st_containers = unserialize( $this->st_containers );
        }

        return null;
    }

    public function getSTSectors() {
        if( $this->st_sectors ) {
            $this->st_sectors = unserialize( $this->st_sectors );
        }

        return null;
    }

}