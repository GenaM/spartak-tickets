<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property integer $status
 * @property integer $user_id
 * @property integer $request_id
 * @property integer $response_id
 * @property string $date
 * @property integer $alarmed
 *
 * The followings are the available model relations:
 * @property BasketOrder[] $basketOrders
 * @property User $user
 * @property RapidaRequest $rapidaRequest
 * @property RapidaResponse $rapidaResponse
 */
class Order extends CActiveRecord
{
    const STATUS_OVERDUE                = 0;
    const STATUS_NEW                    = 1;
    const STATUS_PAYED                  = 2;
    const STATUS_CANCELED               = 3;
    const STATUS_SENT                   = 4;
    const STATUS_CANCELED_RAPIDA        = 5;
    const STATUS_ERROR                  = 100;
    const STATUS_ERROR_PAYED            = 101;
    const STATUS_ERROR_RAPIDA           = 102;
    const STATUS_ERROR_EMPTY_BASKET     = 103;


    public $rapidaRequest, $rapidaResponse;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, user_id, request_id, response_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, status, user_id, request_id, response_id, date, alarmed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'basketOrders' => array(self::HAS_MANY, 'BasketOrder', 'order_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'rapidaRequest' => array(self::BELONGS_TO, 'RapidaRequest', 'request_id'),
			'rapidaRespone' => array(self::BELONGS_TO, 'RapidaReponse', 'response_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'user_id' => 'User',
			'request_id' => 'Request',
			'response_id' => 'Response',
			'date' => 'Date',
			'alarmed' => 'Alarmed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status',$this->status);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('request_id',$this->request_id);
		$criteria->compare('response_id',$this->response_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('alarmed',$this->alarmed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeSave()    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->status   = Order::STATUS_NEW;
                $this->user_id  = Yii::app()->user->id;
                $this->date     = CSite::getNow();

                $old = Order::model()->findAllByAttributes( array( 'user_id' => $this->user_id , 'status' => Order::STATUS_NEW ) );
                if( $old ) {
                    foreach( $old as $order )   {
                        $order->attributes = array(
                            'status' => Order::STATUS_CANCELED
                        );
                        $order->save();
                    }
                }
            }
            return true;
        }
        else
            return false;
    }

    public function afterFind() {
        $this->rapidaRequest = RapidaRequest::model()->findByPK( $this->request_id );
        parent::afterFind();
    }

    public function loadResponse()  {
        $this->rapidaResponse = RapidaRequest::model()->findByPK( $this->response_id );
    }

    public function loadRequest()  {
        $this->rapidaRequest = RapidaRequest::model()->findByPK( $this->request_id );
    }

    public function setAttrs( $attributes ) {
        $this->attributes = $attributes;
        if( ! $this->save() ) {
            return false;
        }
        return true;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
