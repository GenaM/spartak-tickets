<a class="btn btn-lg btn-info update-command" data-toggle="modal" data-target="#operation-started-modal" href="#" data-url="<?php echo Yii::app()->createUrl('spartAdmin/updateEvents') ?>">Обновить список матчей</a>

<?php if( $data ) { ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $data as $event ) { ?>
                <tr>
                    <td><?php echo $event['event_id'] ?></td>
                    <td><?php echo $event['name'] ?></td>
                    <td>
                        <a title="Редактировать" class="btn btn-lg btn-danger" href="<?php echo Yii::app()->createUrl('spartAdmin/updateEvent', array('id' => $event->id)) ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                        <a title="Обновить сектора" class="btn btn-lg btn-info update-command" data-toggle="modal" data-target="#operation-started-modal" href="#" data-url="<?php echo Yii::app()->createUrl('spartAdmin/updateSectors', array('id' => $event['id'])) ?>"><span class="glyphicon glyphicon-equalizer" aria-hidden="true"></span></a>
                    </td>
                </tr>

            <?php } ?>
        </tbody>
    </table>
<?php } ?>