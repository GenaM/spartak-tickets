$(document).ready(function() {

  $('.orders tr[data-id]').click(function() {
    var subT = $('#' + $(this).data('id'));
    if( subT.is(':visible') ) {
      subT.hide();
    } else {
      subT.show();
    }
  });

  $('.orders .process').click(function() {
    $.ajax({
      url : $(this).data('url')
    });
  });

  $('#additional-operations .clearReservations').click(function() {
    var self = $(this);
    var data = {};
    if(self.data('with')) {
      data[$(self.data('with')).attr('name')] = $(self.data('with')).val();
    }
    $.ajax({
      url : self.data('url'),
      type: 'get',
      data: data,
      success: function(r) {
        alert(r);
      }
    });
  });

  $('.update-command').click(function() {
    $.ajax({
      url : $(this).data('url')
    });
  });
});