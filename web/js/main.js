//TODO normalize selectors in layouts && here

$(document).ready(function () {

  $('input, textarea').placeholder();

  $('.select').selectBox();

  jQuery(document).tooltip();

  $('.event-list').change(function () {
    var selected = $('.event-list :selected');
    if (selected.data('url')) {
      window.location.href = selected.data('url');
    }
  });

  $('.seats-table__cell').click(function () {
    var self = $(this);
    if (self.hasClass('disabled')) {
      return;
    }
    if (self.hasClass('seat-free')) {
      self.removeClass('seat-free').addClass('seat-active');
      addToBasket($('.scheme.sector').data('addurl').replace('_rowName_', self.data('row'))
        .replace('_seatId_', self.data('seat'))
        .replace('_seatName_', self.data('seatname')));
    } else if (self.hasClass('seat-active')) {
      self.removeClass('seat-active').addClass('seat-free');
      removeFromBasket($('.scheme.sector').data('removeurl').replace('_seatId_', self.data('seat')));
    }
  });

  $('.cart.widget .rm-btn').live('click', function () {
    removeFromBasket($(this).data('removeurl'));
  });

  $('[data-href]').live('click', function () {
    window.location.href = $(this).data('href');
  });

  $('[data-href] *').live('click', function () {
    window.location.href = $(this).parents('[data-href]').data('href');
  });

  //TODO: this is spike, it's actual while we have 1 pay system
  if ($('#pay-form').length) {
    $('#pay-form').submit();
  }


  $('[name="pay-system"]').change(function (e) {
    $('#pay-form').submit();
  });

//  if ($('.scheme.sector').length) {
//    var seat;
//    setInterval(function () {
//      $.ajax({
//        url: $('.scheme.sector').data('statusurl'),
//        type: 'get',
//        dataType: 'json',
//        success: function (r) {
//          if (!r) {
//            return;
//          }
//          $.each(r.seats, function (i, val) {
//            seat = $('.seats-table__cell[data-seat="' + val.id + '"]');
//            seat.removeClass('seat-free seat-busy seat-active');
//            if (val.status == 1) {
//              seat.addClass('seat-free');
//            } else if (val.status == 3 && $.inArray(val.id, r.inBasket)) {
//              seat.addClass('seat-active');
//            } else {
//              seat.addClass('seat-busy');
//            }
//          });
//        }
//      });
//    }, 15000);
//  }
});


function addToBasket(url) {
  $('.cart__form').html('<i class="preloader preloader_visibility_yes"></i>');
  $('.seats-table__cell').addClass('disabled');
  $.ajax(url, {
    type: 'get',
    dataType: 'json',
    success: function (r) {
      $('.seats-table__cell.disabled').removeClass('disabled');
      if (r) {
        if (r.redirect) {
          window.location.href = r.redirect
        }
        if (r.html) {
          $('.cart.widget').html($(r.html).html());
        }
      }
    }
  });
}

function removeFromBasket(url) {
  $('.cart__form').html('<i class="preloader preloader_visibility_yes"></i>');
  $('.seats-table__cell').addClass('disabled');
  $.ajax(url, {
    type: 'get',
    dataType: 'json',
    success: function (r) {
      $('.seats-table__cell.disabled').removeClass('disabled');
      if (r) {
        if (r.redirect) {
          window.location.href = r.redirect
        }
        if (r.html) {
          $('.cart.widget').html($(r.html).html());
        }
        var seatId = getURLParameter('seatId', url);
        url = url.replace(seatId, '_seatId_');
        var sector = $('.sector.scheme[data-removeurl="' + url + '"]');
        if (sector.length) {
          sector.find('.seat-active[data-seat="' + seatId + '"]').removeClass('seat-active').addClass('seat-free');
        }
      }
    }
  });
}

function getURLParameter(name, url) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [, ""])[1].replace(/\+/g, '%20')) || null;
}