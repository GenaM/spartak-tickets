<?php
/**
 * @var $cs CClientScript
 */
$cs = Yii::app()->getClientScript();
$cs->coreScriptPosition = CClientScript::POS_END;
?>
<!DOCTYPE html>
<html class="page">
<head>
    <title>Spartak Tickets | Административная панель</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
        $cs->registerCssFile('/css/bootstrap/css/bootstrap.min.css');
        $cs->registerCssFile('/css/bootstrap/css/bootstrap-theme.min.css');
        $cs->registerCssFile( '/css/spartAdmin.css' )
    ?>

    <?php $cs->registerScriptFile( '/js/jquery-1.11.2.min.js') ?>
    <?php $cs->registerScriptFile('/css/bootstrap/js/bootstrap.min.js'); ?>
    <?php $cs->registerScriptFile( '/js/spartAdmin.js' , CClientScript::POS_END ) ?>
</head>

<body class="page__body">
    <?php $this->renderPartial('//layouts/_spartAdmin/header'); ?>
    <div class="container">
        <?php echo $content; ?>
    </div>


    <div class="modal fade" id="operation-started-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Запуск операции</h4>
                </div>
                <div class="modal-body">
                    <p>Операция запущена</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>



<!--    --><?php //$this->renderPartial('//layouts/_spartAdmin/footer'); ?>
</body>
</html>