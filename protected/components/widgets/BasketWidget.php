<?php
    class BasketWidget extends CWidget  {

        public $eventList;
        public $basketList;
        public $message;

        public $totalAmount     = 0;
        public $totalTickets    = 0;
        public $totalTicketsLabel;

        private $ticketsLabels = array(
            1 => 'билет',
            2 => 'билета',
            3 => 'билетов',
        );

        public function run()   {
            if( ! $this->eventList['all'] ) {
                return false;
            }
            $this->processEvents();
            $this->processTickets();
            $this->totalTicketsLabel = CSite::getCorrectStr( $this->totalTickets , $this->ticketsLabels );
            $this->render( 'basketWidget' );
            return true;
        }

        private function processEvents()    {
            foreach( $this->eventList['all'] as $event )   {
                if( isset( $this->basketList[ $event['event_id'] ] ) )    {
                    $this->basketList[ $event['event_id'] ]['name'] = $event['name'];
                    $this->basketList[ $event['event_id'] ]['start_time'] = $event['start_time'];
                }
            }
        }

        private function processTickets()   {
            if( ! $this->basketList )   {
                return false;
            }
            foreach( $this->basketList as $event )  {
                $this->totalAmount  = CSite::getTotalSum( $event['tickets'] , 'amount' );
                $this->totalTickets = CSite::getTotalSum( $event['tickets'] );
            }
            return true;
        }

    }