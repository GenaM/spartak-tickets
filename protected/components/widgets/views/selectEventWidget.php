<div class="input-group clearfix">
    <span class="select-control">
        <select class="event-list select">
            <option class="select__option" value="0">Выбрать событие</option>
<!--            --><?php //foreach( $this->events as $event ) { ?>
<!--                <option class="select__option" data-url="--><?php //echo Yii::app()->createUrl( 'tickets/stadium' , array( 'eventId' => $event['event_id'] ) ) ?><!--">--><?php //echo $event['name'] ?><!--</option>-->
<!--            --><?php //} ?>
        </select>
    </span>

    <a href="<?php echo Yii::app()->createUrl( 'site/info' ) ?>" class="btn control-width-medium r-float" type="button">Как купить билет</a>
</div>

<div class="event-slider">
<!--    <a class="event-slider__prev --><?php //echo $this->prevEvent ? null : 'event-slider__prev_state_disabled' ?><!--" href="--><?php //echo $this->prevEvent ? Yii::app()->createUrl( 'tickets/stadium' , array( 'eventId' => $this->prevEvent['event_id'] , 'sId' => $this->prevEvent['stadium_id'] ) ) : null ?><!--"></a>-->
    <a class="event-slider__prev event-slider__prev_state_disabled" href="#"></a>
<!--    <a class="event-slider__next --><?php //echo $this->nextEvent ? null : 'event-slider__next_state_disabled' ?><!--" href="--><?php //echo $this->nextEvent ? Yii::app()->createUrl( 'tickets/stadium' , array( 'eventId' => $this->nextEvent['event_id'] , 'sId' => $this->nextEvent['stadium_id'] ) ) : null ?><!--"></a>-->
    <a class="event-slider__next event-slider__next_state_disabled" href="#"></a>

        <?php $date = new DateTime( $this->activeEvent['start_time'] ); ?>
        <div class="event-slider__box">
            <p class="date">&nbsp;<?php /* echo $date->format( 'd-m-Y' ) */ ?></p>
            <p class="game-title"><?php echo $this->activeEvent['name'] ?></p>
            <p class="time">&nbsp;<?php /*echo $date->format( 'H:i' )*/ ?></p>
        </div>
</div>