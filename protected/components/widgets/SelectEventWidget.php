<?php
    class SelectEventWidget extends CWidget {

        public $events      = array();
        public $nextEvent   = null;
        public $prevEvent   = null;
        public $activeEvent = null;

        public function run()   {
            if( ! $this->events )   {
                return false;
            }
            $this->render( 'selectEventWidget' );
            return true;
        }

    }