<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $errorCode = 404;

    public $session;

    /**
     * @var null | TicketSoftHelper
     */
    public $tickets = null;
    public $basket  = null;
    public $events  = null;

    public $showBasket  = true;
    public $loadBasket  = true;
    public $loadTickets = true;

    public function beforeAction( $action )  {
        $this->session = new CHttpSession;
        $this->session->open();

        if( $this->loadTickets ) {
            $this->tickets = new TicketSoftHelper();
        }

        SpartakMainHelper::getEvents( Yii::app()->request->getParam( 'eventId' ) );

        if( $this->loadBasket ) {
            SpartakMainHelper::loadBasket();
        }
        return parent::beforeAction( $action );
    }

    /**
     * @return float
     */
    public function getTotalAmount()   {
        $result = 0;
        if( $this->basket ) {
            foreach( $this->basket as $event )  {
                foreach( $event['tickets'] as $ticket ) {
                    if( $ticket->status == Basket::STATUS_RESERVED ) {
                        $result += $ticket->amount;
                    }
                }
            }
        }
        return round( $result , 2 );
    }
}