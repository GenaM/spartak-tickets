<div class="section">
    <section class="widget">
        <header class="widget__header">необходимо заполнить данные в личном кабинете</header>
        <div class="widget__body">
            <div class="error-label">
                <i class="icon error-icon"></i>
                ОШИБКА: <span class="error-label__text">Для осуществления покупки вам необходимо указать полные контактные сведения о себе в личном кабинете lk.spartak.com</span>
            </div>
        </div>
    </section>
</div>