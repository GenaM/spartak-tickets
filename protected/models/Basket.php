<?php

/**
 * This is the model class for table "basket".
 *
 * The followings are the available columns in table 'basket':
 * @property integer $id
 * @property integer $user_id
 * @property string $stadium_id
 * @property string $event_id
 * @property string $event_type
 * @property string $sector_id
 * @property string $seat_id
 * @property string $reservation_id
 * @property string $reservation_number
 * @property integer $reservation_result
 * @property integer $sell_reservation_result
 * @property integer $set_reservation_payed
 * @property integer $confirm_reservation
 * @property string $sector_name
 * @property string $tribune_name
 * @property string $row_name
 * @property string $seat_name
 * @property double $amount
 * @property integer $upper
 * @property integer $status
 * @property string $barcode
 * @property string $ticket_serial
 * @property string $ticket_number
 * @property string $expiration_time
 *
 * The followings are the available model relations:
 * @property User $user
 * @property BasketOrder[] $basketOrders
 */
class Basket extends CActiveRecord
{
    const STATUS_OVERDUE            = 0;
    const STATUS_NEW                = 1;
    const STATUS_RESERVED           = 2;
    const STATUS_CONFIRMED          = 6;
    const STATUS_PAYED              = 3;
    const STATUS_SOLD               = 4;
    const STATUS_CANCELED           = 5;
    const STATUS_EVENT_MISSING      = 99;
    const STATUS_ERROR              = 100;
    const STATUS_BARCODE_ERROR      = 101;
    const STATUS_LIMIT_ERROR        = 102;
    const STATUS_TS_ERROR           = 103;
    const STATUS_TS_REMOVE_ERROR    = 104;

    const FIRST_STEP_RESERVATION_TIME   = 300; //5 min
    const SECOND_STEP_RESERVATION_TIME  = 3600; //40 min

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'basket';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, stadium_id, event_id, sector_id, seat_id, sector_name, tribune_name, row_name, seat_name, expiration_time, event_type', 'required'),
			array('user_id, upper, status', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, stadium_id, event_id, sector_id, seat_id, reservation_id, reservation_number, reservation_result, sell_reservation_result, confirm_reservation, set_reservation_payed, sector_name, tribune_name, row_name, seat_name, amount, upper, status', 'safe', 'on'=>'search'),
            array('barcode, reservation_id, reservation_number, sell_reservation_result, reservation_result, confirm_reservation, set_reservation_payed, status, ticket_serial, ticket_number', 'safe')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'basketOrders' => array(self::HAS_MANY, 'BasketOrder', 'basket_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'stadium_id' => 'Stadium',
			'event_id' => 'Event',
			'event_type' => 'Event Type',
			'sector_id' => 'Sector',
			'seat_id' => 'Seat',
			'reservation_id' => 'Reservation ID',
			'reservation_number' => 'Reservation Number',
			'reservation_result' => 'Reservation Result',
			'sell_reservation_result' => 'SellReservation Result',
			'confirm_reservation' => 'Reservation Confirm',
			'set_reservation_payed' => 'Set Reservation Payed',
			'sector_name' => 'Sector Name',
			'tribune_name' => 'Tribune Name',
			'row_name' => 'Row Name',
			'seat_name' => 'Seat Name',
			'amount' => 'Amount',
			'upper' => 'Upper',
			'status' => 'Status',
			'barcode' => 'Barcode',
			'expiration_time' => 'Expiration Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('stadium_id',$this->stadium_id,true);
		$criteria->compare('event_id',$this->event_id,true);
		$criteria->compare('event_type',$this->event_id);
		$criteria->compare('sector_id',$this->sector_id,true);
		$criteria->compare('seat_id',$this->seat_id,true);
		$criteria->compare('reservation_id',$this->reservation_id,true);
		$criteria->compare('reservation_number',$this->reservation_number,true);
		$criteria->compare('reservation_result',$this->reservation_result,true);
		$criteria->compare('sell_reservation_result',$this->sell_reservation_result,true);
		$criteria->compare('confirm_reservation',$this->confirm_reservation,true);
		$criteria->compare('set_reservation_payed',$this->set_reservation_payed,true);
		$criteria->compare('sector_name',$this->sector_name,true);
		$criteria->compare('tribune_name',$this->tribune_name,true);
		$criteria->compare('row_name',$this->row_name,true);
		$criteria->compare('seat_name',$this->seat_name,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('upper',$this->upper);
		$criteria->compare('status',$this->status);
		$criteria->compare('barcode',$this->barcode);
		$criteria->compare('ticket_serial',$this->ticket_serial);
		$criteria->compare('ticket_number',$this->ticket_number);
		$criteria->compare('expiration_time',$this->expiration_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeValidate() {
        if( $this->isNewRecord ) {
            if( ! $this->set_reservation_payed ) {
                $this->set_reservation_payed = -1;
            }
            if( ! $this->reservation_result && $this->reservation_result != 0 ) {
                $this->reservation_result = -1;
            }
            if( ! $this->sell_reservation_result && $this->sell_reservation_result != 0 ) {
                $this->sell_reservation_result = -1;
            }
            if( ! $this->confirm_reservation ) {
                $this->confirm_reservation = -1;
            }
        }
        return parent::beforeValidate();
    }

    public function setAttrs( $attributes ) {
        $this->attributes = $attributes;
        if( ! $this->save() ) {
            return false;
        }
        return true;
    }

    public function setReservationPayed($reservationPayedResult, $order, $user) {
        $this->setAttrs( array( 'set_reservation_payed' => $reservationPayedResult ) );
        if ((bool)$reservationPayedResult) {
            $event = Event::model()->findByAttributes(array('event' => $order->basketOrders[0]->basket->event_id));
            SpartakMainHelper::sendCashierNotification($order, $user, 'Ошибка бронирвоания TicketSoft (ReservationPayed2: ' . $reservationPayedResult . ') (' . $event->name . ')');
            $this->setAttrs( array( 'status' => Basket::STATUS_TS_ERROR ) );
            return false;
        }
        return true;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Basket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function countBoughtForEvent( $eventId ) {
        return (int) Basket::model()->count( //TODO remove SPIKE
            'user_id = :uid AND event_id = :eid AND (status = :status1 OR status = :status2)',
            array(':uid' => Yii::app()->user->id, ':eid' => $eventId, ':status1' => Basket::STATUS_SOLD, ':status2' => Basket::STATUS_PAYED)
        );
    }
}
