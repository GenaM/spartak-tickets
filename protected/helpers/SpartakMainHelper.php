<?php
    class SpartakMainHelper {

        public static $basketNewTime      = 600;
        public static $basketReservedTime = 1800;

        /**
         * get events for events lists
         * @param $activeEventId
         * @return boolean
         */
        public static function getEvents( $activeEventId = null )    {
            $handler = isset( Yii::app()->controller ) ? Yii::app()->controller : Yii::app()->command;
            $events = Event::model()->findAll( '(end_time > :t1 OR end_time = :t2) AND is_active = 1', array( ':t1' => CSite::getNow(), ':t2' => "0000-00-00 00:00:00" ) );
            foreach( $events as $key => $event )    {
                $eventStartTime = new DateTime( $event['start_time'] );
                $eventStartTime->modify(  '-' . Yii::app()->params['events']['beforeEventStartTime'] . 's'  );
                if( time() > $eventStartTime->getTimestamp() && $event['type'] == TicketSoftHelper::TYPE_EVENT ) {
                    unset( $events[ $key ] );
                }
            }
            if( empty( $events ) )  {
                return true;
            }

            sort( $events );
            $activeEvent    = null;
            $nextEvent      = null;
            $prevEvent      = null;

            if( ! $activeEventId )    {
                $activeEvent    = $events[ key( $events ) ];
                $nextEvent      = isset( $events[1] ) ? $events[1] : null;
            } else {
                foreach( $events as $key => $event )  {
                    if( $event['event_id'] == $activeEventId )  {
                        $activeEvent    = $event;
                        $prevEvent      = isset( $events[ ( $key - 1 ) ] ) ? $events[ ( $key - 1 ) ] : null;
                        $nextEvent      = isset( $events[ ( $key + 1 ) ] ) ? $events[ ( $key + 1 ) ] : null;
                    }
                }
            }
            $handler->events = array(
                'all'      => $events,
                'prev'     => $prevEvent,
                'next'     => $nextEvent,
                'active'   => $activeEvent
            );
            $handler->session[ Yii::app()->params['basket']['eventsName'] ] = array(
                'events'    => $handler->events,
                'time'      => time()
            );
            return true;
        }


        public static function sendCashierNotification( Order $order , User $user , $topic ) {
            $handler = isset( Yii::app()->controller ) ? Yii::app()->controller : Yii::app()->command;
            $event = Event::model()->findByAttributes(array('event_id' => $order->basketOrders[0]->basket->event_id));
            CSite::sendMail( Yii::app()->params['cashierEmail'] , $handler->renderPartial( '//mails/cashier_notification' , array( 'order' => $order, 'user' => $user ) , true ) , $topic . ' (' . $event->name . ')' );
        }

        /**
         * Generate PDF tickets and sands it to owner mails
         * @param $order Order
         * @param $user User
         * @return bool
         */
        public static function sendTickets( Order $order , User $user )  {
            Yii::app()->mail->registerScripts();
            $files          = array();
            $mailTemplate   = '';
            $params         = array( 'fullName' => implode( ' ' , array( $user->last_name , $user->first_name , $user->middle_name ) ), 'notFullName' => implode( ' ' , array( $user->first_name , $user->middle_name ) ) );
            $handler        = isset( Yii::app()->controller ) ? Yii::app()->controller : Yii::app()->command;
            foreach( $order->basketOrders as $basketOrder )  {
                $basket                 = $basketOrder->basket;
                $params['eventType']    = $basket->event_type;
                if(! $basket->amount)   {
                    continue;
                }
                if( $basket->event_type == TicketSoftHelper::TYPE_EVENT )   {
                    $currentEvent = self::checkEvent( $basketOrder->basket->event_id );
                    if( $currentEvent['is_concert'] )   {
                        $mailTemplate = 'new_concert_order';
                    } else {
                        $mailTemplate = 'new_order';
                    }
                    if( ( $basket->status != Basket::STATUS_SOLD ) || ! $basket->barcode )  {
                        continue;
                    }

                    $pdfTemplate = $currentEvent['is_concert'] ? '//pdf/ticket-concert' : '//pdf/ticket-football';
                    if(file_exists(Yii::app()->basePath . '/views/pdf/unique/' . $basket->event_id . '.php'))   {
                        $pdfTemplate = '//pdf/unique/' . $basket->event_id;
                    }

                    $path = CSite::generatePDF(
                        $handler->renderPartial(
                            $pdfTemplate,
                            array_merge( array(
                                'barcode'   => $basket->barcode . '.gif',
                                'qrcode'    => 'qr_' . $basket->barcode . '.png',
                                'ticket'    => $basket,
                                'email'     => $user->email,
                                'phone'     => $user->phone,
                                'time'      => new DateTime( $order->date ),
                                'order'     => $order->rapidaRequest->ORDER,
                                'current'   => $currentEvent
                            ), $params ) ,
                            true
                        ) ,
                        $basket->barcode
                    );

                    if( file_exists( $path ) )  {
                        $files[] = Swift_Attachment::fromPath( $path , 'application/pdf' );
                    }
                } elseif( $basket->event_type == TicketSoftHelper::TYPE_SEASON )    {
                    $mailTemplate = 'new_season_order';
                    $path = CSite::generatePDF(
                        array(
                            $handler->renderPartial('//pdf/order_form', array('user' => $user), true),
                        ),
                        'orderForm' . $order->id,
                        false,
                        array(
                            'orientation' => 'Landscape',
                            'margin-bottom' => '1',
                            'margin-top' => '1',
                            'margin-left' => '1',
                            'margin-right' => '1',
                        )
                    );
                    if( $basket->status != Basket::STATUS_PAYED || !empty( $params['tickets'][$basket->id] ) )  {
                        continue;
                    }
                    if( file_exists( $path ) )  {
                        $files[] = Swift_Attachment::fromPath( $path , 'application/pdf' );
                    }
                }
                if( ! empty( $basket ) ) {
                    $params['tickets'][$basket->id] = $basket;
                }
            }
            if( empty( $params['tickets'] ) || empty( $mailTemplate ) ) {
                return false;
            }

            $event = Event::model()->findByAttributes(array('event_id' => $order->basketOrders[0]->basket->event_id));
            if( CSite::sendMail( array_merge( array( $user->email ), Yii::app()->params['cashierEmail'] ), $handler->renderPartial( '//mails/' . $mailTemplate , $params , true ) , 'Ваш заказ на tickets.spartak.com (' . $event->name . ')' , $files ) )  {
                self::writeLog(
                    $order->rapidaRequest->ORDER . "\t" .
                    $user->email . "\t" .
                    date( 'Y-m-d H:i:s' ) . "\t" .
                    'sent' ,
                    'log_' . date('Y-m-d') . '.txt' );
                return true;
            }
            self::writeLog(
                $order->rapidaRequest->ORDER . "\t" .
                $user->email . "\t" .
                date( 'Y-m-d H:i:s' ) . "\t" .
                'not sent' ,
                'log_' . date('Y-m-d') . '.txt' );
            return false;
        }

        /**
         * load basket for user from DB
         * @return null
         */
        public static function loadBasket() {
            if( Yii::app()->user->isGuest ) {
                return null;
            }

            $criteria = new CDbCriteria();
            $criteria->addCondition( 'user_id = ' . Yii::app()->user->id );
            $criteria->addInCondition( 'status' , array( Basket::STATUS_NEW, Basket::STATUS_OVERDUE, Basket::STATUS_EVENT_MISSING, Basket::STATUS_RESERVED, Basket::STATUS_ERROR, Basket::STATUS_BARCODE_ERROR, Basket::STATUS_LIMIT_ERROR, Basket::STATUS_TS_REMOVE_ERROR ) );
            $basket = Basket::model()->findAll( $criteria );

            Yii::app()->controller->basket = array();

            foreach( $basket as $item )  {
                if(! self::checkEvent($item->event_id)) {
                    $item->setAttrs( array( 'status' => Basket::STATUS_EVENT_MISSING ) );
                    $item->save();
                    continue;
                }
                if(CSite::getDatesDiff($item->expiration_time) >= (2 * 24 * 60 * 60)) {
                    continue;
                }
                if( ( $item->status == Basket::STATUS_NEW || $item->status == Basket::STATUS_RESERVED || $item->status == Basket::STATUS_CONFIRMED ) && $item->expiration_time <= CSite::getNow() )  {
                    $item->attributes = array( 'status' => Basket::STATUS_OVERDUE );
                    $item->save();
                }
                Yii::app()->controller->basket[ $item->event_id ]['tickets'][] = $item;
            }

        }

        /**
         * @param $orderId
         */
        public static function addBasketOrders( $orderId )   {
            foreach( Yii::app()->controller->basket as $event ) {
                foreach( $event['tickets'] as $ticket ) {
                    $basketOrder = new BasketOrder();
                    $basketOrder->attributes = array(
                        'order_id'  => $orderId,
                        'basket_id' => $ticket->id
                    );
                    $basketOrder->save();
                }
            }
        }


        /**
         * Check, that number of tickets in basket <= max
         * @param $type
         * @return bool|int
         */
        public static function checkTicketsNumberInBasket( $type = TicketSoftHelper::TYPE_EVENT , $eventId ) {
            if( empty( Yii::app()->controller->basket ) )   {
                Yii::app()->controller->basket = self::loadBasket();
            }

            if( count( Yii::app()->controller->basket ) > 0 && empty( Yii::app()->controller->basket[ $eventId ] ) ) {
                return 4;
            }

            $boughtTicketsNumber = Basket::countBoughtForEvent( $eventId );
            if( ($boughtTicketsNumber) >= Yii::app()->params['basket']['maxTicketsInBasket'] ) {
                return 2;
            }

            if( empty( Yii::app()->controller->basket ) )   {
                return true;
            }
            $number = 0;
            foreach( Yii::app()->controller->basket as $event ) {
                foreach( $event['tickets'] as $ticket ) {
                    if( $ticket['event_type'] == TicketSoftHelper::TYPE_SEASON )   {
                        return 1;
                    }
                    if( $ticket['status'] == Basket::STATUS_RESERVED || $ticket['status'] == Basket::STATUS_PAYED ) {
                        return 3;
                    }
                }
                $eventTicketsNumber     = (int) count( $event['tickets'] );
                if( ($eventTicketsNumber + $boughtTicketsNumber) >= Yii::app()->params['basket']['maxTicketsInBasket'] ) {
                    return 2;
                }
                $number += count( $event['tickets'] );
            }
            if( $type == TicketSoftHelper::TYPE_SEASON && $number == 0 )    {
                return 0;
            }
            return $number < Yii::app()->params['basket']['maxTicketsInBasket'] ? 0 : 1;
        }

        /**
         * @param $eventId
         * @param $seatId
         * @return bool
         */
        public static function setTicketPayed( $eventId , $seatId ) {
            $basket = Basket::model()->findByAttributes( array(
                'user_id'   => Yii::app()->user->id,
                'event_id'  => $eventId,
                'seat_id'   => $seatId
            ) );

            if( empty( $basket ) || $basket->status != Basket::STATUS_RESERVED )   {
                return false;
            }

            $basket->status = Basket::STATUS_PAYED;
            return $basket->save();
        }

        /**
         * @return array
         */
        public static function getActiveSeats()   {
            $result = array();
            if( ! Yii::app()->controller->basket )   {
                return $result;
            }
            foreach( Yii::app()->controller->basket as $eventId => $event )    {
                foreach( $event['tickets'] as $ticket ) {
                    $result[ $eventId ][ $ticket->stadium_id ][ $ticket->sector_id ][] = $ticket->seat_id;
                }
            }
            return $result;
        }

        /**
         * @param $eventId
         * @return bool
         */
        public static function checkEvent( $eventId )   {
            $handler = isset( Yii::app()->controller ) ? Yii::app()->controller : Yii::app()->command;
            if(empty($handler->events['all']))  {
                return false;
            }
            foreach( $handler->events['all'] as $event )  {
                if( $eventId == $event['event_id'] )  {
                    return $event;
                }
            }

            return false;
        }

        /**
         * @param $string
         * @param $logFileName
         * @return bool
         */
        public static function writeLog($string, $logFileName, $rewrite = false)   {
            $file = fopen( CSite::getTempDir() . $logFileName , $rewrite ? 'w' : 'a');
            fwrite(
                $file,
                date('Y-m-d H:i:s') . "\t\t" . ( empty( $_SERVER['REMOTE_ADDR'] ) ? 'localhost' : $_SERVER['REMOTE_ADDR'] ) . PHP_EOL . $string . PHP_EOL
            );
            fclose( $file );
            return true;
        }


    }