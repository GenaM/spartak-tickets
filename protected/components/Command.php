<?php
    class Command extends CConsoleCommand {


        public $session;

        /**
         * @var null | TicketSoftHelper
         */
        protected $tickets;

        public $events = null;

        public $loadEvents = true;

        public function run($args)
        {
            $this->tickets = new TicketSoftHelper();

            if( $this->loadEvents ) {
                SpartakMainHelper::getEvents( Yii::app()->request->getParam( 'eventId' ) );
            }

            parent::run($args);
        }

        public function renderPartial($template,$data=null,$return=true){
            $path = Yii::getPathOfAlias('application.views').$template.'.php';
            if(!file_exists($path)) throw new Exception(404);
            return $this->renderFile($path, $data, $return);
        }

    }