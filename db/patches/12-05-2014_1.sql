ALTER TABLE  `user` ADD  `email` TEXT NOT NULL;

ALTER TABLE  `basket` ADD  `reservation_number` INT NOT NULL AFTER  `reservation_id`;

ALTER TABLE  `basket` ADD  `status` INT NOT NULL;

ALTER TABLE  `basket` ADD  `date` DATETIME NOT NULL;

ALTER TABLE  `basket` DROP  `error` , DROP  `confirmed` , DROP  `payed`;