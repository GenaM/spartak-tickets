<div class="section">
    <?php if( $this->events['all'] ) { ?>
        <?php $cnt = 0 ?>
        <?php foreach( $this->events['all'] as $event ) { ?>
            <?php if( $cnt == 0 ) { ?>
                <section class="game-preview" data-href="<?php echo Yii::app()->createUrl( 'tickets/stadium' , array( 'eventId' => $event['event_id'] ) ) ?>">
                    <header class="game-preview__header">
                        <span class="stadium-name">Стадион <?php echo $event['stadium_name'] ?></span>
                        <?php $date = new DateTime( $event['start_time'] ); ?>
                        <span class="date"><?php echo $date->format( 'd-m-Y' ) ?></span>
                        <span class="time"><?php echo $date->format( 'H:i' ) ?></span>
                    </header>

                    <div class="game-preview__body clearfix">
                        <div class="f-team">
                            <img class="team-logo" src="/i/teams/spartak-game.png">
                        </div>

                        <div class="f-team">
                            <img class="team-logo team-logo_position_right" src="/i/teams/anzhi-game.png">
                        </div>

                        <div class="game-preview__title">
                            <span class="game-preview__team">ФК «Спартак»</span>&mdash;<span class="game-preview__team">ФК «Анжи»</span>
                        </div>
                    </div>
                </section>
            <?php } else { ?>
                <?php if( $cnt == 1 ) { ?>
                    <ul class="preview-list clearfix">
                <?php } ?>
                    <li class="preview-list__item" data-href="<?php echo Yii::app()->createUrl( 'tickets/stadium' , array( 'eventId' => $event['event_id'] ) ) ?>">
                        <span class="preview-list__team">
                            <img class="team-logo" src="/i/teams/spartak-vs.png">
                        </span>

                        <span class="preview-list__team">
                            <img class="team-logo" src="/i/teams/fcv-vs.png">
                        </span>
                    </li>
                <?php if( $cnt == count( $this->events['all'] ) ) { ?>
                    </ul>
                <?php } ?>
            <?php } ?>
            <?php $cnt++ ?>
        <?php } ?>
    <?php } ?>
</div>