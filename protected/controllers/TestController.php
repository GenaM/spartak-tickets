<?php
    class TestController extends Controller {

        public function actionOrderProcessor( $id ) {
            $order = Order::model()->findByPk($id);
            OrderHelper::processPayedOrder($order, $this->tickets);
            Yii::app()->end();
        }

        public function actionSendTickets( $id ) {
            $order = Order::model()->find( new CDbCriteria( array(
                'condition' => 't.id = :id',
                'params'    => array( ':id' => $id ),
                'with' => array(
                    'basketOrders' => array(
                        'with' => array( 'basket' )
                    )
                )
            ) ) );

            $order->rapidaRequest = RapidaRequest::model()->findByPk( $order->request_id );

            $user = User::model()->findByPK( $order->user_id );

            if( SpartakMainHelper::sendTickets( $order , $user ) )  {
                $order->attributes = array(
                    'status' => Order::STATUS_SENT
                );
            }
        }

        public function actionGeneratePDF($id) {
            $order = Order::model()->find( new CDbCriteria( array(
                'condition' => 't.id = :id',
                'params'    => array( ':id' => $id ),
                'with' => array(
                    'basketOrders' => array(
                        'with' => array( 'basket' )
                    )
                )
            ) ) );

            $order->rapidaRequest = RapidaRequest::model()->findByPk( $order->request_id );

            $user = User::model()->findByPK( $order->user_id );

            Yii::app()->mail->registerScripts();
            $files          = array();
            $eventType      = null;
            $params         = array( 'fullName' => implode( ' ' , array( $user->last_name , $user->first_name , $user->middle_name ) ) );
            foreach( $order->basketOrders as $basketOrder )  {
                $basket         = $basketOrder->basket;
                $eventType      = $basket->event_type;
                if( $basket->event_type == TicketSoftHelper::TYPE_EVENT )   {
                    if( $basket->status != Basket::STATUS_SOLD || ! $basket->barcode )  {
                        continue;
                    }
	                $barcode = CSite::generateBarcode( $basket->barcode );

                    $path = CSite::generatePDF(
                        Yii::app()->controller->renderPartial(
                            '//pdf/ticket' ,
                            array(
                                'barcode'   => $barcode . '.gif',
                                'ticket'    => $basket,
                                'fullName'  => implode( ' ' , array( $user->last_name , $user->first_name , $user->middle_name ) ),
                                'email'     => $user->email,
                                'phone'     => $user->phone,
                                'time'      => new DateTime( $order->date ),
                                'order'     => $order->rapidaRequest->ORDER,
                                'current'   => SpartakMainHelper::checkEvent( $basketOrder->basket->event_id )
                            ) ,
                            true
                        ) ,
                        $basket->barcode,
                        0
                    );
                    if( file_exists( $path ) )  {
                        $files[] = Swift_Attachment::fromPath( $path , 'application/pdf' );
                    }

                    echo CSite::sendMail( array( 'ibrainraider@gmail.com', 'gennadiy.mukhay@gmail.com' ), '' , 'test' , $files );
                }
            }
        }

        public function actionCheckRapidaCheck($id)    {
            $orders = Order::model()->findAll( new CDbCriteria( array(
//                'condition' => 't.status = :status AND t.request_id IS NOT NULL AND t.response_id IS NULL',
                'condition' => 't.id = :id',
//                'params'    => array( ':status' => Order::STATUS_NEW ),
                'params'    => array( ':id' => $id ),
            ) ) );

            if( ! $orders ) {
                echo 'any orders';
                Yii::app()->end();
            }

            foreach( $orders as $order ) {
                $rapidaRequest = RapidaRequest::model()->findByPK($order->request_id);
                $params     = $rapidaRequest->attributes;
                unset( $params['id'] );
                var_dump( RapidaHelper::checkTest( $params ) ); Yii::app()->end();
            }
        }

        public function actionCheckRapidaCommandCheck() {
            $orders = Order::model()->findAll( new CDbCriteria( array(
                'condition' => 'status = 2',
                'params'    => array(),
                'order'     => 'id DESC'
            ) ) );

            var_dump( count( $orders ) );

            if( ! $orders ) {
                Yii::app()->end();
            }

            foreach( $orders as $order ) {
                var_dump( $order->attributes );
                $rapidaRequest = RapidaRequest::model()->findByPK($order->request_id);
                $params = $rapidaRequest->attributes;
                unset($params['id']);

                $response = (array)json_decode(RapidaHelper::checkTest($params));
                var_dump( $params, PHP_EOL, $response , PHP_EOL );
                if (!empty($response) && empty($response['ERROR'])) {
                    RapidaHelper::processRapidaResponse($response, $this->tickets);
                } elseif(empty($response)) {
                    $order->status = Order::STATUS_CANCELED_RAPIDA;
                    $order->save();
                } else {
                    echo 'Error. order ID' . $order->id . PHP_EOL ;
                }
            }
        }

        public function actionClearReservation($id, $cardNumber) {
            $item = Basket::model()->findByPK($id);
            var_dump( $this->tickets->ClearReservation( $item->stadium_id , $cardNumber , $item->reservation_number ) );
        }

        public function actionGetEvents() {
            var_dump($this->tickets->getEvents());
        }

        public function actionCan() {
            $SUH = new SpartakUsersHelper();
            var_dump( $SUH->canLogin( 'chernyaevgleb@gmail.com', '3819698' ) );
        }

        public function actionRead($name) {
            echo '<pre>' . file_get_contents( CSite::getTempDir() . $name ) . '</pre>';
        }

        public function actionOrder() {
            RapidaHelper::processRapidaResponse( array(
                'AMOUNT' => 10,
                'CURRENCY' => 'RUB',
                'ORDER' => '09281510578820733084',
                'MERCH_NAME' => 'SPARTAK',
                'MERCHANT' => '000792224009505',
                'TERMINAL' => '24009505',
                'EMAIL' => 'spartakbilet@yandex.ru',
                'TRTYPE' => 1,
                'TIMESTAMP' => 20140928111408,
                'NONCE'     => 'd5440962b77cbb01ca324b5a2f88f7b9',
                'BACKREF' => 'http://tickets.spartak.com/payed',
                'RESULT' => 0,
                'RC' => '00',
                'RCTEXT' => 'Approved',
                'AUTHCODE' => '662295',
                'RRN'   => '427117183917',
                'INT_REF' => '43CC66E3E1AE7D42',
                'P_SIGN' => '19D1B39A809DEDE622E69C4DEB973AA33721C10C'
            ), $this->tickets );
        }

        public function actionCashierSend($id) {
            $this->loadBasket = false;

            $order = Order::model()->find( new CDbCriteria( array(
                'condition' => 't.id = :id',
                'params'    => array( ':id' => $id ),
                'with' => array(
                    'basketOrders' => array(
                        'with' => array( 'basket' )
                    )
                )
            ) ) );

            $order->rapidaRequest = RapidaRequest::model()->findByPk( $order->request_id );

            $user = User::model()->findByPK( $order->user_id );

            SpartakMainHelper::sendCashierNotification( $order , $user , 'Заказ №' . $order->id . ' оплачен' );
        }


    }