<?php
/**
 * @var $model LoginForm
 */
?>

<div class="section">
    <section class="widget">
        <header class="widget__header">Регистрация</header>
        <div class="widget__body">

            <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'form login-form'
                    )
                ));
            ?>

                <?php echo $form->textField($model, 'username', array('class' => 'input control-size-m', 'tabindex' => 1 , 'placeholder' => 'ЛОГИН' )); ?>
                <?php echo $form->passwordField($model, 'password', array('class' => 'input control-size-m', 'tabindex' => 2 , 'placeholder' => 'ПАРОЛЬ' )); ?>
                <button class="btn control-size-m" type="submit" tabindex="3">Войти</button>
            <?php $this->endWidget(); ?>

                <div class="login-actions">
                    <a class="btn control-size-m" href="http://lk.spartak.com/" target="_blank">Зарегистрироваться</a>

                    <p class="text">Если у вас нет аккаунта,<br>вы можете пройти регистрацию или</p>

                    <a class="btn control-size-m" href="http://lk.spartak.com/profile/reset/" target="_blank">Восстановить пароль</a>
                </div>

            <?php if( $model->hasErrors() )  { ?>
                <?php $errors = $model->getErrors(); ?>
                <div class="error-label">
                    <i class="icon error-icon"></i>
                        <?php $error = array_shift( $errors ) ?>
                        ОШИБКА: <span class="error-label__text"><?php echo $error[0] ?> </span>
                </div>
            <?php } ?>

        </div>
    </section>
</div>