<?php if( Yii::app()->user->isGuest && Yii::app()->request->url != Yii::app()->createUrl( 'site/login' ) )   { ?>
    <section class="widget">
        <header class="widget__header">Авторизация</header>

        <div class="widget__body">

            <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'action' => Yii::app()->createUrl( 'site/login' ),
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'form login-form'
                    )
                ));
            ?>

                <?php echo $form->textField($model, 'username', array('class' => 'input control-size-m', 'tabindex' => 1 , 'placeholder' => 'ЛОГИН' )); ?>
                <?php echo $form->passwordField($model, 'password', array('class' => 'input control-size-m', 'tabindex' => 2 , 'placeholder' => 'ПАРОЛЬ' )); ?>
                <button class="btn control-size-m" type="submit" tabindex="3">Войти</button>

                <p class="text">или пройдите <a class="link red-link" href="http://lk.spartak.com/" target="_blank">регистрацию</a></p>

            <?php $this->endWidget(); ?>

            <?php if( $model->hasErrors() )  { ?>
                <?php $errors = $model->getErrors(); ?>
                <div class="error-label">
                    <i class="icon error-icon"></i>
                    ОШИБКА: <span class="error-label__text"><?php echo $errors['username'][0] ?> </span>
                </div>
            <?php } ?>

        </div>
    </section>
<?php } elseif( ! Yii::app()->user->isGuest ) { ?>
    <section class="widget">
        <header class="widget__header">Кабинет пользователя</header>

        <div class="widget__body">
            <div class="user-info">
                <p class="text">Вы вошли на сайт как</p>

                <p class="user-info__name">
                    <?php echo ! empty( Yii::app()->user->lastName ) ? Yii::app()->user->lastName : null ?>
                    <?php echo ! empty( Yii::app()->user->firstName ) ? mb_substr( Yii::app()->user->firstName , 0 , 1 ) . '.' : null ?>
                    <?php echo ! empty( Yii::app()->user->middleName ) ? mb_substr( Yii::app()->user->middleName , 0 , 1 ) . '.' : null ?>
                </p>

                <a class="link red-link" href="<?php echo Yii::app()->createUrl( 'site/logout' ) ?>">Выход</a>
            </div>
        </div>
    </section>
<?php } ?>