<?php
    $error      = $this->basketList ? false : true;
    $canBePayed = true;
?>

<section class="widget cart">
    <header class="widget__header">Корзина
        <span class="cart__count"><span class="cart__num"><?php echo $this->totalTickets ?></span>&nbsp;<?php echo $this->totalTicketsLabel ?></span>
    </header>

    <div class="widget__body">
        <form class="cart__form" action="#">
            <?php if( $this->basketList ) { ?>
                <?php foreach( $this->basketList as $eventId => $event ) { ?>
                    <div class="cart__group" data-event="<?php echo $eventId ?>">
                        <div class="cart-caption">
                            <span class="game-title"><?php echo $event['name'] ?></span>
                        </div>
                        <table class="cart-table">
                            <thead class="cart-table__head">
                            <tr class="cart-table__row">
                                <th class="cart-table__cell">сектор</th>
                                <th class="cart-table__cell">ряд</th>
                                <th class="cart-table__cell">место</th>
                                <th class="cart-table__cell">цена</th>
                            </tr>
                            </thead>
                            <tbody class="cart-table__body">
                            <?php foreach( $event['tickets'] as $ticket )   { ?>
                                <tr class="cart-table__row <?php echo (in_array($ticket->status, array(Basket::STATUS_ERROR, Basket::STATUS_OVERDUE, Basket::STATUS_EVENT_MISSING, Basket::STATUS_LIMIT_ERROR, Basket::STATUS_TS_REMOVE_ERROR))) ? 'warning' : null ?>" data-seat="<?php echo $ticket->seat_id ?>">
                                    <td class="cart-table__cell">
                                        <?php if( $ticket->status == Basket::STATUS_ERROR ) { ?>
                                            <?php $error = true; ?>
                                            <i class="icon warning-icon" title="Не удается забронировать выбранное Вами место. Выберите другое. <?php echo $ticket->reservation_result ? '#' . $ticket->reservation_result : '' ?>"></i>
                                        <?php } elseif( $ticket->status == Basket::STATUS_OVERDUE ) { ?>
                                            <?php $error = true; ?>
                                            <i class="icon warning-icon" title="Время бронирования истекло. Удалите билет из корзины."></i>
                                        <?php } elseif( $ticket->status == Basket::STATUS_EVENT_MISSING ) { ?>
                                            <?php $error = true; ?>
                                            <i class="icon warning-icon" title="Время бронирования уже истекло. Удалите билет из корзины."></i>
                                        <?php } elseif( $ticket->status == Basket::STATUS_BARCODE_ERROR ) { ?>
                                            <?php $error = true; ?>
                                            <i class="icon warning-icon" title="Не получилось получить информацию о билете. Обратитесь к администрации."></i>
                                        <?php } elseif( $ticket->status == Basket::STATUS_LIMIT_ERROR ) { ?>
                                            <?php $error = true; ?>
                                            <i class="icon warning-icon" title="По Вашей карте <?php echo Yii::app()->user->cardNumber ?> уже куплено максимальное количество билетов на мероприятие."></i>
                                        <?php } elseif( $ticket->status == Basket::STATUS_TS_REMOVE_ERROR ) { ?>
                                            <?php $error = true; ?>
                                            <i class="icon warning-icon" title="Не получается удалить бронь."></i>
                                        <?php } ?>
                                        <?php echo $ticket->sector_name ?>
                                    </td>
                                    <td class="cart-table__cell"><?php echo $ticket->row_name ?></td>
                                    <td class="cart-table__cell"><?php echo $ticket->seat_name ?></td>
                                    <td class="cart-table__cell">
                                        <span class="sum">
                                            <?php echo $ticket->amount ?>
                                            <?php
                                                $expirationTime = new DateTime($ticket->expiration_time);
                                                $now            = new DateTime(CSite::getNow());
                                                if( $ticket->status == Basket::STATUS_OVERDUE || $ticket->status == Basket::STATUS_LIMIT_ERROR || $ticket->status == Basket::STATUS_EVENT_MISSING || $ticket->status == Basket::STATUS_TS_REMOVE_ERROR || $ticket->status == Basket::STATUS_NEW || $ticket->status == Basket::STATUS_ERROR || ( $ticket->status == Basket::STATUS_RESERVED && $expirationTime < $now ) ) {
                                            ?>
                                                <button class="rm-btn" type="button" data-removeurl="<?php echo Yii::app()->createUrl( 'site/removeFromBasket' , array(
                                                    'eventId'       => $eventId,
                                                    'seatId'        => $ticket->seat_id,
                                                ) ) ?>"></button>
                                            <?php } else {
                                                $canBePayed = false;
                                            } ?>
                                        </span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            <?php } ?>

            <?php if( ! empty($this->message) ) { ?>
                <div class="red-text"><?php echo $this->message ?></div>
            <?php } elseif(!$canBePayed) { ?>
                <div class="red-text">
                    Повторная оплата не может быть осуществлена.
                </div>
            <?php } elseif( empty($this->message) && ! $error ) { ?>
                <div class="cart__action">
                    <p class="text">Заказ: <span class="cart__num"><?php echo $this->totalTickets ?>&nbsp;</span>шт. на сумму <span class="cart__sum"><?php echo $this->totalAmount ?>&nbsp;</span>руб.</p>
                    <a href="<?php echo Yii::app()->createUrl( 'tickets/reservation' ) ?>" class="btn btn-primary control-size-s" type="button">Купить</a>
                </div>
            <?php } ?>
            <div>
                <span class="red-text">Внимание!</span> После перехода на страницу оплаты вы не сможете удалить билеты в течение <?php echo Basket::SECOND_STEP_RESERVATION_TIME / 60 ?> минут
            </div>
        </form>
    </div>
</section>