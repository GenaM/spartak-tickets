<?php
    class SpartAdminController extends Controller {
        public $showBasket  = false;
        public $loadBasket  = false;
        public $loadTickets = true;

        public $layout = '//layouts/spartAdmin';


        public function beforeAction( $action )  {
           if(! in_array($_SERVER['REMOTE_ADDR'], array('77.50.236.4', '54.246.91.77', '91.197.10.251', '77.50.4.153', '95.165.189.145', '95.165.189.147', '83.220.45.58'))) {
               throw new CHttpException( 404 );
           }

            return parent::beforeAction( $action );
        }

        public function actionIndex() {
            $this->render('index', array('data' => Event::model()->findAll(array('order' => 'start_time DESC'))));
        }

        public function actionViewOrders( $status, $page = 0 ) {
            $onPage = 350;
            $criteria = new CDbCriteria();
            $criteria->addCondition('date > "2014-12-31 23:59:59"');
            $criteria->addCondition('t.status = ' . $status);

            $criteria->with = array('user','basketOrders' => array('with' => 'basket'));

            $pages = ceil(Order::model()->count($criteria) / $onPage );

            $criteria->order = 'date DESC';
            $criteria->offset = $page * $onPage;
            $criteria->limit = $onPage;

            $orders = Order::model()->findAll($criteria);
            $this->render('viewOrders',array('data' => $orders, 'page' => $page, 'pages' => $pages, 'status' => $status));
        }

        public function actionViewOrdersUnFull( $status = Order::STATUS_SENT, $page = 0 ) {
            $onPage = 350;
            $criteria = new CDbCriteria();
            $criteria->with = array(
                'user',
                'basketOrders' => array(
                    'condition' => 'sell_reservation_result <> 0 AND set_reservation_payed <> -1',
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'with' => array(
                        'basket' => array(
                            'together' => true,
                            'joinType' => 'INNER JOIN',
                        )
                    )
                )
            );

            $criteria->addCondition('date > "' . CSite::getNow(-(10 * 24 * 60 * 60)) . '"');
            $criteria->addCondition('t.status = ' . Order::STATUS_SENT);

            $pages = ceil(Order::model()->count($criteria) / $onPage );

            $criteria->order = 'date DESC';
            $criteria->offset = $page * $onPage;
            $criteria->limit = $onPage;

            $orders = Order::model()->findAll($criteria);
            $this->render('viewOrders',array('data' => $orders, 'page' => $page, 'pages' => $pages, 'status' => $status));
        }

        public function actionUpdateEvent( $id ) {
            $event = Event::model()->findByPk( $id );

            if(isset($_POST['Event']))
            {
                $event->attributes=$_POST['Event'];
                $event->save();
            }

            $this->render('updateEvent', array('model' => $event));
        }

        public function actionUpdateEvents() {
            shell_exec( 'nohup php ' . Yii::app()->basePath . '/yiic ts saveEvents > /dev/null & echo $!' );
        }

        public function actionUpdateSectors($id = null) {
            shell_exec( 'nohup php ' . Yii::app()->basePath . '/yiic ts saveSectors --id=' . $id . ' > /dev/null & echo $!' );
        }

        public function actionClearReservations($ignoreTS = false, $hours = 1) {
            shell_exec( 'nohup php ' . Yii::app()->basePath . '/yiic ts clearReservations --ignoreTS=' . (int) $ignoreTS . ' --hours=' . (int) $hours . ' > /dev/null & echo $!' );
        }

        public function actionUploadImage($id, $big)
        {
            Yii::import("ext.EAjaxUpload.qqFileUploader");
            $folder = Yii::app()->basePath . '/../web/uploads/' . $id . '/';
            if (!file_exists($folder)) {
                mkdir($folder);
            }
            $allowedExtensions = array("jpg","png","gif");
            $sizeLimit = 10 * 1024 * 1024;
            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($folder);

            $result['path'] = '/uploads/' . $id . '/' . $result['filename'];

            $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
            $fileName = $result['filename']; //GETTING FILE NAME
            $model = Event::model()->findByPK($id);
            if( $big ) {
                $model->big_banner = $fileName;
            } else {
                $model->little_banner = $fileName;
            }
            if($model->save())
                echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
            else
                echo json_encode( false );
        }

        public function actionOrderProcessor( $id ) {
            $order = Order::model()->findByPk($id);
            OrderHelper::processPayedOrder($order, $this->tickets);
            Yii::app()->end();
        }

        public function actionAdditional() {
            $params = array();
            if(file_exists($CRLog = CSite::getTempDir() . 'clearReservations.log')) {
                $params['CRLog'] = file_get_contents($CRLog);
            }
            $this->render('additional', $params);
        }


        public function actionTestQR($id) {
            $order = Order::model()->findByPk($id);
            OrderHelper::processPayedOrder($order, $this->tickets, true);
            Yii::app()->end();
        }

        public function actionTestCash()    {
            $date = new DateTime();
            $date->modify('+ 15 min');


            Cash24Helper::createInvoice(array(
                'amount' => '1',
                'currency' => 'RUB',
                'email' => 'Gennady.Mukhai@fors.ru',
                'description' => 'test',
                'order' => $date->format(DateTime::ATOM),
                'success' => 'http://tickets.spartak.com/tickets/cash24success',
                'cancel' => 'http://tickets.spartak.com/tickets/cash24cancel',
                'callback' => 'http://tickets.spartak.com/tickets/cash24callback',
                'expires' => $date->format(DateTime::ATOM),
                'phone' => '+79787817579',
                'auth' => Cash24Helper::$shopID
            ));

            Yii::app()->end();
        }
    }