<div class="section">
    <article class="article">
        <h1 class="article-title">Как добраться до стадиона</h1>

        <p class="text">В сезоне-14/15 ФК «Спартак-Москва» проводит домашние матчи на стадионе «Открытие Арена».</p>

        <ul class="address" itemscope itemtype="http://schema.org/Organization">
            <li class="address__item" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                Адрес стадиона:&nbsp;<span itemprop="streetAddress">Москва, Волоколамское шоссе, владение 67.</span>
            </li>
            <li class="address__item" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                Ближайшая станция метро: &nbsp;<span itemprop="streetAddress">«Тушинская»</span>
            </li>
        </ul>
	<p class="text">В июле 2014 года будет открыта станция метро «Спартак».</p>
	<p><img src="/i/pages/stadium-address.jpg"></p>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d17931.92129422686!2d37.441339629638655!3d55.81945106741464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54879fb49c793%3A0xfc3b0d63bfbcc532!2z0KHRgtCw0LTQuNC-0L0g0KHQv9Cw0YDRgtCw0Lo!5e0!3m2!1sru!2s!4v1403847685504" width="575" height="350" frameborder="0" style="border:0"></iframe>
        </div>
    </article>
</div>