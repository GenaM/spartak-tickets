<?php

class OrderHelper
{
    public static function processPayedOrder(Order $order, $ticketsClient, $dev = false)
    {
        if (empty($order->rapidaRequest)) {
            $order->rapidaRequest = RapidaRequest::model()->findByPk($order->request_id);
        }
        if (empty($order->user)) {
            $order->user = User::model()->findByPk($order->user_id);
        }
        if (empty($order->basketOrders)) {
            $order->basketOrders = User::model()->findAllByAttributes(array('order_id' => $order->id));
        }
        if (!$order->response_id) {
            return false;
        }
        foreach ($order->basketOrders as $basketOrder) {
            $basket = $basketOrder->basket;
            if ($basket->set_reservation_payed != 0) {
                $reservationPayedResult = $ticketsClient->SetReservationPayed($basket->stadium_id, $basket->reservation_id, $basket->amount, 1 . $basket->id);
                $basket->attributes = array(
                    'set_reservation_payed' => $reservationPayedResult
                );
                $basket->save();
                if ((bool)$reservationPayedResult) {
                    $event = Event::model()->findByAttributes(array('event_id' => $basket->event_id));
                    SpartakMainHelper::sendCashierNotification($order, $order->user, 'Ошибка бронирвоания TicketSoft (ReservationPayed2: ' . $reservationPayedResult . '), (' . $event->name . '}');
                    $basket->attributes = array(
                        'status' => Basket::STATUS_TS_ERROR
                    );
                    $basket->save();
                    continue;
                }
            }

            if ($basket->event_type == TicketSoftHelper::TYPE_EVENT) {
                $reservationInfo = $ticketsClient->SellReservation($basket->stadium_id, $basket->reservation_id, 2 . $basket->id);
                if (is_array($reservationInfo) && !empty($reservationInfo['barcode'])) {
                    $barcode = CSite::generateBarcode($reservationInfo['barcode']);
                    if ($barcode) {
                        $basket->attributes = array(
                            'barcode' => $barcode,
                            'ticket_number' => $reservationInfo['ticketNumber'],
                            'ticket_serial' => $reservationInfo['ticketSerial'],
                            'sell_reservation_result' => 0,
                            'status' => Basket::STATUS_SOLD,
                        );
                    }
                    $qrcode = CSite::generateQRcode($reservationInfo['barcode']);
                } else {
                    $basket->attributes = array(
                        'status' => Basket::STATUS_BARCODE_ERROR,
                        'sell_reservation_result' => $reservationInfo
                    );
                }
                $basket->save();
            }
        }

        if (SpartakMainHelper::sendTickets($order, $order->user)) {
            $order->attributes = array(
                'status' => Order::STATUS_SENT
            );
        } else {
            $order->attributes = array(
                'status' => Order::STATUS_ERROR_PAYED
            );
        }
        $order->save();
        return true;
    }

}