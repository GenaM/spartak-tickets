<!DOCTYPE html>
<html class="page">
<head>
<title>Spartak Tickets | Билет</title>
<meta charset="utf-8"/>
<meta name="author" content="Геннадий Мухай, Андрей Сычев, Елена Мусатова, Владимир Кичин, Клим Миронов"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<!-- Проектные стили -->
<style>
*
{
    margin: 0;
    padding: 0;

    vertical-align: baseline;

    -webkit-print-color-adjust: exact;
}

img
{
    border: 0;
}

ul
{
    list-style: none;
}

table
{
    border-spacing: 0;
    border-collapse: collapse;
    width: 100%;
}

.page
{
    font: 13px Arial, sans-serif;

    position: relative;

    height: 100%;
    width: 1000px
;

    background: #fff;
}

.page__body
{
    background: none;
}

.ticket
{
    font: 10px Arial, sans-serif;

    width: 1070px;
    margin: auto;
}

.ticket__header
{
    overflow: hidden;

    margin: 0 0 5px;
}

.ticket-title
{
    font: 18px Arial, sans-serif;

    padding: 0 0 4px;

    text-transform: uppercase;
}

.print-logo
{
    float: left;

    width: 271px;
    height: 114px;
}

.ticket .warn
{
    float: right;

    text-align: right;

    color: #e5173f;
}

.list_size_s
{
    float: left;

    width: 50%;
    margin: 10px 0 0;
}

.ticket .list__item
{
    padding: 0 0 0 6px;
}

.ticket .list__item:before
{
    position: relative;
    left: -5px;

    content:  '–';
}

.ticket .text
{
    overflow: hidden;

    padding: 4px;

    background: #fff;
}

.red-text
{
    color: #e5173f;
}

.warn__1line
{
    font: bold 24px Arial, sans-serif;
}

.warn__2line
{
    font-size: 18px;
}

.warn__3line
{
    font-size: 18px;

    margin: 45px 0 0;

    text-transform: uppercase;
}

.info-block
{
    margin-bottom: 5px;
    padding: 10px 12px;
    width: 1000px;
    background: #fff;
}

.ticket__body .info-block:first-child
{
    padding: 0;
}

.info-block_type_important .ticket-title
{
    color: #e5173f;
}

.attention-text
{
    font: bold 15px Arial, sans-serif;
}

.ticket__wrapper
{
    position: relative;

    height: 408px;
    margin-bottom: 22px;
    padding: 15px 0 0;

    border-top: 1px dotted #000;
    border-bottom: 1px dotted #000;
}

.ticket__wrapper:before,
.ticket__wrapper:after
{
    font-size: 10px;
    font-style: italic;

    position: absolute;
    left: 3px;
}

.ticket__wrapper:before
{
    top: 1px;

    content: 'линия сгиба №2';
}

.ticket__wrapper:after
{
    bottom: -15px;

    content: 'линия сгиба №1';
}

.ticket__control
{
    margin-bottom: 12px;

    border-bottom: 1px dotted #000;
}

.ticket__control .ticket-title
{
    padding: 0 0 0 24px;
}

.ticket__control .text
{
    float: left;

    width: 733px;
    height: 353px;
    margin-right: 12px;
}

.ticket__control .list__item
{
    line-height: 9px;

    padding: 1px 0 0 6px;
}

.ticket-table
{
    width: 100%;
}

.ticket-table__cell
{
    border-collapse: collapse;

    border-top: 2px solid #ccc;
    border-right: 12px solid #ccc;
    border-bottom: 2px solid #ccc;
    border-left: 12px solid #ccc;
}

.ticket-table__row:first-child .ticket-table__cell
{
    border-top: 8px solid #ccc;
}

.ticket-table__row:last-child .ticket-table__cell
{
    border-bottom: 12px solid #ccc;
}

.ticket-table__cell .label
{
    font-weight: bold;

    text-shadow: 1px 1px 1px #fff;
}

.ticket-table__cell .text b
{
    font: bold 13px Arial, sans-serif;
}

.ticket__footer
{
    padding: 0px 0px;
    width: 1000px;
    background: #ccc;
}

.ticket__footer .text
{
    font: bold 20px/40px Arial, sans-serif;

    height: 30px;

    text-align: center;

    background: #fff;
}

.line-keeper:before,
.line-keeper:after
{
    font-size: 10px;
    font-style: italic;

    position: absolute;
    right: 3px;
}

.line-keeper:before
{
    top: 1px;

    content: 'линия сгиба №2';
}

.line-keeper:after
{
    bottom: -15px;

    content: 'линия сгиба №1';
}

.layout-table
{
    width: 100%;
}

.layout-table__cell
{
    vertical-align: middle;
}

.layout-table__row:first-child .layout-table__cell:last-child
{
    text-align: right;
}

.layout-table__row:nth-child(2) .layout-table__cell:first-child,
.layout-table__row:nth-child(3) .layout-table__cell:first-child
{
    width: 128px;
}

.layout-table__row:last-child .layout-table__cell
{
    padding-top: 15px;
}

.layout-table .img
{
    vertical-align: middle;
}

.layout-table .sub-title
{
    font: bold 18px Arial, sans-serif;
}

.layout-table h2.sub-title
{
    font: bold 24px Verdana, Arial, sans-serif;

    display: inline-block;

    width: 522px;

    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
}

.layout-table .date
{
    float: right;

    padding: 0 15px;
}

.layout-table_type_prime
{
    font-family: Arial, sans-serif;
}

.layout-table_type_prime .layout-table__cell
{
    height: 386px;

    text-align: center;
    vertical-align: top;
}

.layout-table_type_prime .layout-table__cell:first-child
{
    width: 135px;

    text-transform: uppercase;

    border-right: 2px dashed #676465;
}

.layout-table_type_prime .layout-table__cell:last-child
{
    width: 130px;

    text-transform: uppercase;
}

.layout-table_type_prime .layout-table__row:last-child .layout-table__cell
{
    padding: 0;
}

.layout-table__cell_transform_yes .img
{
    -webkit-transform: rotate(180deg);
    -moz-transform: rotate(180deg);
    transform: rotate(180deg);
}

.ticket-logo
{
    font-size: 9px;
}

.ticket-logo .img
{
    margin: 20px 0 5px;
}

.spine
{
    font-size: 11px;

    color: #e5173f;
}

.ticket-control
{
    font-size: 11px;

    text-align: center;
}

.ticket-control .text
{
    padding: 20px 0;
}

.t-details
{
    width: 116px;
    margin: 25px auto;
}

.t-details__item
{
    padding: 2px 0;

    text-align: left;
}

.t-details__label
{
    display: inline-block;

    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 50%;
}

.t-details__item:nth-child(5) .t-details__label
{
    padding-top: 15px;
}

.t-details__item:nth-child(6) .t-details__label
{
    padding: 22px 0 0 10px;
}

.t-details__item:nth-child(7) .t-details__label
{
    padding-left: 10px;
}

.vert-text
{
    font-size: 7px;

    position: absolute;
    top: 192px;
    left: -186px;

    -webkit-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    -o-transform: rotate(-90deg);
    transform: rotate(-90deg);
}

.vert-text .text
{
    padding: 1px;

    text-align: center;
}

.prime-block
{
    position: relative;

    overflow: hidden;

    width: 100%;
    height: 100%;

    background: url(<?php echo Yii::app()->basePath ?>/../web/i/print/t2-bg.jpg) 28px 4px no-repeat;
}

.prime-block__header
{
    padding: 20px 0 0;

    text-align: right;
}

.prime-block__header .sub-title
{
    font-family: Arial, sans-serif;

    display: inline-block;

    padding: 0 8px;

    vertical-align: middle;
    text-transform: uppercase;
}

.prime-block__stadium
{
    width: 230px;
    height: 80px;
    margin: 10px 0 0 245px;

    border-right: 2px solid #cf112b;
}

.prime-block__stadium .sub-title
{
    font: bold 18px/0.8em Arial, sans-serif;

    display: inline-block;

    text-transform: uppercase;

    color: #e5173f;
}

.prime-block__stadium .year
{
    font: bold 17px Arial, sans-serif;

    display: block;

    text-transform: uppercase;
}

.prime-block__name
{
    font-size: 12px;

    float: right;

    width: 475px;
    padding: 3px 0 2px;
    margin-bottom: 10px;

    text-align: left;

    color: #e5173f;
    border-bottom: 1px dotted #676465;
}

.prime-block__partners
{
    clear: both;

    text-align: right;
}

.arrow-title
{
    font: 11px/17px Arial, sans-serif;

    position: relative;

    float: right;

    height: 18px;
    margin-left: 18px;
    padding: 0 10px;

    text-align: center;

    color: #fff;
    background: #cf112b;
}

.arrow-title:before
{
    position: absolute;
    top: 0;
    left: -18px;

    display: block;

    width: 18px;
    height: 18px;

    content: '';

    background: url(<?php echo Yii::app()->basePath ?>/../web/i/print/t2-triangle.jpg) no-repeat;
}

.prime-table
{
    margin: 30px 0 0 410px;
}

.prime-table__cell
{
    font-size: 12px;

    padding: 7px 8px;

    vertical-align: middle;
}

.prime-table__head .prime-table__row:first-child
{
    text-align: left;
}

.prime-table__cell:nth-child(2),
.prime-table__body .prime-table__cell
{
    border-right: 2px solid #cf112b;
}

.prime-table__cell:last-child,
.prime-table__header .prime-table__row:nth-child(2) .prime-table__cell
{
    border-right: 2px solid #fff;
}

.prime-table__header .prime-table__row:nth-child(2) .prime-table__cell
{
    font-size: 8px;
    font-weight: normal;

    height: 18px;
    padding: 0 8px;

    color: #fff;

    background: #cf112b;
}

.prime-table__header .prime-table__row:nth-child(2) .prime-table__cell:first-child
{
    padding-right: 0;
    background: transparent;
}

.prime-table .arrow-title
{
    font: 8px/18px Arial, sans-serif;

    padding-right: 20px;

    text-transform: uppercase;
}

.stat-table
{
    font-size: 18px;

    float: right;

    margin: 0 15px 0 0;
}

.stat-table__cell
{
    padding: 4px 8px;

    text-align: center;
}

.stat-table__head .stat-table__cell
{
    font-weight: normal;

    text-transform: uppercase;

    border-bottom: 2px solid #000;
}

.stat-table__body .stat-table__cell
{
    font-weight: bold;
}
</style>
</head>

<body class="page__body">
<section class="ticket">
<header class="ticket__header">
    <img class="print-logo" src="<?php echo Yii::app()->basePath ?>/../web/i/print/print-logo.png"/>

    <div class="warn">
        <p class="warn__1line">Требования к печати! Лазерный принтер или</p>
        <p class="warn__2line">«высокое качество» печати струйного принтера — 600 точек</p>
        <p class="warn__3line">Распечатайте без искажений и предъявите при входе на матч</p>
<p></p>
    </div>
</header>

<div class="ticket__body">
<article class="info-block">
    <table class="ticket-table">
        <tbody class="ticket-table__tbody">
        <tr class="ticket-table__row">
            <td class="ticket-table__cell">
                <span class="label">заказ/платеж/оформление</span>
                <p class="text">
                    <b>
                        <?php echo date( 'd.m.Y' ) ?>/
                        <?php echo date( 'd.m.Y' ) ?>/
                        <?php echo date( 'd.m.Y' ) ?>
                    </b>
                </p>
            </td>
            <td class="ticket-table__cell">
                <span class="label">цена билета:</span>
                <p class="text"><b><?php echo round( $ticket['amount'] ) ?> руб</b></p>
            </td>
        </tr>

        <tr class="ticket-table__row">
            <td class="ticket-table__cell">
                <span class="label">агент:</span>
                <p class="text">
                    ОАО Футбольный клуб &nbsp;«Спартак-Москва»<br/>
                    105066,&nbsp;Москва,&nbsp;ул.Новорязанская,&nbsp;д.31/7,&nbsp;корп.2<br/>
                    ИНН&nbsp;7728212268&nbsp;Тел: +7-495-111-1922
                </p>
            </td>
            <td class="ticket-table__cell">
                <span class="label">организатор:</span>
                <p class="text">
                    ООО «Российский Футбольный Союз»
                </p>
            </td>
        </tr>

        <tr class="ticket-table__row">
            <td class="ticket-table__cell" colspan="2">
                <span class="label">фамилия, имя, отчество</span>
                <p class="text"><b><?php echo $fullName ?></b></p>
            </td>
        </tr>

        <tr class="ticket-table__row">
            <td class="ticket-table__cell">
                <span class="label">электронная почта:</span>
                <p class="text"><b><?php echo $email ?></b></p>
            </td>
            <td class="ticket-table__cell">
                <span class="label">мобильный телефон:</span>
                <p class="text"><b><?php echo $phone ?></b></p>
            </td>
        </tr>

        <tr class="ticket-table__row">
            <td class="ticket-table__cell">
                <span class="label">дата и номер заказа:</span>
                <p class="text"><b>ЗАКАЗ №<?php echo $ticket['reservation_number'] ?> от <?php echo $time->format( 'd.m.Y H:i' ) ?></b></p>
            </td>
            <td class="ticket-table__cell">
                <span class="label">код электронного билета:</span>
                <p class="text"><b><?php echo $ticket['ticket_serial'] ?> <?php echo $ticket['ticket_number']?></b></p>
            </td>
        </tr>
        </tbody>
    </table>
</article>


<div class="ticket__wrapper">
    <table class="layout-table">
        <tbody class="layout-table__body">
            <tr class="layout-table__row">
                <td class="layout-table__cell" rowspan="4">
                    <img class="img" src="<?php echo CSite::getTempDir() . $barcode ?>">
                </td>
                <td class="layout-table__cell" colspan="3">
                    <h2 class="subtitle" align="center">    <?php   echo strtoupper( $current['name'] ) ?></h2>
		    <p style="color:red" align="center"></p>

                </td>
                
                <td class="layout-table__cell layout-table__cell_transform_yes" rowspan="4">
                    <img class="img" src="<?php echo CSite::getTempDir() . $barcode ?>">
                </td>
            </tr>
            
                                                                                                
            <tr class="layout-table__row">
                <td class="layout-table__cell" colspan="3">
                    <p>&nbsp;<br>
                    &nbsp;<br></p>
                    <img class="img" src="<?php echo Yii::app()->basePath ?>/../web/i/print/6463592/date.jpg" width="778" >
                </td>
            </tr>
            
            <tr class="layout-table__row">
                <td class="layout-table__cell">
                </td>
                <td class="layout-table__cell">
                    <h3 class="sub-title">Стадион "Открытие Арена"</h3>
        	</td>
                <td class="layout-table__cell stat-wrap">
                    <table class="stat-table">
                    <thead class="stat-table__head">
                        <tr class="stat-table__row">
                            <th class="stat-table__cell">Трибуна</th>
                            <th class="stat-table__cell">Сектор</th>
                            <th class="stat-table__cell">Ряд</th>
            		    <th class="stat-table__cell">Место</th>
                        </tr>
                    </thead>
                    
                    <tbody class="stat-table__body">
                	<tr class="stat-table__row">
                            <td class="stat-table__cell"><?php echo $ticket['tribune_name'] ?></td>
                            <td class="stat-table__cell"><?php echo $ticket['sector_name'] ?> <?php /*echo $ticket['upper'] ? 'Верх' : 'Низ'*/ ?></td>
                            <td class="stat-table__cell"><?php echo $ticket['row_name'] ?></td>
                            <td class="stat-table__cell"><?php echo $ticket['seat_name'] ?></td>
                        </tr>
                    </tbody>
                    </table>
                </td>
            </tr>
        
            <tr class="layout-table__row">
                <td class="layout-table__cell" colspan="3">
                    <img class="img" src="<?php echo Yii::app()->basePath ?>/../web/i/print/6463592/partners.png" width="778" height="152" >
                    <p style="color:red" align="center"></p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
<div class="ticket__control">
    <h2 class="ticket-title">Правила контроля электронных билетов и посещения матчей</h2>

    <article class="info-block">
        <div class="text">
            <ul class="list">
                <li class="list__item">
                    Вход на матч возможен при наличии действительного электронного билета (исключая
                    детей, младше 7 лет, которым билет не требуется),в сектор и на место, указанное на
                    билете. На контрольной части билета нанесен штрих-код. Доступ на матч осуществляется
                    только по этому штрих-коду через турникет или с использованием системы
                    автоматизированного входного контроля. Владелец билета обязан аккуратно обращаться с
                    билетом для надлежащего считывания штрих-кода турникетом/устройствами
                    автоматического входного контроля.
                </li>

                <li class="list__item">
                    Владелец электронного билета обязан сложить бланк электронного билета по линиям
                    сгиба, вставить в считыватель турникета либо предоставить сотруднику
                    контрольно-пропускной службы бланк электронного билета штрих-кодом вниз,
                    турникет/сотрудник контрольно-пропускной службы считывает штрих-код с бланка
                    электронного билета. В случае проблем сверяются данные системы автоматизированного
                    входного контроля (фамилию, имя, матч, сектор, ряд, место, платеж) с информацией,
                    указанной на данном бланке электронного билета и, при необходимости, с
                    удостоверением личности владельца электронного билета. По требованию сотрудника
                    контрольно-пропускной службы владелец электронного билета должен предъявить
                    удостоверение личности (паспорт, водительские права), а также подтверждающие оплату
                    документы.
                </li>

                <li class="list__item">
                    Владелец электронного билета имеет право пройти на матч через турникет/пункт
                    автоматизированного входного контроля при выполнении следующих условий:
                    <ul class="list">
                        <li class="list__item">Бланк электронного билета распечатан без искажений, штрих-код считывается турникетом.</li>
                        <li class="list__item">
                            Данные, указанные на бланке электронного билета в разделе "фамилия, имя,
                            отчество владельца", совпадают с данными, указанными в Вашем удостоверении
                            личности.
                        </li>
                        <li class="list__item">Турникет выводит на дисплей сообщение "ПРОХОД РАЗРЕШЕН".</li>
                    </ul>
                </li>

                <li class="list__item">
                    Электронный билет действителен только при приобретении его у Организатора или
                    уполномоченных агентов. Если электронный билет куплен у третьих лиц, или если
                    электронный билет утерян, украден, подделан, не является удобочитаемым, является
                    дубликатом или получен в нарушение данных условий, Вы не получите доступ на матч или
                    должны будете покинуть спортсооружение.
                </li>

                <li class="list__item">
                    Электронный билет не может быть продан, обменен на другой билет, продан или передан
                    для коммерческих, рекламных или продвигающих целей без предварительного письменного
                    согласия Организатора. Владелец билета обязан сохранять свой электронный билет до
                    конца матча и предъявлять его по требованию официальных лиц.
                </li>

                <li class="list__item">
                    Владелец электронного билета согласен, в качестве обязательного условия для доступа
                    на матч, подвергнуться проверке безопасности при входе на спортсооружение. Повторный
                    вход по электронному билету не осуществляется.
                </li>

                <li class="list__item">
                    Возврат электронного билета возможен только в случае переноса или отмены матча.
                    Организатор не несет никакой ответственности за какие-либо претензии, убытки,
                    компенсации, потери или расходы в результате отмены, отсрочки или изменения матча.
                </li>

                <li class="list__item">
                    Владелец электронного билета может быть задействован Организатором или
                    Администрацией спортсооружения при съемках, записях и фотографиях матча.
                </li>

                <li class="list__item">
                    Владелец электронного билета ответственен за свою собственную безопасность и за
                    безопасность детей, сопровождающих владельца электронного билета, и должен всегда
                    проявлять внимательность.
                </li>

                <li class="list__item">
                    Владельцу электронного билета следует предусмотреть достаточное время для прибытия
                    на матч с учетом осуществления проверок безопасности и электронных билетов.
                </li>

                <li class="list__item">
                    Запрещается проносить на спортсооружение любые виды оружия, колющие и режущие
                    предметы, стеклянную и пластиковую тару, продукты питания, пиротехнику всех видов, а
                    также иные предметы, которые могут мешать зрителям и нормальному ходу проведения
                    матча.
                </li>

                <li class="list__item">
                    Владелец электронного билета обязуется соблюдать "Правила поведения в
                    спортсооружении (для футбольных матчей)", которые установлены Администрацией
                    спортсооружения, Организатором и РФПЛ и spartak.com.
                </li>
            </ul>
        </div>

        <img width="225px" height="361px" class="print-stadium" src="<?php echo Yii::app()->basePath ?>/../web/i/print/print-stadium-<?php echo $ticket['stadium_id'] ?>.png">
    </article>
</div>

<article class="info-block info-block_type_important">
    <div class="text">
        <h2 class="ticket-title">Внимание! Повторный проход по копии или оригиналу электронного билета невозможен!</h2>

        <p class="attention-text">
            Уважаемые болельщики! Берегите свои билеты от копирования! В случае
            копирования бланков электронных билетов доступ на матч будет открыт только по тому билету,
            который был предъявлен первым – если на дисплее турникета / системы автоматизированного
            входного контроля выводится сообщение «ПОВТОРНЫЙ ПРОХОД», Вы не будете допущены на матч.
        </p>
    </div>
</article>

<article class="info-block">
    <div class="text text_size_s">
        <p>
            Администрация спортсооружения и Организатор имеет право отказать Вам во входе на матч, или
            удалить Вас со спортсооружения без компенсации стоимости электронного билета, если:
        </p>

        <ul class="list list_size_s">
            <li class="list__item">Вы не выполняете данные условия и нарушаете установленные правила поведения;</li>
            <li class="list__item">Вы отказываетесь от проведения личного досмотра;</li>
            <li class="list__item">Вы мешаете проведению матча;</li>
            <li class="list__item">Вы мешаете удовольствию, комфорту или безопасности других лиц на спортсооружении;</li>
            <li class="list__item">Вы производите фото-, видео- или телесъемку профессиональной аппаратурой.</li>
        </ul>

        <ul class="list list_size_s">
            <li class="list__item">Вы находитесь в состоянии алкогольного или наркотического опьянения;</li>
            <li class="list__item">Вы курите или разливаете спиртные напитки внутри спортсооружения;</li>
            <li class="list__item">Ваш электронный билет утерян, украден, подделан, поврежден или не читается;</li>
            <li class="list__item">Вы приобрели электронный билет из неуполномоченного источника;</li>
            <li class="list__item">Вы появляетесь в служебных помещениях без разрешения администрации.</li>
        </ul>
    </div>
</article>
</div>

<footer class="ticket__footer">
    <p class="text">В случае проблем с проходом на Стадион обращайтесь в "Группу разбора"</p>
    <p align="center" style="color:red"></p>
</footer>
</section>
</body>

</html>