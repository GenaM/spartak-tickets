<?php

/**
 * This is the model class for table "rapida_request".
 *
 * The followings are the available columns in table 'rapida_request':
 * @property integer $id
 * @property string $AMOUNT
 * @property string $CURRENCY
 * @property string $ORDER
 * @property string $DESC
 * @property integer $TERMINAL
 * @property integer $TRTYPE
 * @property string $MERCH_NAME
 * @property string $MERCHANT
 * @property string $EMAIL
 * @property string $TIMESTAMP
 * @property string $NONCE
 * @property string $P_SIGN
 * @property string $BACKREF
 *
 * The followings are the available model relations:
 * @property Order $order
 */
class RapidaRequest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rapida_request';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('AMOUNT, CURRENCY, ORDER, DESC, TERMINAL, TRTYPE, MERCH_NAME, MERCHANT, EMAIL, TIMESTAMP, NONCE, P_SIGN, BACKREF', 'required'),
			array('TERMINAL, TRTYPE', 'numerical', 'integerOnly'=>true),
			array('EMAIL', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, AMOUNT, CURRENCY, ORDER, DESC, TERMINAL, TRTYPE, MERCH_NAME, MERCHANT, EMAIL, TIMESTAMP, NONCE, P_SIGN, BACKREF', 'safe', 'on'=>'search'),
		);
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'order' => array(self::BELONGS_TO, 'Order', array('id' => 'request_id')),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'AMOUNT' => 'Amount',
			'CURRENCY' => 'Currency',
			'ORDER' => 'Order',
			'DESC' => 'Desc',
			'TERMINAL' => 'Terminal',
			'TRTYPE' => 'Trtype',
			'MERCH_NAME' => 'Merch Name',
			'MERCHANT' => 'Merchant',
			'EMAIL' => 'Email',
			'TIMESTAMP' => 'Timestamp',
			'NONCE' => 'Nonce',
			'P_SIGN' => 'P Sign',
			'BACKREF' => 'Backref',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('AMOUNT',$this->AMOUNT,true);
		$criteria->compare('CURRENCY',$this->CURRENCY,true);
		$criteria->compare('ORDER',$this->ORDER,true);
		$criteria->compare('DESC',$this->DESC,true);
		$criteria->compare('TERMINAL',$this->TERMINAL);
		$criteria->compare('TRTYPE',$this->TRTYPE);
		$criteria->compare('MERCH_NAME',$this->MERCH_NAME,true);
		$criteria->compare('MERCHANT',$this->MERCHANT,true);
		$criteria->compare('EMAIL',$this->EMAIL,true);
		$criteria->compare('TIMESTAMP',$this->TIMESTAMP,true);
		$criteria->compare('NONCE',$this->NONCE,true);
		$criteria->compare('P_SIGN',$this->P_SIGN,true);
		$criteria->compare('BACKREF',$this->BACKREF,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RapidaRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
