<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
    <title>Page 1</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
        <!--
        p {margin: 0; padding: 0;}	.ft00{font-size:19px;font-family:Times;color:#e4143b;}
        .ft01{font-size:13px;font-family:Times;color:#231f20;}
        .ft02{font-size:10px;font-family:Times;color:#231f20;}
        .ft03{font-size:4px;font-family:Times;color:#231f20;}
        .ft04{font-size:10px;font-family:Times;color:#d3d2d2;}
        .ft05{font-size:1px;font-family:Times;color:#231f20;}
        .ft06{font-size:5px;font-family:Times;color:#231f20;}
        .ft07{font-size:10px;line-height:21px;font-family:Times;color:#231f20;}
        .ft08{font-size:10px;line-height:24px;font-family:Times;color:#231f20;}
        .ft09{font-size:10px;line-height:18px;font-family:Times;color:#231f20;}
        .ft010{font-size:10px;line-height:25px;font-family:Times;color:#231f20;}
        .ft011{font-size:10px;line-height:20px;font-family:Times;color:#231f20;}
        .ft012{font-size:10px;line-height:23px;font-family:Times;color:#231f20;}
        .ft18{font-size:10px;font-family:Times;color:#231f20;}
        .ft13{font-size:10px;font-family:Times;color:#d3d2d2;}
        .ft14{font-size:10px;line-height:19px;font-family:Times;color:#231f20;}
        .ft15{font-size:10px;line-height:22px;font-family:Times;color:#231f20;}
        .ft16{font-size:10px;line-height:17px;font-family:Times;color:#231f20;}
        .ft17{font-size:10px;line-height:18px;font-family:Times;color:#231f20;}
        -->
    </style>
</head>
<body bgcolor="#fff" vlink="blue" link="blue" style="margin: 0px; >
<div id="page1-div" style="position:relative;width:1262px;height:870px;">
<img width="1262" height="870" src="<?php echo Yii::app()->basePath ?>/../web/i/season/o_1831b13e5c3c6cda001.png" alt="background image"/>
<p style="position:absolute;top:34px;left:43px;white-space:nowrap" class="ft00"><b>ЗАЯВЛЕНИЕ – АНКЕТА&#160;</b></p>
<p style="position:absolute;top:60px;left:43px;white-space:nowrap" class="ft01">НА ПРЕДОСТАВЛЕНИЕ ПРЕДОПЛАЧЕННОЙ ПОПОЛНЯЕМОЙ КАРТЫ ОАО БАНК «ОТКРЫТИЕ» «MASTERCARD PREPAID PAY-</p>
<p style="position:absolute;top:78px;left:43px;white-space:nowrap" class="ft01">PASS» В &#160;РАМКАХ СОВМЕСТНОГО ПРОЕКТА С ОАО «ФУТБОЛЬНЫЙ КЛУБ «СПАРТАК-МОСКВА»&#160;</p>
<p style="position:absolute;top:81px;left:755px;white-space:nowrap" class="ft02">(далее – Заявление - Анкета)</p>
<p style="position:absolute;top:82px;left:845px;white-space:nowrap" class="ft03">1</p>
<p style="position:absolute;top:112px;left:51px;white-space:nowrap" class="ft02">1. ИСТОЧНИК ИНФОРМАЦИИ О БАНКЕ</p>
<p style="position:absolute;top:126px;left:43px;white-space:nowrap" class="ft02">Я, нижеподписавшийся(аяся) (также именуемый(ая) Клиент),&#160;</p>
<p style="position:absolute;top:138px;left:43px;white-space:nowrap" class="ft02">прошу в соответствии с параметрами, указанными в настоящем&#160;</p>
<p style="position:absolute;top:150px;left:43px;white-space:nowrap" class="ft02">Заявлении-Анкете, в Правилах, Тарифах предоставить мне Карту,&#160;&#160;</p>
<p style="position:absolute;top:162px;left:43px;white-space:nowrap" class="ft02">таким образом, заключив со мной Договор в соответствии с пунктом&#160;</p>
<p style="position:absolute;top:174px;left:43px;white-space:nowrap" class="ft02">3 статьи 438 Гражданского Кодекса Российской Федерации.</p>
<p style="position:absolute;top:186px;left:43px;white-space:nowrap" class="ft02">Я понимаю и соглашаюсь, что Банк имеет право, в случаях,&#160;</p>
<p style="position:absolute;top:198px;left:43px;white-space:nowrap" class="ft02">предусмотренных законодательством Российской Федерации,&#160;</p>
<p style="position:absolute;top:210px;left:43px;white-space:nowrap" class="ft02">отказать в заключении договора о выдаче и использовании Карты.</p>
<p style="position:absolute;top:222px;left:43px;white-space:nowrap" class="ft02">Я подтверждаю, что все положения Правил, Тарифов, &#160;тексты&#160;</p>
<p style="position:absolute;top:234px;left:43px;white-space:nowrap" class="ft02">Правил, Тарифы и Памятка по безопасному использованию&#160;</p>
<p style="position:absolute;top:246px;left:43px;white-space:nowrap" class="ft02">банковской карты&#160;&#160;получены&#160;мной&#160;до&#160;подписания&#160;настоящего&#160;</p>
<p style="position:absolute;top:258px;left:43px;white-space:nowrap" class="ft02">Заявления-Анкеты.</p>
<p style="position:absolute;top:270px;left:43px;white-space:nowrap" class="ft02">Я&#160;согласен&#160;(согласна),&#160;что&#160;Договор&#160;считается&#160;заключенным&#160;с&#160;даты&#160;</p>
<p style="position:absolute;top:282px;left:43px;white-space:nowrap" class="ft02">исполнения&#160;всех&#160;перечисленных действий:&#160;заполнения&#160;настоящего&#160;</p>
<p style="position:absolute;top:294px;left:43px;white-space:nowrap" class="ft02">Заявления-Анкеты,&#160;предоставления&#160;мною&#160;документов,&#160;</p>
<p style="position:absolute;top:306px;left:43px;white-space:nowrap" class="ft02">удостоверяющих личность, получения банковской карты и внесения&#160;</p>
<p style="position:absolute;top:318px;left:43px;white-space:nowrap" class="ft02">денежных средств в оплату Лимита Карты.</p>
<p style="position:absolute;top:338px;left:51px;white-space:nowrap" class="ft02">2. ПАРАМЕТРЫ КАРТЫ</p>
<p style="position:absolute;top:357px;left:43px;white-space:nowrap" class="ft08">Название Тарифа:&#160;Карта Болельщика&#160;<br/>Тип (категория) Карты:&#160;MASTERCARD PREPAID PAYPASS<br/>Номер болельщика:&#160;</p>
<p style="position:absolute;top:403px;left:167px;white-space:nowrap" class="ft04">ХХХ&#160;ХХХХХХХ</p>
<p style="position:absolute;top:409px;left:267px;white-space:nowrap" class="ft02">&#160; &#160;Валюта :&#160;рубли РФ&#160;</p>
<p style="position:absolute;top:427px;left:51px;white-space:nowrap" class="ft02">3. ПЕРСОНАЛЬНЫЕ ДАННЫЕ КЛИЕНТА</p>
<p style="position:absolute;top:446px;left:43px;white-space:nowrap" class="ft02">Ф. И. О. Клиента (полностью)</p>
<p style="position:absolute;top:446px;left:210px;white-space:nowrap" class="ft02"><?php echo $user->last_name ?></p>
<p style="position:absolute;top:466px;left:45px;white-space:nowrap" class="ft02"><?php echo $user->first_name ?></p>
<p style="position:absolute;top:466px;left:240px;white-space:nowrap" class="ft02"><?php echo $user->middle_name ?></p>
<p style="position:absolute;top:509px;left:43px;white-space:nowrap" class="ft05">&#160;</p>
<p style="position:absolute;top:499px;left:43px;white-space:nowrap" class="ft02">Дата&#160;рождения&#160;</p><?php  $bdate = new DateTime( $user->full_info->BirthDate ); ?>
<p style="position:absolute;top:499px;left:136px;white-space:nowrap" class="ft04"></p>
<p style="position:absolute;top:500px;left:136px;white-space:nowrap" class="ft02"><?php echo $bdate->format('d') ?></p>
<p style="position:absolute;top:499px;left:174px;white-space:nowrap" class="ft04"></p>
<p style="position:absolute;top:500px;left:174px;white-space:nowrap" class="ft02"><?php echo $bdate->format('m') ?></p>
<p style="position:absolute;top:500px;left:219px;white-space:nowrap" class="ft02"><?php echo $bdate->format('Y') ?></p>
<p style="position:absolute;top:499px;left:271px;white-space:nowrap" class="ft02">&#160;Пол&#160;</p>
<p style="position:absolute;top:499px;left:318px;white-space:nowrap" class="ft02"><?php if( $user->full_info->Sex ) { echo $user->full_info->Sex == 'Male' ? 'M' : 'Ж'; } ?></p>
<p style="position:absolute;top:526px;left:43px;white-space:nowrap" class="ft02">Гражданство (подданство)&#160;&#160;</p>
<p style="position:absolute;top:526px;left:220px;white-space:nowrap" class="ft02">&#160;РФ&#160;&#160;</p>
<p style="position:absolute;top:531px;left:269px;white-space:nowrap" class="ft02">&#160;Иное</p>
<p style="position:absolute;top:543px;left:43px;white-space:nowrap" class="ft09">Место рождения<br/>Документ, удостоверяющий личность</p>
<p style="position:absolute;top:543px;left:143px;white-space:nowrap" class="ft09"><?php echo $user->full_info->Passport->BirthPlace ?></p>
<p style="position:absolute;top:587px;left:65px;white-space:nowrap" class="ft02">&#160;Паспорт гражданина РФ&#160;</p>
<p style="position:absolute;top:587px;left:266px;white-space:nowrap" class="ft02">&#160;&#160;Иной&#160;документ (указать)</p>
<p style="position:absolute;top:624px;left:43px;white-space:nowrap" class="ft02">Серия&#160;</p>
<p style="position:absolute;top:624px;left:85px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Passport->Series ?></p>
<p style="position:absolute;top:624px;left:213px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Passport->Number ?></p>
<p style="position:absolute;top:624px;left:156px;white-space:nowrap" class="ft02">&#160; &#160;Номер&#160;</p>
<p style="position:absolute;top:647px;left:43px;white-space:nowrap" class="ft03">1</p>
<p style="position:absolute;top:646px;left:45px;white-space:nowrap" class="ft02">&#160;&#160;Термины, используемые в настоящем Заявлении-анкете, имеют то же&#160;</p>
<p style="position:absolute;top:658px;left:48px;white-space:nowrap" class="ft02">значение, что и в Правилах предоставления и использования банковских&#160;</p>
<p style="position:absolute;top:670px;left:48px;white-space:nowrap" class="ft02">расчетных карт ОАО Банк «ОТКРЫТИЕ» для физических лиц.</p>
<p style="position:absolute;top:683px;left:43px;white-space:nowrap" class="ft03">2</p>
<p style="position:absolute;top:682px;left:46px;white-space:nowrap" class="ft02">&#160;&#160;ИПДЛ – иностранное &#160;публичное должностное лицо - &#160;любое назначае-</p>
<p style="position:absolute;top:694px;left:49px;white-space:nowrap" class="ft02">мое или избираемое лицо, занимающее какую-либо должность в зако-</p>
<p style="position:absolute;top:706px;left:49px;white-space:nowrap" class="ft02">нодательном, исполнительном, административном или судебном орга-</p>
<p style="position:absolute;top:718px;left:49px;white-space:nowrap" class="ft02">не иностранного государства, или лицо, выполняющее какую-либо&#160;</p>
<p style="position:absolute;top:730px;left:49px;white-space:nowrap" class="ft02">публичную функцию для иностранного государства, в том числе, для&#160;</p>
<p style="position:absolute;top:742px;left:49px;white-space:nowrap" class="ft02">публичного ведомства или государственного предприятия либо лицо,&#160;</p>
<p style="position:absolute;top:754px;left:49px;white-space:nowrap" class="ft02">ранее занимавшее публичную должность, с момента, сложения полно-</p>
<p style="position:absolute;top:766px;left:49px;white-space:nowrap" class="ft02">мочий которого прошло менее 1 (Одного) года. В случае если Клиент&#160;</p>
<p style="position:absolute;top:778px;left:49px;white-space:nowrap" class="ft02">проставляет в поле отметку «да», ему необходимо заполнить Вопросник&#160;</p>
<p style="position:absolute;top:790px;left:49px;white-space:nowrap" class="ft02">для идентификации иностранных публичных должностных лиц.</p>
<p style="position:absolute;top:803px;left:43px;white-space:nowrap" class="ft03">3</p>
<p style="position:absolute;top:802px;left:46px;white-space:nowrap" class="ft02">&#160;&#160;ПДЛ – публичное должностное лицо. К ПДЛ относятся должностные&#160;</p>
<p style="position:absolute;top:814px;left:49px;white-space:nowrap" class="ft02">лица публичных международных организаций,&#160;а также лица, замещаю-</p>
<p style="position:absolute;top:826px;left:49px;white-space:nowrap" class="ft02">щие (занимающие) государственные должности Российской Федера-</p>
<p style="position:absolute;top:838px;left:49px;white-space:nowrap" class="ft02">ции, должности членов Совета директоров Центрального банка Россий-</p>
<p style="position:absolute;top:646px;left:449px;white-space:nowrap" class="ft02">ской Федерации, должности федеральной государственной службы,&#160;</p>
<p style="position:absolute;top:658px;left:449px;white-space:nowrap" class="ft02">назначение на которые и освобождение от которых осуществляются&#160;</p>
<p style="position:absolute;top:670px;left:449px;white-space:nowrap" class="ft02">Президентом Российской Федерации или Правительством Российской&#160;</p>
<p style="position:absolute;top:682px;left:449px;white-space:nowrap" class="ft02">Федерации,&#160;должности&#160;в Центральном&#160;банке&#160;Российской&#160;Федерации,&#160;</p>
<p style="position:absolute;top:694px;left:449px;white-space:nowrap" class="ft02">государственных&#160;корпорациях&#160;и&#160;иных&#160;организациях,&#160;созданных Россий-</p>
<p style="position:absolute;top:706px;left:449px;white-space:nowrap" class="ft02">ской Федерацией на основании федеральных законов, включенные в&#160;</p>
<p style="position:absolute;top:718px;left:449px;white-space:nowrap" class="ft02">перечни должностей, определяемые Президентом Российской Федера-</p>
<p style="position:absolute;top:730px;left:449px;white-space:nowrap" class="ft02">ции.</p>
<p style="position:absolute;top:743px;left:442px;white-space:nowrap" class="ft03">4</p>
<p style="position:absolute;top:742px;left:446px;white-space:nowrap" class="ft02">&#160;&#160;Заработная плата, пенсия, доходы от предпринимательской деятельно-</p>
<p style="position:absolute;top:754px;left:449px;white-space:nowrap" class="ft02">сти, наследство, личные сбережения, процентный доход по вкладам&#160;</p>
<p style="position:absolute;top:766px;left:449px;white-space:nowrap" class="ft02">(ценным бумагам), прочие доходы (указать).</p>
<p style="position:absolute;top:779px;left:442px;white-space:nowrap" class="ft03">5</p>
<p style="position:absolute;top:778px;left:446px;white-space:nowrap" class="ft02">&#160;&#160;Бенефициарный&#160;владелец&#160;- физическое лицо, которое в&#160;конечном счете&#160;</p>
<p style="position:absolute;top:790px;left:449px;white-space:nowrap" class="ft02">прямо&#160;или&#160;косвенно&#160;(через&#160;третьих&#160;лиц)&#160;имеет&#160;возможность&#160;контроли-</p>
<p style="position:absolute;top:802px;left:449px;white-space:nowrap" class="ft02">ровать&#160;действия&#160;клиента.&#160;Если&#160;Клиент&#160;проставляет&#160;отметку&#160;«да»,&#160;ему&#160;</p>
<p style="position:absolute;top:814px;left:449px;white-space:nowrap" class="ft02">необходимо заполнить документ «Сведения о бенефициарном владель-</p>
<p style="position:absolute;top:826px;left:449px;white-space:nowrap" class="ft02">це» и предоставить копии подтверждающих документов или заверенное&#160;</p>
<p style="position:absolute;top:838px;left:449px;white-space:nowrap" class="ft02">Клиентом письмо, подтверждающие достоверность этих сведений.</p>
<p style="position:absolute;top:647px;left:842px;white-space:nowrap" class="ft03">6</p>
<p style="position:absolute;top:646px;left:846px;white-space:nowrap" class="ft02">&#160;&#160;Выгодоприобретатель&#160;-&#160;лицо,&#160;к&#160;выгоде&#160;которого&#160;действует&#160;Клиент,&#160;в&#160;том&#160;</p>
<p style="position:absolute;top:658px;left:849px;white-space:nowrap" class="ft02">числе на основании агентского договора, договоров поручения, комис-</p>
<p style="position:absolute;top:670px;left:849px;white-space:nowrap" class="ft02">сии и доверительного управления, при проведении операций с денеж-</p>
<p style="position:absolute;top:682px;left:849px;white-space:nowrap" class="ft02">ными средствами и иным имуществом.</p>
<p style="position:absolute;top:122px;left:442px;white-space:nowrap" class="ft010">Код&#160;подразделения&#160;(при&#160;наличии)&#160;<br/>Дата выдачи&#160;</p><?php $idate = new DateTime( $user->full_info->Passport->WhenIssued ); ?>
<p style="position:absolute;top:122px;left:645px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Passport->IssuedByCode ?></p>
<p style="position:absolute;top:147px;left:521px;white-space:nowrap" class="ft02"><?php echo $idate->format('d') ?></p>
<p style="position:absolute;top:153px;left:526px;white-space:nowrap" class="ft02"></p>
<p style="position:absolute;top:147px;left:558px;white-space:nowrap" class="ft02"><?php echo $idate->format('m') ?></p>
<p style="position:absolute;top:153px;left:558px;white-space:nowrap" class="ft02"></p>
<p style="position:absolute;top:147px;left:604px;white-space:nowrap" class="ft02"><?php echo $idate->format('Y') ?></p>
<p style="position:absolute;top:153px;left:655px;white-space:nowrap" class="ft02">&#160;&#160;Кем выдан:</p>
<p style="position:absolute;top:173px;left:445px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Passport->IssuedBy ?></p>
<p style="position:absolute;top:190px;left:442px;white-space:nowrap" class="ft011">Адрес места жительства (регистрации)<br/>Страна&#160;&#160;</p>
<p style="position:absolute;top:216px;left:490px;white-space:nowrap" class="ft02"></p>
<p style="position:absolute;top:216px;left:510px;white-space:nowrap" class="ft02">&#160;РФ&#160;&#160;</p>
<p style="position:absolute;top:216px;left:559px;white-space:nowrap" class="ft02">&#160;иное&#160;</p>
<p style="position:absolute;top:235px;left:442px;white-space:nowrap" class="ft02">Дата&#160;регистрации&#160;</p><?php $rdate = new DateTime( $user->full_info->Security->RegistrationDate ); ?>
<p style="position:absolute;top:235px;left:550px;white-space:nowrap" class="ft02"><?php echo $rdate->format('d') ?></p>
<p style="position:absolute;top:241px;left:575px;white-space:nowrap" class="ft02"></p>
<p style="position:absolute;top:235px;left:588px;white-space:nowrap" class="ft02"><?php echo $rdate->format('m') ?></p>
<p style="position:absolute;top:241px;left:615px;white-space:nowrap" class="ft02"></p>
<p style="position:absolute;top:235px;left:633px;white-space:nowrap" class="ft02"><?php echo $rdate->format('Y') ?>
<p style="position:absolute;top:241px;left:685px;white-space:nowrap" class="ft02">&#160;&#160;Индекс&#160;</p>
<p style="position:absolute;top:238px;left:750px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Contacts->AddressByPassport->Index ?></p>
<p style="position:absolute;top:261px;left:442px;white-space:nowrap" class="ft07">Регион (республика, край, область)<br/>Район<br/>Город/населенный пункт<br/>Улица, м/район, квартал</p>
<p style="position:absolute;top:261px;left:645px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Contacts->AddressByPassport->RegionId ?></p>
<p style="position:absolute;top:282px;left:645px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Contacts->AddressByPassport->State ?></p>
<p style="position:absolute;top:304px;left:600px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Contacts->AddressByPassport->Settlement ?> <?php echo  $user->full_info->Contacts->AddressByPassport->CityId ?></p>
<p style="position:absolute;top:326px;left:600px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Contacts->AddressByPassport->Street ?></p>
<p style="position:absolute;top:350px;left:442px;white-space:nowrap" class="ft02">Дом&#160;</p>
<p style="position:absolute;top:350px;left:472px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Contacts->AddressByPassport->House ?></p>
<p style="position:absolute;top:350px;left:530px;white-space:nowrap" class="ft02">&#160;Корпус/строение&#160;</p>
<p style="position:absolute;top:350px;left:645px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Contacts->AddressByPassport->Block ?></p>
<p style="position:absolute;top:350px;left:699px;white-space:nowrap" class="ft02">&#160;Квартира&#160;</p>
<p style="position:absolute;top:350px;left:770px;white-space:nowrap" class="ft02"><?php echo $user->full_info->Contacts->AddressByPassport->Flat ?></p>
<p style="position:absolute;top:379px;left:442px;white-space:nowrap" class="ft02">Адрес места пребывания (фактический)</p>
<p style="position:absolute;top:399px;left:442px;white-space:nowrap" class="ft02"></p>
<p style="position:absolute;top:399px;left:465px;white-space:nowrap" class="ft02">&#160;совпадает&#160;с&#160;адресом&#160;регистрации&#160;</p>
<p style="position:absolute;top:399px;left:711px;white-space:nowrap" class="ft02">&#160;&#160;не&#160;совпадает&#160;адре-</p>
<p style="position:absolute;top:411px;left:714px;white-space:nowrap" class="ft02">сом регистрации&#160;</p>
<p style="position:absolute;top:399px;left:690px;white-space:nowrap" class="ft02"></p>
<p style="position:absolute;top:431px;left:442px;white-space:nowrap" class="ft02">Страна&#160;&#160;</p>
<p style="position:absolute;top:431px;left:510px;white-space:nowrap" class="ft02">&#160;РФ&#160;&#160;</p>
<p style="position:absolute;top:431px;left:559px;white-space:nowrap" class="ft02">&#160;Иная&#160;</p>
<p style="position:absolute;top:431px;left:701px;white-space:nowrap" class="ft02">Индекс&#160;</p>
<p style="position:absolute;top:431px;left:750px;white-space:nowrap" class="ft02"></p>
<p style="position:absolute;top:456px;left:442px;white-space:nowrap" class="ft011">Регион (республика, край, область)  <br/>Район <br/>Город/населенный пункт  <br/>Улица, м/район, квартал <br/>Дом&#160;</p>
<p style="position:absolute;top:541px;left:530px;white-space:nowrap" class="ft02">&#160;Корпус?/?строение&#160; </p>
<p style="position:absolute;top:541px;left:699px;white-space:nowrap" class="ft02">&#160;Квартира&#160;</p>
<p style="position:absolute;top:570px;left:442px;white-space:nowrap" class="ft02">Домашний телефон&#160; <?php echo $user->full_info->Contacts->MobilePhone ?> </p>
<p style="position:absolute;top:583px;left:442px;white-space:nowrap" class="ft02">По&#160;адресу&#160;регистрации&#160;</p>
<p style="position:absolute;top:583px;left:636px;white-space:nowrap" class="ft02">&#160;По&#160;месту&#160;фактического&#160;</p>
<p style="position:absolute;top:595px;left:636px;white-space:nowrap" class="ft02">проживания</p>
<p style="position:absolute;top:611px;left:450px;white-space:nowrap" class="ft04">+7&#160;(ХХХ)</p>
<p style="position:absolute;top:617px;left:510px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:611px;left:540px;white-space:nowrap" class="ft04">ХХХ&#160;ХХХХ</p>
<p style="position:absolute;top:617px;left:627px;white-space:nowrap" class="ft02">&#160;</p>
<p style="position:absolute;top:611px;left:644px;white-space:nowrap" class="ft04">+7&#160;(ХХХ)</p>
<p style="position:absolute;top:617px;left:704px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:611px;left:734px;white-space:nowrap" class="ft04">ХХХ&#160;ХХХХ</p>
<p style="position:absolute;top:152px;left:842px;white-space:nowrap" class="ft02">Мобильный&#160;телефон&#160;</p>
<p style="position:absolute;top:151px;left:1044px;white-space:nowrap" class="ft04">+7&#160;(ХХХ)</p>
<p style="position:absolute;top:157px;left:1104px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:151px;left:1134px;white-space:nowrap" class="ft04">ХХХ&#160;ХХХХ</p>
<p style="position:absolute;top:176px;left:842px;white-space:nowrap" class="ft02">Дополнительный телефон&#160;</p>
<p style="position:absolute;top:176px;left:1044px;white-space:nowrap" class="ft04">+7&#160;(ХХХ)</p>
<p style="position:absolute;top:182px;left:1104px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:176px;left:1134px;white-space:nowrap" class="ft04">ХХХ&#160;ХХХХ</p>
<p style="position:absolute;top:202px;left:842px;white-space:nowrap" class="ft012">E-mail<br/>ИНН (при наличии)<br/>Являетесь ли Вы ИПДЛ</p>
<p style="position:absolute;top:202px;left:882px;white-space:nowrap" class="ft012"><?php echo $user->full_info->Contacts->Email ?></p>
<p style="position:absolute;top:248px;left:972px;white-space:nowrap" class="ft06">2</p>
<p style="position:absolute;top:247px;left:976px;white-space:nowrap" class="ft02">?&#160;</p>
<p style="position:absolute;top:247px;left:1172px;white-space:nowrap" class="ft04">ДА</p>
<p style="position:absolute;top:252px;left:1192px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:247px;left:1199px;white-space:nowrap" class="ft04">НЕТ</p>
<p style="position:absolute;top:272px;left:842px;white-space:nowrap" class="ft02">Состоите&#160;ли&#160;Вы&#160;в&#160;родстве&#160;с&#160;ИПДЛ?&#160;</p>
<p style="position:absolute;top:272px;left:1172px;white-space:nowrap" class="ft04">ДА</p>
<p style="position:absolute;top:277px;left:1192px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:272px;left:1199px;white-space:nowrap" class="ft04">НЕТ</p>
<p style="position:absolute;top:291px;left:842px;white-space:nowrap" class="ft02">Планируете ли Вы осуществлять&#160;&#160;</p>
<p style="position:absolute;top:303px;left:842px;white-space:nowrap" class="ft02">операции от имени ИПДЛ?&#160;</p>
<p style="position:absolute;top:303px;left:1172px;white-space:nowrap" class="ft04">ДА</p>
<p style="position:absolute;top:308px;left:1192px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:303px;left:1199px;white-space:nowrap" class="ft04">НЕТ</p>
<p style="position:absolute;top:319px;left:842px;white-space:nowrap" class="ft02">Являетесь&#160;ли&#160;Вы&#160;публичным&#160;должностным&#160;&#160;</p>
<p style="position:absolute;top:331px;left:842px;white-space:nowrap" class="ft02">лицом&#160;(ПДЛ)</p>
<p style="position:absolute;top:332px;left:918px;white-space:nowrap" class="ft06">3</p>
<p style="position:absolute;top:331px;left:922px;white-space:nowrap" class="ft02">?&#160;</p>
<p style="position:absolute;top:331px;left:1172px;white-space:nowrap" class="ft04">ДА</p>
<p style="position:absolute;top:336px;left:1192px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:331px;left:1199px;white-space:nowrap" class="ft04">НЕТ</p>
<p style="position:absolute;top:345px;left:842px;white-space:nowrap" class="ft02">Должность ПДЛ (заполняется для ПДЛ)</p>
<p style="position:absolute;top:382px;left:842px;white-space:nowrap" class="ft02">Источник дохода</p>
<p style="position:absolute;top:383px;left:937px;white-space:nowrap" class="ft06">4</p>
<p style="position:absolute;top:382px;left:941px;white-space:nowrap" class="ft02">&#160;(заполняется для ПДЛ)</p>
<p style="position:absolute;top:421px;left:842px;white-space:nowrap" class="ft02">Наличие&#160;бенефициарного&#160;владельца</p>
<p style="position:absolute;top:422px;left:1048px;white-space:nowrap" class="ft06">5</p>
<p style="position:absolute;top:421px;left:1052px;white-space:nowrap" class="ft02">&#160;</p>
<p style="position:absolute;top:421px;left:1172px;white-space:nowrap" class="ft04">ДА</p>
<p style="position:absolute;top:426px;left:1192px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:421px;left:1199px;white-space:nowrap" class="ft04">НЕТ</p>
<p style="position:absolute;top:446px;left:842px;white-space:nowrap" class="ft02">Наличие&#160;выгодоприобретателя</p>
<p style="position:absolute;top:447px;left:1016px;white-space:nowrap" class="ft06">6</p>
<p style="position:absolute;top:446px;left:1020px;white-space:nowrap" class="ft02">&#160;</p>
<p style="position:absolute;top:446px;left:1172px;white-space:nowrap" class="ft04">ДА</p>
<p style="position:absolute;top:451px;left:1192px;white-space:nowrap" class="ft02">&#160;&#160;</p>
<p style="position:absolute;top:446px;left:1199px;white-space:nowrap" class="ft04">НЕТ</p>
<p style="position:absolute;top:467px;left:850px;white-space:nowrap" class="ft02">4.&#160;&#160;ДЕКЛАРАЦИЯ И ЗАЯВЛЕНИЕ&#160;</p>
<p style="position:absolute;top:493px;left:842px;white-space:nowrap" class="ft02">1.&#160;</p>
<p style="position:absolute;top:499px;left:874px;white-space:nowrap" class="ft02">&#160;&#160;&#160;Я в соответствии с требованием Федерального закона от&#160;</p>
<p style="position:absolute;top:512px;left:842px;white-space:nowrap" class="ft02">27.07.2006&#160;№152-ФЗ «О персональных данных» даю свое согласие&#160;</p>
<p style="position:absolute;top:524px;left:842px;white-space:nowrap" class="ft02">на обработку (сбор, запись, систематизацию, накопление, хранение,&#160;</p>
<p style="position:absolute;top:536px;left:842px;white-space:nowrap" class="ft02">уточнение (обновление, изменение), извлечение, использование,&#160;</p>
<p style="position:absolute;top:548px;left:842px;white-space:nowrap" class="ft02">распространение, передачу (включая трансграничную передачу),&#160;</p>
<p style="position:absolute;top:560px;left:842px;white-space:nowrap" class="ft02">обезличивание, блокирование, удаление и уничтожение) моих&#160;</p>
<p style="position:absolute;top:572px;left:842px;white-space:nowrap" class="ft011">персональных данных, &#160;с использованием средств автоматизации.&#160;<br/>&#160; &#160;</p>
<p style="position:absolute;top:599px;left:873px;white-space:nowrap" class="ft02">&#160;&#160;&#160;Я даю свое согласие на обработку персональных данных,</p>
<p style="position:absolute;top:615px;left:842px;white-space:nowrap" class="ft02">в том числе без использования средств автоматизации.&#160;</p>
</div>
<div id="page2-div" style="position:relative;width:1262px;height:870px;">
    <img width="1262" height="870" src="<?php echo Yii::app()->basePath ?>/../web/i/season/o_1831b13e5c3c6cda002.png" alt="background image"/>
    <p style="position:absolute;top:37px;left:43px;white-space:nowrap" class="ft18">Согласие на&#160;обработку моих персональных данных, в том числе с&#160;</p>
    <p style="position:absolute;top:49px;left:43px;white-space:nowrap" class="ft18">использованием средств автоматизации мною дается ОАО Банк&#160;</p>
    <p style="position:absolute;top:61px;left:43px;white-space:nowrap" class="ft18">«ОТКРЫТИЕ», 119021, г. Москва, ул. Тимура Фрунзе д. 11, стр. 13 :</p>
    <p style="position:absolute;top:76px;left:43px;white-space:nowrap" class="ft18">- в отношении любой информации, относящейся ко мне прямо или&#160;</p>
    <p style="position:absolute;top:88px;left:43px;white-space:nowrap" class="ft18">косвенно, включая: фамилию, имя, отчество; данные документа,&#160;</p>
    <p style="position:absolute;top:100px;left:43px;white-space:nowrap" class="ft18">удостоверяющего&#160;личность;&#160;год,&#160;месяц,&#160;число и&#160;место&#160;рождения;&#160;</p>
    <p style="position:absolute;top:112px;left:43px;white-space:nowrap" class="ft18">гражданство, адрес, другие сведения, предоставленные мною для&#160;</p>
    <p style="position:absolute;top:124px;left:43px;white-space:nowrap" class="ft18">заключения Договора или в период его действия, содержащиеся в&#160;</p>
    <p style="position:absolute;top:136px;left:43px;white-space:nowrap" class="ft18">заявлениях, письмах, соглашениях и иных документах;</p>
    <p style="position:absolute;top:151px;left:43px;white-space:nowrap" class="ft18">- для целей продвижения услуг Банка, совместных услуг Банка и&#160;</p>
    <p style="position:absolute;top:163px;left:43px;white-space:nowrap" class="ft18">третьих лиц, продуктов (товаров, работ, услуг) третьих лиц;</p>
    <p style="position:absolute;top:179px;left:43px;white-space:nowrap" class="ft18">- как Банку, так и любым третьим лицам, которые в результате&#160;</p>
    <p style="position:absolute;top:191px;left:43px;white-space:nowrap" class="ft18">обработки персональных данных, уступки, продажи &#160;или обременения&#160;</p>
    <p style="position:absolute;top:203px;left:43px;white-space:nowrap" class="ft18">иным образом полностью или частично прав требования по&#160;</p>
    <p style="position:absolute;top:215px;left:43px;white-space:nowrap" class="ft18">Договору получили персональные данные Клиента, стали&#160;</p>
    <p style="position:absolute;top:227px;left:43px;white-space:nowrap" class="ft18">правообладателями в отношении указанных прав, агентам и&#160;</p>
    <p style="position:absolute;top:239px;left:43px;white-space:nowrap" class="ft18">уполномоченным лицам Банка и указанных третьих лиц, а также&#160;&#160;</p>
    <p style="position:absolute;top:251px;left:43px;white-space:nowrap" class="ft18">компаниям (в объеме фамилия, имя, отчество, адреса и номера&#160;</p>
    <p style="position:absolute;top:263px;left:43px;white-space:nowrap" class="ft18">телефонов), осуществляющим почтовую рассылку по заявке Банка.</p>
    <p style="position:absolute;top:278px;left:43px;white-space:nowrap" class="ft18">Перечень третьих лиц, в отношении которых даю согласие на&#160;</p>
    <p style="position:absolute;top:290px;left:43px;white-space:nowrap" class="ft18">обработку моих персональных данных в целях продвижения услуг&#160;</p>
    <p style="position:absolute;top:302px;left:43px;white-space:nowrap" class="ft18">Банка, совместных услуг Банка и третьих лиц, продуктов (товаров,&#160;</p>
    <p style="position:absolute;top:314px;left:43px;white-space:nowrap" class="ft18">работ, услуг) третьих лиц, осуществления почтовых рассылок по&#160;</p>
    <p style="position:absolute;top:326px;left:43px;white-space:nowrap" class="ft18">заявке Банка, а также обслуживания кредитов: Открытое&#160;</p>
    <p style="position:absolute;top:338px;left:43px;white-space:nowrap" class="ft18">акционерное&#160;общество&#160;«Брокерский дом&#160;«ОТКРЫТИЕ»,&#160;место&#160;</p>
    <p style="position:absolute;top:350px;left:43px;white-space:nowrap" class="ft18">нахождения 115114, г. Москва, ул. Летниковская, д.2, стр. 4; Общество&#160;</p>
    <p style="position:absolute;top:362px;left:43px;white-space:nowrap" class="ft18">с ограниченной ответственностью «Управляющая компания&#160;</p>
    <p style="position:absolute;top:374px;left:43px;white-space:nowrap" class="ft18">«ОТКРЫТИЕ», место нахождения 119021, Москва, ул. Тимура Фрунзе&#160;</p>
    <p style="position:absolute;top:386px;left:43px;white-space:nowrap" class="ft18">д. 11, стр. 13; Открытое акционерное общество «Открытие Холдинг»,&#160;</p>
    <p style="position:absolute;top:398px;left:43px;white-space:nowrap" class="ft18">место нахождения 115114, г. Москва, ул. Летниковская, д.2, стр.4;&#160;</p>
    <p style="position:absolute;top:410px;left:43px;white-space:nowrap" class="ft18">Общество с ограниченной ответственностью «Долговой центр&#160;</p>
    <p style="position:absolute;top:422px;left:43px;white-space:nowrap" class="ft18">«ОТКРЫТИЕ», место нахождения 105064, г. Москва,&#160;</p>
    <p style="position:absolute;top:434px;left:43px;white-space:nowrap" class="ft18">Яковоапостольский пер., д. 12, стр. 1; Открытое акционерное общество&#160;</p>
    <p style="position:absolute;top:446px;left:43px;white-space:nowrap" class="ft18">«ОТКРЫТИЕ СТРАХОВАНИЕ», место нахождения 123007, г. Москва,&#160;</p>
    <p style="position:absolute;top:458px;left:43px;white-space:nowrap" class="ft18">4-я Магистральная улица, дом 11, строение 2; Закрытое акционерное&#160;</p>
    <p style="position:absolute;top:470px;left:43px;white-space:nowrap" class="ft18">общество «АККОРД ПОСТ», место нахождения 113452, Москва г,&#160;</p>
    <p style="position:absolute;top:482px;left:43px;white-space:nowrap" class="ft18">Азовская ул, д. 31; ФГУП «Почта России», место нахождения 131000,&#160;</p>
    <p style="position:absolute;top:494px;left:43px;white-space:nowrap" class="ft18">г. Москва, Варшавское шоссе, д. 37; Открытое акционерное общество&#160;</p>
    <p style="position:absolute;top:506px;left:43px;white-space:nowrap" class="ft18">Банк&#160;«Финансовая&#160;корпорация Открытие», место&#160;нахождения&#160;</p>
    <p style="position:absolute;top:518px;left:43px;white-space:nowrap" class="ft18">109240, Москва г, Верхняя Радищевская ул, д. 3, стр. 1, Открытое&#160;</p>
    <p style="position:absolute;top:530px;left:43px;white-space:nowrap" class="ft18">акционерное общество «Футбольный Клуб «Спартак - Москва»,&#160;</p>
    <p style="position:absolute;top:542px;left:43px;white-space:nowrap" class="ft18">место нахождения 105066, г. Москва, ул. Новорязанская, д. 31/7,&#160;</p>
    <p style="position:absolute;top:554px;left:43px;white-space:nowrap" class="ft18">корп. 24.</p>
    <p style="position:absolute;top:569px;left:43px;white-space:nowrap" class="ft18">Согласие действует до момента получения&#160;Банком письменного&#160;</p>
    <p style="position:absolute;top:581px;left:43px;white-space:nowrap" class="ft18">заявления Клиента об отзыве согласия на обработку персональных&#160;</p>
    <p style="position:absolute;top:593px;left:43px;white-space:nowrap" class="ft18">данных. Прекращение Договора не прекращает действие&#160;согласия.&#160;</p>
    <p style="position:absolute;top:605px;left:43px;white-space:nowrap" class="ft18">Осведомлен(а), что настоящее согласие может быть отозвано мной&#160;</p>
    <p style="position:absolute;top:617px;left:43px;white-space:nowrap" class="ft18">путем предоставления в Банк заявления в простой письменной&#160;</p>
    <p style="position:absolute;top:629px;left:43px;white-space:nowrap" class="ft18">форме.</p>
    <p style="position:absolute;top:644px;left:43px;white-space:nowrap" class="ft18">Подтверждаю, что мне сообщена информация о наименовании и&#160;</p>
    <p style="position:absolute;top:656px;left:43px;white-space:nowrap" class="ft18">месте нахождения Банка, о цели обработки персональных данных и&#160;</p>
    <p style="position:absolute;top:668px;left:43px;white-space:nowrap" class="ft18">ее правовых основаниях, о предполагаемых пользователях&#160;</p>
    <p style="position:absolute;top:680px;left:43px;white-space:nowrap" class="ft18">персональных данных и о правах лиц, указанных мною в Заявлении-</p>
    <p style="position:absolute;top:692px;left:43px;white-space:nowrap" class="ft18">Анкете, как субъектов персональных данных, предусмотренных&#160;</p>
    <p style="position:absolute;top:704px;left:43px;white-space:nowrap" class="ft18">Федеральным законом от 27.07.2006 №152-ФЗ «О персональных&#160;</p>
    <p style="position:absolute;top:716px;left:43px;white-space:nowrap" class="ft18">данных».</p>
    <p style="position:absolute;top:730px;left:43px;white-space:nowrap" class="ft18">&#160; &#160;</p>
    <p style="position:absolute;top:736px;left:74px;white-space:nowrap" class="ft18">&#160;&#160;&#160;Я даю согласие Банку на передачу и раскрытие любой</p>
    <p style="position:absolute;top:751px;left:43px;white-space:nowrap" class="ft18">информации, касающейся Договора (включая предоставленные&#160;</p>
    <p style="position:absolute;top:763px;left:43px;white-space:nowrap" class="ft18">мной при заключении Договора и в период его действия сведения,&#160;</p>
    <p style="position:absolute;top:775px;left:43px;white-space:nowrap" class="ft18">содержащиеся в заявлениях, письмах, соглашениях и иных&#160;</p>
    <p style="position:absolute;top:787px;left:43px;white-space:nowrap" class="ft18">документах), любым третьим лицам, которые в результате уступки,&#160;</p>
    <p style="position:absolute;top:799px;left:43px;white-space:nowrap" class="ft18">продажи или обременения иным образом полностью или частично&#160;</p>
    <p style="position:absolute;top:811px;left:43px;white-space:nowrap" class="ft18">прав требования по Договору стали правообладателями в&#160;</p>
    <p style="position:absolute;top:823px;left:43px;white-space:nowrap" class="ft18">отношении указанных прав, агентам, уполномоченным&#160;лицам Банка&#160;</p>
    <p style="position:absolute;top:835px;left:43px;white-space:nowrap" class="ft18">и указанных третьих лиц.</p>
    <p style="position:absolute;top:37px;left:442px;white-space:nowrap" class="ft18">2. Подтверждаю, что сведения, содержащиеся в настоящем&#160;</p>
    <p style="position:absolute;top:49px;left:442px;white-space:nowrap" class="ft18">Заявлении-Анкете и документах, предоставленных к нему, являются&#160;</p>
    <p style="position:absolute;top:61px;left:442px;white-space:nowrap" class="ft18">верными и точными на дату подписания Заявления-Анкеты и&#160;</p>
    <p style="position:absolute;top:73px;left:442px;white-space:nowrap" class="ft18">обязуюсь&#160;уведомить&#160;Банк&#160;в&#160;случае&#160;изменения&#160;данных&#160;сведений&#160;в&#160;</p>
    <p style="position:absolute;top:85px;left:442px;white-space:nowrap" class="ft18">срок&#160;не&#160;позднее календарного дня&#160;с&#160;даты&#160;изменения&#160;сведений&#160;в&#160;</p>
    <p style="position:absolute;top:97px;left:442px;white-space:nowrap" class="ft18">порядке, установленном Договором.</p>
    <p style="position:absolute;top:112px;left:442px;white-space:nowrap" class="ft18">3. Я предоставляю Банку право на получение от соответствующих&#160;</p>
    <p style="position:absolute;top:124px;left:442px;white-space:nowrap" class="ft18">государственных и муниципальных органов, а также от предприятий&#160;</p>
    <p style="position:absolute;top:136px;left:442px;white-space:nowrap" class="ft18">и организаций заключений о достоверности сведений, указанных&#160;</p>
    <p style="position:absolute;top:148px;left:442px;white-space:nowrap" class="ft18">мной в Заявлении-Анкете, и содержащихся в предъявленных мной&#160;</p>
    <p style="position:absolute;top:160px;left:442px;white-space:nowrap" class="ft18">документах.&#160;Согласен&#160;с&#160;тем,&#160;что&#160;любые&#160;сведения,&#160;содержащиеся&#160;в&#160;</p>
    <p style="position:absolute;top:172px;left:442px;white-space:nowrap" class="ft18">Заявлении-Анкете, могут быть в любое время проверены или&#160;</p>
    <p style="position:absolute;top:184px;left:442px;white-space:nowrap" class="ft18">перепроверены Банком, его агентами и правопреемниками,&#160;</p>
    <p style="position:absolute;top:196px;left:442px;white-space:nowrap" class="ft18">непосредственно&#160;или&#160;с&#160;помощью&#160;специализированных&#160;агентств,&#160;с&#160;</p>
    <p style="position:absolute;top:208px;left:442px;white-space:nowrap" class="ft18">использованием любых источников информации.</p>
    <p style="position:absolute;top:223px;left:442px;white-space:nowrap" class="ft18">4.&#160;Не&#160;возражаю&#160;против&#160;проверки&#160;Банком&#160;указанных&#160;мной&#160;в&#160;</p>
    <p style="position:absolute;top:235px;left:442px;white-space:nowrap" class="ft18">настоящем Заявлении-Анкете данных и получения иной&#160;</p>
    <p style="position:absolute;top:247px;left:442px;white-space:nowrap" class="ft18">необходимой информации способами, не противоречащими&#160;</p>
    <p style="position:absolute;top:259px;left:442px;white-space:nowrap" class="ft14">действующему законодательству Российской Федерации.<br/>5.&#160;</p>
    <p style="position:absolute;top:284px;left:479px;white-space:nowrap" class="ft18">&#160;Согласен(а)&#160;с&#160;направлением&#160;мне&#160;Банком&#160;и/или&#160;ОАО&#160;«&#160;ФК&#160;</p>
    <p style="position:absolute;top:295px;left:442px;white-space:nowrap" class="ft18">«Спартак - Москва» коммерческих предложений.</p>
    <p style="position:absolute;top:311px;left:442px;white-space:nowrap" class="ft18">6. Я прошу предоставить мне Услугу «SMS-инфо» с использованием&#160;</p>
    <p style="position:absolute;top:323px;left:442px;white-space:nowrap" class="ft18">номера телефона сотовой связи, указанного мной в разделе 2&#160;</p>
    <p style="position:absolute;top:335px;left:442px;white-space:nowrap" class="ft18">настоящего Заявления-Анкеты.</p>
    <p style="position:absolute;top:350px;left:442px;white-space:nowrap" class="ft18">7. Согласен(а) с тем, что принятие Банком настоящего Заявления-</p>
    <p style="position:absolute;top:362px;left:442px;white-space:nowrap" class="ft18">Анкеты&#160;и&#160;иных&#160;документов&#160;к&#160;рассмотрению,&#160;а&#160;также мои&#160;возможные&#160;</p>
    <p style="position:absolute;top:374px;left:442px;white-space:nowrap" class="ft18">расходы (на оформление необходимых документов и т.п.) не влекут&#160;</p>
    <p style="position:absolute;top:386px;left:442px;white-space:nowrap" class="ft18">за собой обязательств Банка выдать мне Карту, и/или возместить&#160;</p>
    <p style="position:absolute;top:398px;left:442px;white-space:nowrap" class="ft18">понесенные мною издержки.&#160;</p>
    <p style="position:absolute;top:413px;left:442px;white-space:nowrap" class="ft18">8. Настоящим подтверждаю, что уведомлен о возможности&#160;</p>
    <p style="position:absolute;top:425px;left:442px;white-space:nowrap" class="ft18">присоединения к Условиям дистанционного банковского&#160;</p>
    <p style="position:absolute;top:437px;left:442px;white-space:nowrap" class="ft18">обслуживания физических лиц в ОАО Банк «ОТКРЫТИЕ» с&#160;</p>
    <p style="position:absolute;top:449px;left:442px;white-space:nowrap" class="ft18">использованием Интернет-банка «Открытие Online» (далее –&#160;</p>
    <p style="position:absolute;top:461px;left:442px;white-space:nowrap" class="ft18">Условия «Открытие Online») в порядке, предусмотренном ст.428&#160;</p>
    <p style="position:absolute;top:473px;left:442px;white-space:nowrap" class="ft18">Гражданского кодекса Российской Федерации, следующими&#160;</p>
    <p style="position:absolute;top:485px;left:442px;white-space:nowrap" class="ft18">способами: посредством банкоматов Банка; путем подачи заявления&#160;</p>
    <p style="position:absolute;top:497px;left:442px;white-space:nowrap" class="ft18">в офисе Банка; путем акцепта направленного Банком в виде SMS-</p>
    <p style="position:absolute;top:509px;left:442px;white-space:nowrap" class="ft18">сообщения с кодом активации предложения о присоединении к&#160;</p>
    <p style="position:absolute;top:521px;left:442px;white-space:nowrap" class="ft18">Условиям «Открытие Online» (путем ввода соответствующего кода&#160;</p>
    <p style="position:absolute;top:533px;left:442px;white-space:nowrap" class="ft18">активации на сайте Интернет-банка «Открытие Online»).</p>
    <p style="position:absolute;top:548px;left:442px;white-space:nowrap" class="ft18">9. Настоящим подтверждаю, что с правилами приобретения&#160;</p>
    <p style="position:absolute;top:560px;left:442px;white-space:nowrap" class="ft18">абонементов и поведения на стадионе, а также с федеральным&#160;</p>
    <p style="position:absolute;top:572px;left:442px;white-space:nowrap" class="ft18">законом от 23 февраля 2013 года № 15-ФЗ «Об охране здоровья&#160;</p>
    <p style="position:absolute;top:584px;left:442px;white-space:nowrap" class="ft18">граждан от воздействия окружающего табачного дыма и последствий&#160;</p>
    <p style="position:absolute;top:596px;left:442px;white-space:nowrap" class="ft18">потребления табака» ознакомлен и согласен</p>
    <p style="position:absolute;top:611px;left:442px;white-space:nowrap" class="ft18">10. Настоящий Договор будет регулироваться и толковаться по&#160;</p>
    <p style="position:absolute;top:623px;left:442px;white-space:nowrap" class="ft18">законодательству Российской Федерации. Любые вопросы,&#160;</p>
    <p style="position:absolute;top:635px;left:442px;white-space:nowrap" class="ft18">разногласия или претензии, возникающие из настоящего Договора&#160;</p>
    <p style="position:absolute;top:647px;left:442px;white-space:nowrap" class="ft18">или в связи ним, подлежат урегулированию между Сторонами на&#160;</p>
    <p style="position:absolute;top:659px;left:442px;white-space:nowrap" class="ft18">взаимоприемлемой основе путем переговоров. При отсутствии&#160;</p>
    <p style="position:absolute;top:671px;left:442px;white-space:nowrap" class="ft18">согласия, споры и разногласия по Договору Стороны договорились&#160;</p>
    <p style="position:absolute;top:683px;left:442px;white-space:nowrap" class="ft18">рассматривать в следующем порядке:</p>
    <p style="position:absolute;top:698px;left:442px;white-space:nowrap" class="ft18">10.1 Иски Банка к Клиенту Стороны договорились рассматривать в</p>
    <p style="position:absolute;top:735px;left:442px;white-space:nowrap" class="ft18">(указывается суд в пределах субъекта Российской Федерации по&#160;</p>
    <p style="position:absolute;top:747px;left:442px;white-space:nowrap" class="ft18">месту нахождения Клиента или по месту заключения Договора).</p>
    <p style="position:absolute;top:762px;left:442px;white-space:nowrap" class="ft18">10.2. Иски Клиента к Банку о&#160;защите прав потребителей&#160;</p>
    <p style="position:absolute;top:774px;left:442px;white-space:nowrap" class="ft18">предъявляются в соответствии с законодательством Российской&#160;</p>
    <p style="position:absolute;top:786px;left:442px;white-space:nowrap" class="ft15">Федерации.<br/>11.&#160;</p>
    <p style="position:absolute;top:814px;left:479px;white-space:nowrap" class="ft18">&#160;Я подтверждаю наличие у меня второго экземпляра&#160;</p>
    <p style="position:absolute;top:824px;left:442px;white-space:nowrap" class="ft18">Заявления-Анкеты, Правил, Тарифов и то, что мне предоставлена&#160;</p>
    <p style="position:absolute;top:836px;left:442px;white-space:nowrap" class="ft18">исчерпывающая информация о предоставляемых услугах и&#160;</p>
    <p style="position:absolute;top:142px;left:842px;white-space:nowrap" class="ft18">полностью разъяснены вопросы, имевшиеся по условиям&#160;</p>
    <p style="position:absolute;top:154px;left:842px;white-space:nowrap" class="ft17">заключения и исполнения Договора.<br/>Подпись Клиента&#160;&#160;&#160;<br/>Ф. И. О. Клиента полностью, собственноручно</p>
    <p style="position:absolute;top:230px;left:842px;white-space:nowrap" class="ft18">&#160;</p>
    <p style="position:absolute;top:230px;left:1048px;white-space:nowrap" class="ft18">Дата&#160;&#160;</p>
    <p style="position:absolute;top:229px;left:1086px;white-space:nowrap" class="ft13">Д&#160;Д</p>
    <p style="position:absolute;top:235px;left:1111px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:229px;left:1124px;white-space:nowrap" class="ft13">ММ</p>
    <p style="position:absolute;top:235px;left:1151px;white-space:nowrap" class="ft18">?/?</p>
    <p style="position:absolute;top:229px;left:1169px;white-space:nowrap" class="ft13">Г&#160;Г&#160;Г&#160;Г</p>
    <p style="position:absolute;top:275px;left:850px;white-space:nowrap" class="ft18">5.&#160;&#160;ДАТА И ПОДПИСЬ УПОЛНОМОЧЕННОГО ЛИЦА,</p>
    <p style="position:absolute;top:289px;left:850px;white-space:nowrap" class="ft18">&#160; &#160; &#160; ПРИНЯВШЕГО ЗАЯВЛЕНИЕ-АНКЕТУ</p>
    <p style="position:absolute;top:310px;left:842px;white-space:nowrap" class="ft18">Ф.И.О. уполномоченного лица&#160;</p>
    <p style="position:absolute;top:356px;left:842px;white-space:nowrap" class="ft18">&#160;Дата принятия настоящего Заявления&#160; &#160; &#160;&#160;&#160;</p>
    <p style="position:absolute;top:356px;left:1085px;white-space:nowrap" class="ft13">Д&#160;Д</p>
    <p style="position:absolute;top:361px;left:1110px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:356px;left:1123px;white-space:nowrap" class="ft13">ММ</p>
    <p style="position:absolute;top:361px;left:1150px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:356px;left:1168px;white-space:nowrap" class="ft13">Г&#160;Г&#160;Г&#160;Г</p>
    <p style="position:absolute;top:377px;left:842px;white-space:nowrap" class="ft18">Подпись&#160;&#160;&#160;</p>
    <p style="position:absolute;top:428px;left:850px;white-space:nowrap" class="ft18">6.&#160;&#160;РАСПИСКА В ПОЛУЧЕНИИ КАРТЫ</p>
    <p style="position:absolute;top:453px;left:864px;white-space:nowrap" class="ft18">&#160;Карту получил. Механические повреждения отсутствуют.&#160;</p>
    <p style="position:absolute;top:473px;left:842px;white-space:nowrap" class="ft18">ID карты:&#160;</p>
    <p style="position:absolute;top:473px;left:907px;white-space:nowrap" class="ft13">ХХХ&#160;ХХХХХ</p>
    <p style="position:absolute;top:478px;left:990px;white-space:nowrap" class="ft18">&#160;&#160;</p>
    <p style="position:absolute;top:493px;left:842px;white-space:nowrap" class="ft18">Номер Карты</p>
    <p style="position:absolute;top:518px;left:859px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:518px;left:878px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:518px;left:896px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:518px;left:915px;white-space:nowrap" class="ft18">&#160;-&#160;</p>
    <p style="position:absolute;top:518px;left:942px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:518px;left:961px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:513px;left:967px;white-space:nowrap" class="ft13"></p>
    <p style="position:absolute;top:518px;left:980px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:513px;left:986px;white-space:nowrap" class="ft13">Х</p>
    <p style="position:absolute;top:518px;left:999px;white-space:nowrap" class="ft18">&#160;-</p>
    <p style="position:absolute;top:513px;left:1012px;white-space:nowrap" class="ft13">Х</p>
    <p style="position:absolute;top:518px;left:1025px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:513px;left:1031px;white-space:nowrap" class="ft13">Х</p>
    <p style="position:absolute;top:518px;left:1043px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:513px;left:1051px;white-space:nowrap" class="ft13">Х</p>
    <p style="position:absolute;top:518px;left:1064px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:513px;left:1072px;white-space:nowrap" class="ft13">Х</p>
    <p style="position:absolute;top:518px;left:1084px;white-space:nowrap" class="ft18">&#160;-&#160;</p>
    <p style="position:absolute;top:518px;left:1112px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:518px;left:1130px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:518px;left:1149px;white-space:nowrap" class="ft18"></p>
    <p style="position:absolute;top:543px;left:842px;white-space:nowrap" class="ft18">Срок действия&#160;&#160;?</p>
    <p style="position:absolute;top:542px;left:935px;white-space:nowrap" class="ft13">ММ</p>
    <p style="position:absolute;top:548px;left:962px;white-space:nowrap" class="ft18">/</p>
    <p style="position:absolute;top:542px;left:980px;white-space:nowrap" class="ft13">Г&#160;Г&#160;Г&#160;Г</p>
    <p style="position:absolute;top:590px;left:842px;white-space:nowrap" class="ft15">ФИО лица, получившего Карту<br/>Подпись&#160;</p>
    <p style="position:absolute;top:612px;left:1048px;white-space:nowrap" class="ft18">Дата&#160;&#160;</p>
    <p style="position:absolute;top:612px;left:1086px;white-space:nowrap" class="ft13">Д&#160;Д</p>
    <p style="position:absolute;top:618px;left:1111px;white-space:nowrap" class="ft18">/</p>
    <p style="position:absolute;top:612px;left:1124px;white-space:nowrap" class="ft13">ММ</p>
    <p style="position:absolute;top:618px;left:1151px;white-space:nowrap" class="ft18">/</p>
    <p style="position:absolute;top:612px;left:1169px;white-space:nowrap" class="ft13">Г&#160;Г&#160;Г&#160;Г</p>
</div>
</body>
</html>
