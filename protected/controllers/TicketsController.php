<?php
    class TicketsController extends Controller  {

        public $layout = '//layouts/main';

        public function beforeAction($action) {
            if( in_array( $action->id, array( 'reservation' , 'order' ) ) ) {
                $this->showBasket = false;
            }
            if( in_array( $action->id, array( 'stadium' ) ) )   {
                $this->loadTickets = false;
            }
            return parent::beforeAction( $action );
        }

        /**
         * @param null $activeEventId
         * @throws CHttpException
         */
        public function actionStadium( $activeEventId = null )  {
            if( Yii::app()->params['blocks']['stadiumPage'] ) {
                $this->render('blocked');
                Yii::app()->end();
            }

            if( ! $activeEventId )  {
                $activeEventId = $this->events['active']['event_id'];
            } else {
                throw new CHttpException( 404 );
            }

            $dbEvent = Event::model()->findByAttributes( array('event_id' => $activeEventId, 'is_active' => true) );
            if( ! $dbEvent )    {
                throw new CHttpException( 404 );
            }

            if( ! Yii::app()->user->isGuest && Yii::app()->params['blocks']['useCanLogin'] ) {
                $user = User::model()->findByPK( Yii::app()->user->id );
                $SUH = new SpartakUsersHelper();
                $canLogin = $SUH->canLogin( $user['email'] , $this->events['active']['event_id'] );
                if($canLogin !== true) {
                    $this->render('canLogin_error/' . $canLogin);
                    Yii::app()->end();
                }
            }

            $this->render( 'stadium' );
        }

        /**
         * @param $sectorId
         * @throws CHttpException
         */
        public function actionSector( $sectorId )   {
            if( Yii::app()->params['blocks']['sectorPage'] ) {
                $this->render('blocked');
                Yii::app()->end();
            }

            if (Yii::app()->user->isGuest) {
                $this->redirect($this->createUrl('site/login'));
            }

            if( ! Yii::app()->user->isGuest && Yii::app()->params['blocks']['useCanLogin'] ) {
                $user = User::model()->findByPK( Yii::app()->user->id );
                $SUH = new SpartakUsersHelper();
                $canLogin = $SUH->canLogin( $user['email'] , $this->events['active']['event_id'] );
                if($canLogin !== true) {
                    $this->render('canLogin_error/' . $canLogin);
                    Yii::app()->end();
                }
            }

            $dbSector   = Sector::model()->findByAttributes( array('sector_id' => (int) $sectorId, 'event_id' => $this->events['active']['event_id']) );

            if( ! $dbSector )   {
                throw new CHttpException( 404 );
            }

            if( ! $dbSector->available )    {
                $this->redirect($this->createUrl('tickets/stadium', array('eventId' => $dbSector['event_id'])));
            }

            if( ! $event = SpartakMainHelper::checkEvent( $dbSector['event_id'] ) )   {
                throw new CHttpException( 404 );
            }

            $sector                 = $this->tickets->getSectorScheme( $dbSector['stadium_id'] , $dbSector['event_id'] , $sectorId , $event['type'] );
            $sector['upper']        = $dbSector['upper'];
            $sector['tribune']      = $dbSector['tribune'];
            if( in_array( $sectorId, array(4939334, 4933332) ) ) { //TODO temporary hardсode =|
                $seats = array();
                foreach( $sector['seats'] as $row )    {
                    foreach( $row as $seat )    {
                        if($seat['status'] == 1)    {
                            $seats[] = $seat;
                        }
                        if( count( $seats ) == 1089 )   { //33x33
                            break 2;
                        }
                    }
                }
                $sector['seats'] = $seats;
                $this->render( 'simplifiedSector', array(
                        'sector'        => $sector,
                        'event'         => $event,
                        'activeSeats'   => SpartakMainHelper::getActiveSeats(),
                    )
                );
            } else {
                $this->render( 'sector' , array(
                    'sector'        => $sector,
                    'event'         => $event,
                    'activeSeats'   => SpartakMainHelper::getActiveSeats(),
                ) );
            }
        }

        public function actionReservation() {
            if( Yii::app()->params['blocks']['reservationPage'] ) {
                $this->render('blocked');
                Yii::app()->end();
            }

            if(Yii::app()->user->isGuest) {
                $this->redirect($this->createUrl('site/login'));
            }
            if( empty( $this->basket ) )    {
                $this->redirect($this->createUrl('site/index'));
            }

            $SUH  = new SpartakUsersHelper();
            $user = $SUH->getUserInfo( Yii::app()->user->contactId, Yii::app()->user->sessionId );
            if( $user ) {
                User::addSpartakUser( $user );
            }

            $orderId        = null;
            $description    = '';

            //TODO: temp
            $rapidaTerminal = null;

            foreach( $this->basket as $eventId => $events ) {
                foreach( $events['tickets'] as $ticket )  {
                    if( ! $orderId ) {
                        $orderId        = date('mdHis') . rand(100,999) . $ticket->reservation_number;
                    }

                    if($ticket->amount) {
                        $description .= $ticket->sector_name . ',' . $ticket->row_name . ',' . $ticket->seat_name . ';';
                    }

                    if( $ticket->status == Basket::STATUS_NEW )    {
                        $confirm = $this->tickets->ConfirmReservation( $ticket->stadium_id , $ticket->reservation_id );
                        $ticket->attributes = array(
                            'confirm_reservation' => $confirm
                        );
                        if( $confirm == 1 || $confirm == 0 ) {
                            $ticket->attributes = array( 'status' => Basket::STATUS_RESERVED, 'expiration_time' => CSite::getNow(Basket::SECOND_STEP_RESERVATION_TIME) );
                            //TODO: temp
                            if(empty($rapidaTerminal)) {
                                $event = SpartakMainHelper::checkEvent( $eventId );
                                $rapidaTerminal = $event->rapida_terminal;
                            }
                        } else {
                            $ticket->attributes = array( 'status' => Basket::STATUS_TS_ERROR );
                        }
                        $ticket->save();
                    } else {
                        $this->render('reservation_timeout');
                        Yii::app()->end();
                    }
                }
            }
            if( ! $orderId ) {
                $orderId = date('mdHis') . rand(100, 999) . date('Y');
            }

            $this->render( 'reservation' , RapidaHelper::saveRapidaRequest($orderId, $description, $rapidaTerminal) );
        }


        public function actionOrder() {
            RapidaHelper::processRapidaResponse( $_POST, $this->tickets );
        }

        /**
         *
         */
        public function actionPayed()  {
            if(Yii::app()->user->isGuest) {
                $this->redirect($this->createUrl('site/login'));
            }

            $this->render('payed');
        }

        /**
         * @param $stadiumId
         * @param $eventId
         * @throws CHttpException
         */
        public function actionGetSectorList( $stadiumId , $eventId )  {
            if( ! SpartakMainHelper::checkEvent( $eventId ) )   {
                throw new CHttpException( 404 );
            }
            $sectors = Sector::model()->findAllByAttributes( array( 'stadium_id' => $stadiumId , 'event_id' => $eventId ) );
            $result = array();

            foreach( $sectors as $key => $sector )   {
                $result[$sector->sector_id] = $sector->attributes;
            }

            echo json_encode( $result );
            Yii::app()->end();
        }

        /**
         * @param $stadiumId
         * @param $eventId
         * @param $sectorId
         * @throws CHttpException
         */
        public function actionGetSeatsStatus( $stadiumId , $eventId , $sectorId )   {
            if( ! SpartakMainHelper::checkEvent( $eventId ) )   {
                throw new CHttpException( 404 );
            }
            $inBasket = SpartakMainHelper::getActiveSeats();
            echo json_encode(
                array(
                    'seats'     => $this->tickets->getSeatsStatus( $stadiumId , $eventId , $sectorId ),
                    'inBasket' => isset( $inBasket[ $eventId ] ) ? $inBasket[ $eventId ] : array()
                )
            );
            Yii::app()->end();
        }


        public function actionCash24success() {
            SpartakMainHelper::writeLog( print_r($_POST, true), 'cash24success' );
        }

        public function actionCash24cancel() {
            SpartakMainHelper::writeLog( print_r($_POST, true), 'cash24cancel' );
        }

        public function actionCash24callback() {
            SpartakMainHelper::writeLog( print_r($_POST, true), 'cash24callback' );
        }

    }