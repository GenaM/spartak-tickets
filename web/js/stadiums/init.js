var sectorList = {};
var sectorListIdName = {};
var stCon = null;

var colors = {
  st: '#E2183D', //standard
  un: '#979797', //unavailable
  av: '#E2183D', //available
  mo: '#FFFFFF', //mouseover
  so: '#DEDEDE'  //solted out
};

$(document).ready(function () {
  stCon = $('#stadium');
  getSectorList();
});

function getSectorList() {
  $.ajax({
    url: stCon.data('getlist'),
    type: 'get',
    dataType: 'json',
    success: function (r) {
      sectorList = r;
      $.each(sectorList, function (i, val) {
        sectorListIdName[ val.sector_id ] = i;
      });
      drawStadium();
    }
  });
}

function drawStadium() {
  Raphael('stadium', stCon.data('width'), stCon.data('height'), function () {
    var self = this;

    self.image(stadium.background, 0, 0, stCon.data('width'), stCon.data('height'));

    $.each(stadium.sectors, function (sectorName, val) {
      drawSector(sectorName);
    });

    if( stadium.numbers !== undefined ) {
      self.image(stadium.numbers, 0, 0, stCon.data('width'), stCon.data('height'));
    }

    self.setStart();

    $.each(stadium.sectors, function (sectorName, val) {
      drawSectorOverlay(sectorName);
    });

    var section = self.setFinish();

    section.hover(function () {
      this.c = this.c || this.attr('fill');
      this.stop().animate({ stroke: colors.mo, 'stroke-opacity': 0.7, fill: colors.mo, 'fill-opacity': 0.7 }, 50);
    }, function () {
      this.stop().animate({ fill: this.c, 'fill-opacity': 0, 'stroke-opacity': 0 }, 50);
    });


    function drawSector(sectorName) {
      if (sectorList[sectorName] == undefined) {
        return;
      }
      var url = stCon.data('sectorurl').replace('_sectorId_', sectorList[ sectorName ].sector_id);
      var color = colors.st;
      if (sectorList[ sectorName ].available == 0) {
        color = colors.un;
      } else if (sectorList[sectorName].free_seats == 0) {
        color = colors.so;
      }
      var attr = { fill: color, stroke: "none", href: url };

      s = self.path(stadium.sectors[ sectorName ]).attr(attr);
      s.scale(1, 1, 0, 0);
    }

    function drawSectorOverlay(sectorName) {
      if( sectorList[sectorName] == undefined ) {
        return null;
      }
      var url = stCon.data('sectorurl').replace('_sectorId_', sectorList[ sectorName ].sector_id);
      if (sectorList[ sectorName ].available == 0) {
        attr = {'fill-opacity': 0, fill: colors.un, stroke: colors.un, 'stroke-opacity': 0 };
      } else if (sectorList[sectorName].free_seats == 0) {
        attr = {'fill-opacity': 0, fill: colors.so, stroke: colors.so, 'stroke-opacity': 0 };
      } else {
        attr = { 'fill-opacity': 0, fill: colors.av, stroke: colors.av, 'stroke-opacity': 0, href: url };
      }

      s = self.path(stadium.sectors[ sectorName ]).attr(attr);
      s.scale(1, 1, 0, 0);
    }

    $('#stadium .preloader').remove();
  });
}