<?php
    class SpartakUsersHelper extends SoapHelper    {

        protected $url =  'http://95.165.189.151/Website/UserPanel.svc?singleWsdl';
        public $errorCode = 452;

        /**
         * @param $email
         * @param $password
         * @return array
         * @throws CHttpException
         */
        public function auth( $email , $password )  {
            $result     = array( 'error' => 0 );
            $response   = $this->call( 'UserLogin', array( 'login' => $email , 'password' => $password ), null, false );
            if( ! $this->checkError( $response ) ) {
                $result['error'] = $response->ErrorCode;
                return $result;
            }
            if( ! $result = $this->getUserInfo( $response->ContactId , $response->SessionId ) ) {
                return false;
            }
            return $result;
        }

        public function getUserInfo( $contactId, $sessionId, $evenFiktiv = true ) {
            $response   = $this->call( 'UserGetInfo', array( 'contactId' => $contactId, 'sessionId' => $sessionId , 'evenFiktiv' => $evenFiktiv ), null, false );
            if( ! $this->checkError( $response ) )  {
                return false;
            }
            $result = array(
                'firstName'     => isset($response->User->Firstname) ? $response->User->Firstname : null,
                'lastName'      => isset($response->User->Lastname) ? $response->User->Lastname : null,
                'middleName'    => isset($response->User->Middlename) ? $response->User->Middlename : null,
                'mail'          => isset($response->User->Contacts->Email) ? $response->User->Contacts->Email: null,
                'customerId'    => $contactId,
                'phone'         => isset($response->User->Contacts->MobilePhone) ? $response->User->Contacts->MobilePhone : null,
                'cardNumber'    => 0,
                'passportIndex' => isset($response->User->Contacts->AddressByPassport->Index) ? $response->User->Contacts->AddressByPassport->Index : null,
                'codeWord'      => isset($response->User->Security->CodeWord) ? $response->User->Security->CodeWord : null,
                'passportNumber'=> isset($response->User->Passport->Number) ? $response->User->Passport->Number : null,
                'contactId'     => $contactId,
                'sessionId'     => $sessionId,
                'fullInfo'      => isset($response->User) ? $response->User : null,
            );

            $response   = $this->call( 'UserGetCardToPay', array( 'contactId' => $contactId, 'sessionId' => $sessionId , 'evenFiktiv' => $evenFiktiv ), null, false );
            if( ! $this->checkError( $response ) )  {
                $result['error'] = $response->ErrorCode;
                return $result;
            }
            $fancard = $response->Fancard;
            if( ! empty( $fancard ) )   {
                $result['cardNumber']   = $fancard->CardNumber;
            }
            return $result;
        }

        public function canLogin( $login, $campaign ) {
            $response = $this->call( 'CanLogin', array( 'login' => (string) $login, 'campaign' => (string) $campaign ) );
            if(is_object($response) && $response->ErrorCode == 0)    {
                return true;
            }
            return ($response->ErrorCode <= 5) ? $response->ErrorCode : 5;
        }

        private function checkError( $response )    {
            if( ! is_object($response) || ! empty($response->ErrorMessage) )    {
                return false;
            }
            return true;
        }
    }