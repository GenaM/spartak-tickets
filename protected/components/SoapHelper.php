<?php
    class SoapHelper extends SoapClient    {

        private $_port;
        private $_urlParts;
        private $_options = array( 'connection_timeout' => 10, 'exceptions' => 1, 'keep_alive' => 1, 'trace' => 1, 'wsdl_cache' => WSDL_CACHE_NONE);

        protected $url;
        protected $options;

        protected $client;

        public $errorCode = 500;

        public function __construct()   {
            $this->urlParts = parse_url( $this->url );

            if( ! empty( $this->urlParts['port'] ) ) {
                $this->_port = $this->urlParts['port'];
            }

            if( $this->options ) {
                $this->_options = array_merge( $this->_options, $this->options );
            }

            try {
                @$this->client = new SoapClient( $this->url , $this->_options );
            } catch( Exception $e ) {
                if($_SERVER['REMOTE_ADDR'] == '54.246.91.77') {
                    var_dump($e, 123);
                } else {
                    throw new CHttpException($this->errorCode);
                }
            }
            $this->client->__setLocation( $this->buildLocation( $this->_urlParts ) );

        }

        public function __doRequest($request, $location, $action, $version, $one_way = true) {
            $parts = parse_url($location);
            if ($this->_port) {
                $parts['port'] = $this->_port;
            }
            $location = $this->buildLocation($parts);

            $return = parent::__doRequest($request, $location, $action, $version, $one_way);
            return $return;
        }

        public function buildLocation($parts = array()) {
            $location = '';

            if (isset($parts['scheme'])) {
                $location .= $parts['scheme'].'://';
            }
            if (isset($parts['user']) || isset($parts['pass'])) {
                $location .= $parts['user'].':'.$parts['pass'].'@';
            }
            $location .= $parts['host'];
            if (isset($parts['port'])) {
                $location .= ':'.$parts['port'];
            }
            $location .= $parts['path'];
            if (isset($parts['query'])) {
                $location .= '?'.$parts['query'];
            }
            return $location;
        }

        public function call( $function, $params = array(), $resultParam = null, $writeLogs = true ) {
            try {
                if($writeLogs) {
                    SpartakMainHelper::writeLog( 'Request:' . PHP_EOL . print_r( $params, true ), date('Y-m-d') . '_' . $function );
                }
                $result = $this->client->__soapCall( $function , array( $params ) )->{$resultParam ? $resultParam : ($function . 'Result')};
                if($writeLogs) {
                    SpartakMainHelper::writeLog( 'Response:' . PHP_EOL . print_r( $result, true ), date('Y-m-d') . '_' . $function );
                }
            } catch( Exception $e ) {
                SpartakMainHelper::writeLog(
                    'Error:' . PHP_EOL . print_r( $params, true ) . PHP_EOL .  $e,
                    date('Y-m-d') . '_' . $function
                );
                throw new CHttpException( $this->errorCode );
            }
            return $result;
        }

    }