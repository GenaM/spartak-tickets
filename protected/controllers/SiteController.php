<?php
    class SiteController extends Controller {

        public $layout = '//layouts/main';

        public function beforeAction( $action ) {
            if( in_array( $action->id, array( 'index', 'address', 'howToBuy', 'seasonTicket', 'login', 'logout' ) ) )   {
                $this->loadTickets = false;
            }

            return parent::beforeAction( $action );
        }

        public function actionIndex()   {
            $criteria = new CDbCriteria();
            $criteria->condition    = 'is_active = 1 AND start_time > :st';
            $criteria->params       = array(':st' => CSite::getNow());

            $events = Event::model()->findAll($criteria);
            $this->render('index', array('events' => $events));
        }

        public function actionLogin() {
            if (!Yii::app()->user->isGuest) {
                $this->redirect($this->createUrl('site/index'));
            }

            $model = new LoginForm;

            if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['LoginForm'])) {
                $model->attributes = $_POST['LoginForm'];
                if ($model->validate() && $model->login()) {
                    if (empty(Yii::app()->user->returnUrl) || Yii::app()->user->returnUrl == '/') {
                        $this->redirect($this->createUrl('site/index'));
                    } else {
                        $this->redirect(Yii::app()->user->returnUrl);
                    }
                }
            }
            $this->render('login', array('model' => $model));
        }

        public function actionLogout()  {
            unset( $this->session );
            Yii::app()->user->logout();
            $this->redirect(Yii::app()->homeUrl);
        }

        /**
         * @param $stadiumId
         * @param $eventId
         * @param $sectorId
         * @param $sectorName
         * @param $tribune
         * @param $rowName
         * @param $seatId
         * @param $seatName
         * @param $upper
         * @throws CHttpException
         */
        public function actionAddToBasket( $stadiumId , $eventId , $sectorId , $sectorName , $tribune , $rowName , $seatId , $seatName , $upper ) {
            $redirect = null;

            if( ! $event = SpartakMainHelper::checkEvent( $eventId ) )   {
                throw new CHttpException( 404 );
            }

            $message = '';

            $ticketsInBasketCheck = SpartakMainHelper::checkTicketsNumberInBasket( $event['type'], $event['event_id'] );

            if( $ticketsInBasketCheck === 1 ) {
                $message = 'Достигнуто максимальное количество билетов/абонементов в корзине.';
            } elseif( $ticketsInBasketCheck === 2 ) {
                $message = 'Вы больше не можете покупать билеты/абонементы на данное мероприятие.';
            } elseif( $ticketsInBasketCheck === 3 ) {
                $message = 'У вас есть не завершенная оплата.';
            } elseif( $ticketsInBasketCheck === 4 ) {
                $message = 'Вы не можете одновременно покупать билеты на разные мероприятия.';
            } else {
                $basket                 = new Basket();

                $basket->attributes     = array(
                    'user_id'               => Yii::app()->user->id,
                    'stadium_id'            => $stadiumId,
                    'event_id'              => $eventId,
                    'event_type'            => $event['type'],
                    'sector_id'             => $sectorId,
                    'sector_name'           => $sectorName,
                    'tribune_name'          => $tribune,
                    'row_name'              => $rowName ? $rowName : '-',
                    'seat_id'               => $seatId,
                    'seat_name'             => $seatName ? $seatName : '-',
                    'upper'                 => $upper,
                    'status'                => Basket::STATUS_ERROR,
                    'expiration_time'       => CSite::getNow(Basket::FIRST_STEP_RESERVATION_TIME),
                    'amount'                => 0,
                );

                if( $basket->save() ) {
                    if( $event['type'] == TicketSoftHelper::TYPE_EVENT )    {
                        $reservation = $this->tickets->ReserveSeat( $stadiumId , $eventId , $sectorId , $seatId , Yii::app()->user->cardNumber , Yii::app()->user->lastName , 3 . $basket->id );
                    } elseif( $event['type'] == TicketSoftHelper::TYPE_SEASON ) {
                        if( empty( Yii::app()->user->cardNumber ) || empty( Yii::app()->user->passportIndex )
                            || empty( Yii::app()->user->codeWord ) || empty( Yii::app()->user->passportNumber ) ) {
                            $message    = 'Пройдите полную регистрацию как "Болельщик" в <a href="http://lk.spartak.com/">личном кабинете</a>.'; // =|
                        } else {
                            $reservation = $this->tickets->ReserveSeasonTicketSeat( $stadiumId , $eventId , $sectorId , $seatId , Yii::app()->user->cardNumber , Yii::app()->user->lastName , 3 . $basket->id );
                        }
                    }

                    if( ! empty( $reservation ) ) {
                        $basket->attributes = array(
                            'reservation_result' => $reservation['result']
                        );
                        if( ( $reservation['result'] == 149 || $reservation['result'] == 150 ) ) {
                            $basket->attributes = array(
                                'status' => Basket::STATUS_LIMIT_ERROR,
                            );
                        } elseif( $reservation['result'] == 0 ) {
                            $basket->attributes = array(
                                'status'                => Basket::STATUS_NEW,
                                'reservation_id'        => $reservation['reservationid'],
                                'reservation_number'    => $reservation['reservationnumber'],
                                'amount'                => $reservation['seatsprice']
                            );
                        }
                    }

                    if( $basket->save() )   {
                        SpartakMainHelper::loadBasket();
                    }
                }
            }

            echo json_encode( array(
                'html'      => $this->widget( 'BasketWidget' , array( 'basketList' => $this->basket , 'eventList' => $this->events , 'message' => $message ) , true ) ,
                'redirect'  => $redirect
            ) );
            Yii::app()->end();
        }

        /**
         * @param $eventId
         * @param $seatId
         */
        public function actionRemoveFromBasket( $eventId , $seatId )    {
            $redirect = null;

            $basket = Basket::model()->findAllByAttributes( array(
                'user_id'   => Yii::app()->user->id,
                'event_id'  => $eventId,
                'seat_id'   => $seatId
            ) );

            if( ! empty( $basket ) )  {
                foreach( $basket as $item ) {
                    $expirationTime = new DateTime($item->expiration_time);
                    $now            = new DateTime(CSite::getNow());
                    if(! in_array($item->status, array(Basket::STATUS_TS_REMOVE_ERROR, Basket::STATUS_LIMIT_ERROR, Basket::STATUS_OVERDUE, Basket::STATUS_EVENT_MISSING, Basket::STATUS_NEW, Basket::STATUS_ERROR)) || ($item->status == Basket::STATUS_RESERVED && $expirationTime > $now)) {
                        continue;
                    }

                    if($item->reservation_number) {
                        $tsResponse = $this->tickets->ClearReservation( $item->stadium_id , Yii::app()->user->cardNumber , $item->reservation_number );
                    }

//                    if(! $item->reservation_number || $tsResponse) {
                        $itemId             = $item->id;
                        $item->attributes   = array('status' => Basket::STATUS_CANCELED); //set status
                        if (!$item->save()) {
                            $item->delete();
                        }

                        $basketOrders = BasketOrder::model()->findAllByAttributes(array('basket_id' => $itemId)); //search ordered
                        if ($basketOrders) {
                            foreach ($basketOrders as $basketOrder) {
                                $basketOrder->delete();
                            }
                        }
//                    } elseif(! $tsResponse) {
//                        $item->attributes   = array('status' => Basket::STATUS_TS_REMOVE_ERROR); //set status
//                        $item->save();
//                    }
                }
            }
            SpartakMainHelper::loadBasket();
            echo json_encode( array(
                'html'      => $this->widget( 'BasketWidget' , array( 'basketList' => $this->basket , 'eventList' => $this->events ) , true ) ,
                'redirect'  => $redirect
            ) );
            Yii::app()->end();
        }

        public function actionInfo()    {
            $this->render('info');
        }

        public function actionSeasonTicket()  {
            $this->render('season_ticket');
        }

        public function actionHowToBuy()  {
            $this->render('how_to_buy');
        }

        public function actionAddress() {
            $this->render('address');
        }

        public function actionWebPart() { //TODO temp
            $stadiums = $this->tickets->getStadiums();
            echo "OK";
        }

    }
