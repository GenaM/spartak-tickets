<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'TICKETS console',
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.components.widgets.*',
        'application.helpers.*',
        'ext.WkHtmlToPdf',
        'ext.yii-mail.YiiMailMessage',
    ),

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=tickets',
            'emulatePrepare' => true,
            'username' => 'tickets',
            'password' => '2VwrEGbExavNhzdw',
            'charset' => 'utf8',
        ),
//		'log'=>array(
//			'class'=>'CLogRouter',
//			'routes'=>array(
//                array(
//                    'class'=>'CFileLogRoute',
//                    'levels'=>'trace, info',
//                    'categories'=>'system.*',
//                ),
//			),
//        ),
        'session' => array (
            'autoStart' => false,
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'dryRun' => false,
        ),
    ),

    'params'=>array(
        'adminEmail'        => 'admin@spartak.com',
        'noreplyEmail'      => 'noreply@spartak.com', //from this mail all users receive mails
        'cashierEmail'      => array( 'tickets@stadionspartak.ru', 'gennadiy.mukhay@gmail.com' ), //for notifications
        'alarmEmail'        => array( 'fors_tickets@stadionspartak.ru', 'genam@yandex.ru' ), //on this email sends error messages about rapida check

        'rapida' => array(
            'payTime' => 540, //9 minutes
            'alarmTime' => 1440 //24 minutes
        ),

        'basket' => array(
            'eventsName'            => 'tickets_events',
            'maxTicketsInBasket'    => 2, //tickets for one event
        ),

        'events' => array(
            'beforeEventStartTime' => 1800, //tickets sale stops 30 min before event start
        ),

        'blocks' => array(
            'stadiumPage'       => false, //block stadium page
            'sectorPage'        => false, //block sector page
            'reservationPage'   => false, //block reservation page

            'useCanLogin'       => true,
        ),

    ),
);