<section class="error-info">
    <span class="error-info__title">Ошибка</span>

    <p class="error-info__text">
        <span class="error-info__1line">Извините</span>
        <span class="error-info__1line"></span>
        <span class="error-info__3line">Отсутствует связь с сервером билетной системы стадиона. Пожалуйста, попробуйте позже</span>
    </p>
</section>