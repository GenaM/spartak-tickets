<?php
    /**
     * @var $cs CClientScript
     */
    $cs = Yii::app()->getClientScript();
    $cs->coreScriptPosition = CClientScript::POS_END;
?>
<!DOCTYPE html>
<html class="page">
    <head>
        <title>Spartak Tickets | Билетная схема</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="author" content="Геннадий Мухай, Андрей Сыев, Елена Мусатова, Владимир Кичин, Клим Миронов"/>
        <!-- Проектные стили -->
        <?php $cs->registerCssFile( '/css/main.css' ) ?>

        <!-- JQuery библиотека -->
        <?php $cs->registerCoreScript( 'jquery' ) ?>
        <?php $cs->registerCoreScript( 'jquery-ui' ) ?>
        <?php Yii::app()->clientScript->registerScriptFile( '/js/jquery-placeholder.js' , CClientScript::POS_END ); ?>
        <?php Yii::app()->clientScript->registerScriptFile( '/plugins/selectBox/jquery.selectBox.js' , CClientScript::POS_END ) ?>
        <?php $cs->registerScriptFile( '/js/main.js' , CClientScript::POS_END ) ?>
    </head>

    <body class="page__body">
        <?php $this->renderPartial( '//layouts/_main/header' ); ?>
        <?php $this->renderPartial( '//layouts/_main/content' , array( 'content' => $content ) ); ?>
        <?php $this->renderPartial( '//layouts/_main/footer' ); ?>
    </body>
</html>