<?php
    class tsCommand extends Command {

        public $loadEvents = false;

        public function actionSaveEvents() {
            $events = $this->tickets->getEvents();
            foreach( $events as $key => $event )    {
                $dbEvent = Event::model()->findByAttributes( array( 'event_id' => $event['id'] ) );
                if( empty( $dbEvent ) ) {
                    $dbEvent = new Event();
                }

                $startTime = new DateTime( $event['starttime'] );
                $endTime = ! empty( $event['endtime'] ) ? new DateTime( $event['endtime'] ) : null;
                $dbEvent->attributes = array(
                    'event_id'              => $event['id'],
                    'name'                  => $event['name'],
                    'description'           => $event['description'],
                    'start_time'            => $startTime->format( 'Y-m-d H:i:s' ),
                    'end_time'              => $endTime ? $endTime->format( 'Y-m-d H:i:s' ) : null,
                    'personal'              => empty( $event['personal'] ) ? 0 : $event['personal'],
                    'is_active'             => false,
                    'is_reserving_enabled'  => empty( $event['isreservingenabled'] ) ? 0 : 1, //TODO remove this spike
                    'st_containers'         => empty( $event['stcontainers'] ) ? null : $event['stcontainers'],
                    'st_sectors'            => empty( $event['stsectors'] ) ? null : $event['stsectors'],
                    'type'                  => $event['type'],
                    'stadium_id'            => $event['stadiumId'],
                    'stadium_name'          => $event['stadiumName'],
                );

                if( ! $dbEvent->save() )    {
                    var_dump( $dbEvent->getErrors() );
                }
            }
        }

        public function actionSaveSectors($id = null) {
//            $events = Event::model()->findAll( 'end_time > :t1 OR end_time = :t2', array( ':t1' => CSite::getNow(), ':t2' => "0000-00-00 00:00:00" ) );
            if($id) {
                $events = array( Event::model()->findByPK($id) );
            } else {
                $events = Event::model()->findAll( 'is_active = 1' );
            }

            if( empty($events) ) {
                return null;
            }

            foreach( $events as $event ) {
                $startTime = new DateTime( $event['start_time'] );
                if( $startTime->getTimestamp() <= CSite::getNow() ) {
                    continue;
                }

                $sectors            = $this->tickets->getSectorList( $event['stadium_id'] , $event['event_id'] );
                $seatNumbers        = $this->tickets->getSectorsFreeSeats( $event['stadium_id'] , $event['event_id'] );
                $sectorsAvailable   = $this->tickets->getSectorsAvailability( $event['stadium_id'] , $event['event_id'] , $event['type'] == TicketSoftHelper::TYPE_SEASON ? 1 : 0 );
                foreach( $sectors as $sector ) {
                    $dbSector   = Sector::model()->findByAttributes( array( 'sector_id' => $sector['id'], 'event_id' => $event['event_id'] ) );
                    if( empty( $dbSector ) ) {
                        $dbSector = new Sector();
                    }

                    $available = false;
                    if( $event['type'] == TicketSoftHelper::TYPE_EVENT ) {
                        $available = empty( $sectorsAvailable[$sector['id']] ) ? false : $sectorsAvailable[$sector['id']];
                    } elseif( $event['type'] == TicketSoftHelper::TYPE_SEASON ) {
                        $available = empty( $event->st_sectors[$sector['id']] ) ? false : $event->st_sectors[$sector['id']];
                    }

                    $dbSector->attributes = array(
                        'stadium_id'            => $event['stadium_id'],
                        'event_id'              => $event['event_id'],
                        'sector_id'             => $sector['id'],
                        'name'                  => $sector['name'],
                        'description'           => $sector['description'],
                        'capacity'              => $sector['capacity'],
                        'tribune'               => $sector['tribune'],
                        'upper'                 => $sector['upper'],
                        'free_seats'            => empty( $seatNumbers[$sector['id']] ) ? 0 : $seatNumbers[$sector['id']],
                        'available'             => (int) $available
                    );

                    if( ! $dbSector->save() )    {
                        //var_dump( $dbSector->getErrors() );
                    }
                }
            }
        }

        public function actionSaveSeats() {
            $startTime  = time();

            $events = Event::model()->findAll( 'end_time > :t1 OR end_time = :t2', array( ':t1' => CSite::getNow(), ':t2' => "0000-00-00 00:00:00" ) );
            foreach( $events as $event ) {
                var_dump( 'Event ID: ' . $event->event_id );

                $cnt = 0;
                $sectors    = Sector::model()->findAll( 'event_id = :eId AND available = 1 AND free_seats > 0', array( ':eId' => $event['event_id'] ) );
                $insert     = array();
                $update     = array();
                foreach( $sectors as $sector ) {
                    if( $sector->scheme_saved == 0 ) {
                        $scheme = $this->tickets->getSectorScheme( $sector->stadium_id , $sector->event_id , $sector->sector_id , $event['type'] );
                        $cnt++;
                        foreach( $scheme as $seat ) {
                            $insert[$sector->sector_id][] = array(
                                'stadium_id'        => $sector->stadium_id,
                                'event_id'          => $sector->event_id,
                                'sector_id'         => $sector->sector_id,
                                'seat_id'           => $seat->id,
                                'row_text'          => $seat->rowText,
                                'seat_text'         => $seat->seatText,
                                'x'                 => $seat->x,
                                'y'                 => $seat->y,
                                'width'             => $seat->width,
                                'height'            => $seat->height,
                                'zone_id'           => $seat->zoneId,
                                'status'            => $seat->status,
                            );
                        }
                    } else {
                        $seatsStatus = $this->tickets->getSeatsStatus( $sector->stadium_id , $sector->event_id , $sector->sector_id );
                        $cnt++;
                        foreach( $seatsStatus as $seat ) {
                            $update[ $seat->status ][] = $seat->id;
                        }
                    }
                }

                var_dump( 'The processing TS transactions: ' . (time()-$startTime) );
                var_dump( 'Transactions number: ' . $cnt );


                $builder=Yii::app()->db->schema->commandBuilder;
                if( $insert ) {
                    foreach( $insert as $sectorId => $seats ) {
                        $saveSeatsCommand=$builder->createMultipleInsertCommand('seat', $seats);
                        if( $saveSeatsCommand->execute() ) {
                            Sector::model()->updateAll(array( 'scheme_saved' => 1 ), 'sector_id=:id', array( ':id'=>$sectorId ) );
                        } else {
                            var_dump( $saveSeatsCommand->errors );
                            die();
                        }
                    }
                }
                if( $update ) {
                    foreach( $update as $status => $ids ) {
                        Seat::model()->updateAll(array( 'status' => $status ), 'seat_id IN (:ids)', array( ':ids'=>implode( ',' , $ids ) ) );
                    }
                }
            }

            var_dump( 'The processing our optimized func: ' . (time()-$startTime) );
        }

        public function actionClearReservations($ignoreTS, $hours) {
            $dateStart = date('d/m/Y H:i:s');
            SpartakMainHelper::writeLog('Начало обработки: ' . $dateStart . '. Идет обработка ...', 'clearReservations.log', true);

            if($hours < 1) {
                $hours = 1;
            }
            if($hours > 47) {
                $hours = 47;
            }

            $statuses = array(Basket::STATUS_OVERDUE, Basket::STATUS_CANCELED);

            if($ignoreTS)   {
                $statuses = array(Basket::STATUS_OVERDUE, Basket::STATUS_EVENT_MISSING, Basket::STATUS_ERROR, Basket::STATUS_BARCODE_ERROR, Basket::STATUS_LIMIT_ERROR, Basket::STATUS_TS_ERROR, Basket::STATUS_TS_REMOVE_ERROR);
            }

            $criteria = new CDbCriteria();
            $criteria->addCondition('expiration_time < "' . CSite::getNow((75 * 60) - ($hours * 60 * 60)) . '"');
            $criteria->addCondition('expiration_time > "' . CSite::getNow(- (2 * 24 * 60 * 60)) . '"');
            $criteria->addInCondition('status', $statuses);
            $criteria->addCondition('reservation_number IS NOT NULL');
            $all = Basket::model()->count($criteria);
            $bad = 0;
            $iterations = ceil($all/1000);
            for($i = 1; $i <= $iterations; $i++) {
                $criteria->limit = 1000;
                $criteria->offset = 1000 * ($i - 1);
                $basket = Basket::model()->findAll($criteria);
                $bad = 0;
                foreach ($basket as $item) {
                    if (! $ignoreTS && $item->reservation_number && $item->user->card_number) {
                        $tsResponse = $this->tickets->ClearReservation($item->stadium_id, $item->user->card_number, $item->reservation_number);
                        if (! $tsResponse) {
                            $bad++;
                        }
                    } else {
                        $tsResponse = true;
                    }

                    if ($tsResponse) {
                        $item->attributes = array(
                            'status' => Basket::STATUS_CANCELED
                        );
                        if(! $item->save()) {
                            var_dump($item->errors);
                        }
                        $item->save();
                    }
                }
            }
            SpartakMainHelper::writeLog(
                'Начало обработки: ' . $dateStart . PHP_EOL .
                'Обнаружено и отправлено на удаление броней: ' . $all . ', успешно удалено ' . ($all - $bad) . ' броней' . PHP_EOL .
                'Обработка завершена: ' . date('d/m/Y H:i:s') . PHP_EOL,

                'clearReservations.log',
                true
            );
        }

    }