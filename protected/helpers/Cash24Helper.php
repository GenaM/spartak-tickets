<?php
    class Cash24Helper  {

        private static $apiUrl             = 'http://api.staging.cash24.ru/1.0/';
        private static $apiNamespace       = 'http://api.cash24.ru/1.0/';

        public static $shopID             = '55dd9d39-afa7-e411-9b22-00155d295802'; //TODO temp
        private static $secretKey          = 'InyfNJH48qpsU5Y7SuGSnH0wG/dk4cG0quGDFNhYWHzb9cpy1CnTnNzU7oDB71UlYKEjQw2SnpoJa8E99LuZSCCeOkk22rpLSSttPrlidB8+YEujoUawfWAf0qvQ/Rlp50whJI+N+ucNSNdvu7HJPswEBNMVpXzgn7Eldmisxy8=';
        private static $commandSecretKey   = 'mJCnt8ZFWdzU1giy5JfSFt+4dnPujbiyb85q3BuM6pgo0/2SaqvPiTM2ZWGy7bCTB7a8yFE7oduO4LozIihgf3HoOujd/PhczARz0Bb+gZE5S9SS/5PGqJnQQkd6SNOgSe/EA8p1yL82lv06RQzVgdqKn2+psCdpqyyvGr+y5pA=';

        public static function createInvoice($params) {
            self::createCommand('create-invoice', $params);
        }

        public static function processResponse($params) {

        }

        private static function createCommand($command, $params) {
            $xml        = self::prepareXML($command, $params);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,            self::$apiUrl );
            curl_setopt($ch, CURLOPT_POSTFIELDS,     $xml->asXML() );

            curl_setopt($ch, CURLOPT_FAILONERROR, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8'));
            $result=curl_exec($ch);
            var_dump(curl_getinfo($ch, CURLINFO_HEADER_OUT));
            curl_close($ch);
            var_dump($result);
        }

        private static function prepareXML($command, $params) {
            $cash24 = new SimpleXMLElement('<cash24 />');
            $cash24->addAttribute('xmlns', self::$apiNamespace);

            $request = $cash24->addChild('request');
            $request->addAttribute('xmlns', self::$apiNamespace . $command . '/');

            $request->addChild('amount', $params['amount']);
            $request->addChild('currency', $params['currency']);
            $request->addChild('email', $params['email']);
            $request->addChild('description', $params['description']);
            $request->addChild('order', $params['order']);
            $request->addChild('success', $params['success']);
            $request->addChild('cancel', $params['cancel']);
            $request->addChild('callback', $params['callback']);
            $request->addChild('phone', $params['phone']);
            $request->addChild('expires', $params['expires']);

            $envelope = $cash24->addChild('envelope');
            $envelope->addChild('auth', $params['auth']);
            $envelope->addChild('sign', self::generateEnvelopeSign($command, $params));

            return $cash24;
        }

        private static function saveRequest($command, $params) {

        }

        private static function saveResponse() {

        }

        /**
         * @param $action
         * @param array $params
         * @return string
         */
        private static function generateEnvelopeSign($action, array $params) {
            $str = $action . '-' .
                $params['amount'] . '-' .
                $params['currency'] . '-' .
                $params['email'] . '-' .
                $params['description'] . '-' .
                $params['order'] . '-' .
                $params['success'] . '-' .
                $params['cancel'] . '-' .
                $params['callback'] . '-' . '--' .
                self::$commandSecretKey;

            return md5( $str );
        }
    }