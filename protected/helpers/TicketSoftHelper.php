<?php
    class TicketSoftHelper extends SoapHelper  {

        protected $url = 'http://95.165.189.156/WebPartServer/services/WebPart?wsdl';
//        protected $url = 'http://95.165.189.150/WebPartServer/services/WebPart?wsdl';
        protected $options = array( 'soap_version' => SOAP_1_2 );

        public $errorCode = 451;

        private $contractId = 3031567;
        private $customerId = 3031562;

        const TYPE_EVENT    = 1;
        const TYPE_SEASON   = 2;

        /**
         * @return array
         * @throws CHttpException
         */
        public function getStadiums()   {
            $result     = array();
            $response   = $this->call('Cinemas');
            if( ! empty( $response->cinema ) )    {
                $cinemas = $response->cinema;
                if( ! is_array( $cinemas ) )    {
                    $cinemas = array( $cinemas );
                }
                foreach( $cinemas as $cinema )  {
                    $result[ $cinema->cinemaId ] = $cinema;
                }
            }
            return $result;
        }

        /**
         * @param string $dates
         * @param int $timeout
         * @return array
         * @throws CHttpException
         */
        public function getEvents( $dates = '' , $timeout = 1000 ) {
            $results    = array();
            $events     = array();
            $cinemas    = $this->getStadiums();
            foreach( $cinemas as $cinemaId => $cinema )  {
                $matches    = array();
                $seasons    = $this->getSeasonTickets( $cinemaId );
                $response   = $this->call( 'Performances', array( 'cinemaId' => $cinemaId , 'dates' => ''/*$dates ? $dates : date('d.m.Y') . '-' . date('d.m.Y', time() + (60*60*24*180))*/ , 'timeout' => $timeout ) );
                if( ! empty( $response->performance ) ) {
                    $matches = $response->performance;
                    if( ! is_array( $matches ) ) {
                        $matches = array( $matches );
                    }
                }
                $events[$cinemaId] = array_merge( $seasons , $matches );
            }
            if( ! empty( $events ) && ! empty( $cinemas ) ) {
                foreach( $cinemas as $cinemaId => $cinema )    {
                    if( empty( $events[$cinemaId] ) )   {
                        continue;
                    }
                    foreach( $events[$cinemaId] as $event ) {
                        if( empty( $event->stcontainers ) && ! $this->checkWebMarkerDiscount( $cinema->cinemaId , $event->id ) ) {
                            continue;
                        }
                        if( empty( $event->stcontainers ) ) {
                            $results[] = $this->fillEventResult( $event , $cinema );
                        } else {
                            $results[] = $this->fillSeasonTicketResult( $event , $cinema );
                        }
                    }
                }
            }
            return $results;
        }

        /**
         * @param $cinemaId
         * @param $eventId
         * @return bool
         */
        private function checkWebMarkerDiscount( $cinemaId , $eventId )   {
            $discounts = $this->call( 'PerformanceDiscounts', array( 'cinemaId' => $cinemaId , 'seanceId' => $eventId ) );
            if( empty( $discounts->discount ) )   {
                return false;
            }
            $discounts = $discounts->discount;
            if( ! is_array( $discounts ) )  {
                $discounts = array( $discounts );
            }
            foreach( $discounts as $discount )  {
                if( $discount->name == 'webmarker' )    {
                    return true;
                }
            }
            return false;
        }

        /**
         * @param $event
         * @param $cinema
         * @return array
         * @throws CHttpException
         */
        private function fillEventResult( $event , $cinema )  {
            $seance                 = $this->call( 'Seances', array( 'cinemaId' => $cinema->cinemaId , 'performanceIds' => $event->id , 'dates' => '' ) );
            $seance                 = (array) $seance->seance;
            $seance['info']         = $event->actors;
            $seance['stadiumId']    = $cinema->cinemaId;
            $seance['stadiumName']  = $cinema->name;
            $seance['type']         = TicketSoftHelper::TYPE_EVENT;
            return $seance;
        }

        /**
         * @param $event
         * @param $cinema
         * @return array
         */
        private function fillSeasonTicketResult( $event , $cinema ) {
            $event                  = (array) $event;
            $seance['info']         = $event['description'];
            $event['stadiumId']     = $cinema->cinemaId;
            $event['stadiumName']   = $cinema->name;
            $event['type']          = TicketSoftHelper::TYPE_SEASON;
            return $event;
        }

        /**
         * @param $cinemaId
         * @return array
         * @throws CHttpException
         */
        public function getSectorList( $cinemaId )    {
            $results  = array();
            $sections = $this->call( 'CinemaHalls', array( 'cinemaId' => $cinemaId ) );
            if( ! empty( $sections->hall ) ) {
                $sections = $sections->hall->hallvariances->hallvariance->stands->stand;
                if( ! is_array( $sections ) ) {
                    $sections = array( $sections );
                }
                foreach( $sections as $part )  {
                    $circles = is_array( $part->circles->circle ) ? $part->circles->circle : array( $part->circles->circle );
                    foreach( $circles as $isLower => $circle )   {
                        $sectors = is_array( $circle->sectors->sector ) ? $circle->sectors->sector : array( $circle->sectors->sector );
                        foreach( $sectors as $sector )  {
                            if( ! is_object( $sector ) )    {
                                continue;
                            }
                            $sector                                                 = (array) $sector;
                            $sector['tribune']                                      = $part->name;
                            $sector['upper']                                        = (int) ! $isLower;
                            $results[ $sector['id'] ]                               = $sector;
                        }
                    }
                }
            }
            return $results;
        }

        /**
         * @param $cinemaId
         * @param $eventId
         * @return array
         * @throws CHttpException
         */
        public function getSectorsAvailability( $cinemaId , $eventId , $forSeasonTicket = false )  {
            $results    = array();
            $response   = $this->call( 'PerformanceAvailability', array( 'cinemaId' => $cinemaId , 'seanceId' => $eventId , 'forSeasonTicket' => $forSeasonTicket ) );
            if( empty( $response->container3 ) ) {
                return $results;
            }
            $sectors = $response->container3;
            if( ! is_array( $sectors ) )    {
                $sectors = array( $sectors );
            }
            foreach( $sectors as $sector ) {
                $results[ $sector->id ] = $sector->canSelling && $sector->canReserving;
            }
            return $results;
        }

        /**
         * @param $cinemaId
         * @param $eventId
         * @return array
         * @throws CHttpException
         */
        public function getSectorsFreeSeats( $cinemaId , $eventId )   {
            $result     = array();
            $sectors   = $this->call( 'FreeSeatCount', array( 'cinemaId' => $cinemaId , 'actionId' => $eventId ) );
            if( empty( $sectors->container2 ) ) {
                return null;
            } else {
                $sectors = $sectors->container2;
            }
            if( ! is_array( $sectors ) )    {
                $sectors = array( $sectors );
            }
            foreach( $sectors as $sector )  {
                $result[ $sector->id ] = $sector->freeCount;
            }
            return $result;
        }

        /**
         * @param $cinemaId
         * @param $actionId
         * @param $containerId
         * @param $type
         * @return array
         * @throws CHttpException
         */
        public function getSectorScheme( $cinemaId , $actionId , $containerId , $type )   {
            $results   = array(); //TODO
            $sector    = $this->call( 'HallScheme', array( 'cinemaId' => $cinemaId , 'actionId' => $actionId , 'containerId' => $containerId ), null, false );
//            return $sector->seats->seat; //TODO for one-stream loading

            foreach( $sector->seats->seat as $seat )  {
                $results[ $seat->y ][ $seat->x ] = array( 'id' => $seat->id , 'name' => $seat->seatText , 'status' => $seat->status , 'zoneId' => $seat->zoneId , 'rowName' => $seat->rowText, 'x' => $seat->x, 'y' => $seat->y );
            }
            $results['seats']   = $this->sortSeatsPlaces( $results );
            $results['name']    = $sector->name;
            $results['id']      = $containerId;
            if( $type == TicketSoftHelper::TYPE_EVENT ) {
                $results['price']   = $this->getSectorPrice( $cinemaId , $actionId ,$containerId );
            } elseif( $type == TicketSoftHelper::TYPE_SEASON )  {
                $results['price']   = $this->getSeasonTicketPrice( $cinemaId , $actionId ,$containerId );
            }
            return $results;
        }

        /**
         * @param $cinemaId
         * @param $actionId
         * @param $containerId
         * @return mixed
         * @throws CHttpException
         */
        public function getSectorPrice( $cinemaId, $actionId, $containerId )    {
            $result     = array();
            $response   = $this->call( 'Prices', array( 'cinemaId' => $cinemaId , 'actionId' => $actionId , 'containerId' => $containerId ) );
            if( ! is_array( $response ) )   {
                $response = array( $response );
            }
            foreach( $response as $zone )   {
                if( empty( $zone->id ) ) {
                    continue;
                }
                $result[ $zone->id ] = $zone;
            }
            return $result;
        }

        /**
         * @param $cinemaId
         * @param $actionId
         * @param $containerId
         * @return mixed
         * @throws CHttpException
         */
        public function getSeasonTicketPrice( $cinemaId, $actionId, $containerId )    {
            $result     = array();
            $response   = $this->call( 'Prices', array( 'cinemaId' => $cinemaId , 'actionId' => $actionId , 'containerId' => $containerId ) );
            if( ! is_array( $response ) )   {
                $response = array( $response );
            }
            foreach( $response as $zone )   {
                if( empty( $zone->id ) ) {
                    continue;
                }
                $result[ $zone->id ] = $zone;
            }
            return $result;
        }

        /**
         * @param $cinemaId
         * @param $seanceId
         * @param $containerId
         * @param $seatId
         * @param $cardNumber
         * @param $lastName
         * @param $internalId
         * @return array
         * @throws CHttpException
         */
        public function ReserveSeat( $cinemaId , $seanceId , $containerId , $seatId , $cardNumber , $lastName , $internalId )   {
            return (array) $this->call( 'ReserveSeats3', array( 'cinemaId' => $cinemaId , 'cardNumber' => $cardNumber , 'seanceId' => $seanceId , 'containerId' => $containerId , 'seats' => $seatId , 'customerId' => $this->customerId , 'client' => $lastName , 'payed' => true , 'internalId' => $internalId ) );
        }

        /**
         * @param $cinemaId
         * @param $seanceId
         * @param $containerId
         * @param $seatId
         * @param $cardNumber
         * @param $lastName
         * @param $internalId
         * @return array
         * @throws CHttpException
         */
        public function ReserveSeasonTicketSeat(  $cinemaId , $seanceId , $containerId , $seatId , $cardNumber , $lastName , $internalId )   {
            return (array) $this->call( 'ReserveSeasontTicketSeats', array( 'cinemaId' => $cinemaId , 'cardNumber' => $cardNumber , 'seasonTicketId' => $seanceId , 'containerId' => $containerId , 'seats' => $seatId , 'customerId' => $this->customerId , 'client' => $lastName , 'internalId' => $internalId ) );
        }

        /**
         * @param $cinemaId
         * @param $reservationId
         * @return mixed
         * @throws CHttpException
         */
        public function ConfirmReservation( $cinemaId , $reservationId )    {
            return $this->call( 'ConfirmReservation', array( 'cinemaId' => $cinemaId , 'reservationId' => $reservationId ) );
        }

        /**
         * @param $cinemaId
         * @param $cardNumber
         * @param $reservationNumber
         * @return mixed
         * @throws CHttpException
         */
        public function ClearReservation( $cinemaId , $cardNumber , $reservationNumber )    {
            return $this->call( 'ClearReservedSeats', array( 'cinemaId' => $cinemaId , 'cardNumber' => $cardNumber , 'reservationNumber' => $reservationNumber ) );
        }

        /**
         * @param $cinemaId
         * @param $reservationId
         * @param $sum
         * @param $internalId
         * @return bool
         * @throws CHttpException
         */
        public function SetReservationPayed( $cinemaId , $reservationId , $sum , $internalId ) {
            return $this->call( 'ReservationPayed2', array( 'cinemaId' => $cinemaId , 'reservationId' => $reservationId , 'sum' => $sum , 'contractId' => $this->contractId , 'internalId' => $internalId ) );
        }

        /**
         * @param $cinemaId
         * @param $actionId
         * @param $containerId
         * @return array
         * @throws CHttpException
         */
        public function getSeatsStatus( $cinemaId , $actionId , $containerId )    {
            ini_set("default_socket_timeout", 10);
            $response = $this->call( 'HallSeatStatus', array( 'cinemaId' => $cinemaId , 'actionId' => $actionId , 'containerId' => $containerId ) );
            if( empty( $response->seat3 ) ) {
                return null;
            }
            return (array) $response->seat3;
        }

        /**
         * @param $cinemaId
         * @param $reservationNumber
         * @return array
         * @throws CHttpException
         */
        public function checkReservation( $cinemaId , $reservationNumber )  {
            return (array) $this->call( 'ReservationInfoByNumber', array( 'cinemaId' => $cinemaId , 'reservationNumber' => $reservationNumber ) );
        }

        /**
         * @param $cinemaId
         * @param $reservationId
         * @param $internalId
         * @return bool
         * @throws CHttpException
         */
        public function SellReservation( $cinemaId , $reservationId , $internalId )    {
            $response = $this->call( 'SellReservation', array( 'cinemaId' => $cinemaId , 'reservationId' => $reservationId , 'internalId' => $internalId ) );
            if( $response->result == 0 )  {
                return (array) $response->sellseats->sellseat;
            } else {
                return $response->result;
            }
        }

        /**
         * @param $cinemaId
         * @return array
         */
        public function getSeasonTickets( $cinemaId )   {
            $events = array();
            $response = $this->call( 'SeasonTickets', array('cinemaId' => $cinemaId ) );
            if( empty( $response->seasonticket ) )  {
                return array();
            }
            $response = $response->seasonticket;
            if( ! is_array( $response ) ) {
                $response = array( $response );
            }
            foreach( $response as $event )   {
                $result = array();
                $sectors = $event->stsectors->stsector;
                if( ! is_array( $sectors ) ) {
                    $sectors = array( $sectors );
                }
                foreach( $sectors as &$sector )  {
                    $result[ $sector->id ] = true;
                }
                $event->stsectors = $result;
                $events[] = $event;
            }
            return $events;
        }

        /**
         * @param $rows
         * @return array
         */
        private function sortSeatsPlaces( $rows )  {
            $result = array();
            foreach( $rows as $rowName => $row ) {
                ksort($row);
                $result[$rowName] = $row;
            }
            ksort( $result );
            return $result;
        }


    }