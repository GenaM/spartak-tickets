SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `tickets` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `tickets` ;

-- -----------------------------------------------------
-- Table `tickets`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tickets`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `crm_id` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tickets`.`basket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tickets`.`basket` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `stadium_id` INT NOT NULL,
  `event_id` INT NOT NULL,
  `sector_id` INT NOT NULL,
  `seat_id` INT NOT NULL,
  `reservation_id` INT NOT NULL,
  `sector_name` TEXT NOT NULL,
  `tribune_name` TEXT NOT NULL,
  `row_name` TEXT NOT NULL,
  `seat_name` TEXT NOT NULL,
  `amount` FLOAT NOT NULL DEFAULT 0,
  `upper` TINYINT(1) NOT NULL DEFAULT 0,
  `error` TINYINT(1) NOT NULL DEFAULT 0,
  `confirmed` TINYINT(1) NOT NULL DEFAULT 0,
  `payed` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_basket_user_idx` (`user_id` ASC),
  CONSTRAINT `fk_basket_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `tickets`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tickets`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tickets`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `amount` FLOAT NOT NULL,
  `status` INT NOT NULL DEFAULT 0,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_order_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `tickets`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tickets`.`basket_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tickets`.`basket_order` (
  `basket_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  INDEX `fk_basket_order_basket1_idx` (`basket_id` ASC),
  INDEX `fk_basket_order_order1_idx` (`order_id` ASC),
  CONSTRAINT `fk_basket_order_basket1`
    FOREIGN KEY (`basket_id`)
    REFERENCES `tickets`.`basket` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_basket_order_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `tickets`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
