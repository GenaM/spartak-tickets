  var sectorList          = {};
var sectorListIdName    = {};
var stCon               = null;

$(document).ready( function() {
    stCon = $('#stadium');
    getSectorList();
} );

function getSectorList()    {
    $.ajax( {
        url         : stCon.data('getlist'),
        type        : 'get',
        dataType    : 'json',
        success : function( r )    {
            sectorList = r;
            $.each( sectorList , function( i , val ) {
                sectorListIdName[ val.id ] = i;
            } );
            getAvailableSectors();
        }
    } );
}

function getAvailableSectors()  {
    $.ajax( {
        url         : stCon.data('getavailable'),
        type        : 'get',
        dataType    : 'json',
        success : function( r )    {
            $.each( r , function( i , val ) {
                if( sectorList[ sectorListIdName[ i ] ] ) {
                    sectorList[ sectorListIdName[ i ] ]['available'] = val;
                }
            } );
            drawStadium()
        }
    } );
}

function drawStadium()  {
    $.each( sectorList , function( i , val ) {
        var url = stCon.data('sectorurl').replace( '_sectorId_' , sectorList[ i ].id ).replace( '_upper_' , sectorList[ i ].upper ).replace( '_tribune_' , sectorList[ i ].tribune );
        var el  = {};
        if( val['available'] )  {
            el = $( '<a></a>').addClass('simple-sector').attr( 'href' , url).text( i );
        } else {
            el = $( '<span></span>').addClass('simple-sector').text( i );
        }
        stCon.append( el );
    } );
    $('#stadium .preloader').remove();
}