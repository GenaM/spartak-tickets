<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('username, password', 'required'),
			array('password', 'authenticate'),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			if(!$this->_identity->authenticate()) {
                if ($this->_identity->errorCode == UserIdentity::ERROR_CAN_LOGIN) {
                    $this->addError('username', Yii::t('app', 'Авторизация для данного логина не доступна.'));
                } elseif ($this->_identity->errorCode == UserIdentity::ERROR_SESSION_TIME_EXPIRED) {
                    $this->addError('username', Yii::t('app', 'Время вашей сессии истекло, пожалуйста повторите вход.'));
                } elseif ($this->_identity->errorCode == UserIdentity::ERROR_CONTACT_NOT_FOUND) {
                    $this->addError('username', Yii::t('app', 'Не найдено соответствующей учетной записи.'));
                } elseif ($this->_identity->errorCode == UserIdentity::ERROR_NO_ACTIVE_PASSWORD) {
                    $this->addError('username', Yii::t('app', 'Нет активного пароля.'));
                } elseif ($this->_identity->errorCode == UserIdentity::ERROR_PASSWORD_TRIES_LIMIT) {
                    $this->addError('username', Yii::t('app', 'Превышен лимит попыток авторизации.'));
                } elseif ($this->_identity->errorCode == UserIdentity::ERROR_PASSWORD_EXPIRES) {
                    $this->addError('username', Yii::t('app', 'Время действия постоянного пароля истекло.'));
                } elseif ($this->_identity->errorCode == UserIdentity::ERROR_USERNAME_BLOCKED) {
                    $this->addError('username', Yii::t('app', 'Данный аккаунт временно заблокирован.'));
                } else {
                    $this->addError('username', Yii::t('app', 'Неверный логин или пароль.'));
                }
            }
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
        if($this->_identity === null) {
			$this->_identity=new UserIdentity($this->username,$this->password);
            $this->_identity->authenticate();
        }
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE) {
            Yii::app()->user->allowAutoLogin = true;
			Yii::app()->user->login( $this->_identity , 3600 ); //1 h.
			return true;
		}
		else {
            return false;
        }
	}

    public function attributeLabels()
    {
        return array(
            'username' => 'Логин',
            'password' => 'Пароль',
        );
    }
}
