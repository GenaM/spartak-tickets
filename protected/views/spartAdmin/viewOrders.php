<?php if($data) { ?>
    <nav>
        <ul class="pagination">
            <?php for($i = 0; $i <= $pages - 1; $i++) { ?>
                <li class="<?php echo $i == $page ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('spartAdmin/' . $this->action->id, array('status' => ! empty($status) ? $status : '', 'page' => $i)); ?>"><?php echo $i ?></a></li>
            <?php } ?>
        </ul>
    </nav>
<?php } ?>


<table class="orders table table-hover">
    <?php if( $data ) { ?>
        <thead>
            <tr>
                <th>номер</th>
                <th></th>
                <th>статус</th>
                <th>Дата/время</th>
                <th>ФИО</th>
                <th>эл. адрес пользователя</th>
            </tr>
        </thead>
    <?php } ?>
    <tbody>
        <?php foreach( $data as $key => $item ) { ?>
            <tr data-id="<?php echo $key ?>">
                <td><?php echo $item->id ?></td>
                <td>
                    <?php if(!in_array($item->status, array(Order::STATUS_CANCELED, Order::STATUS_CANCELED_RAPIDA, Order::STATUS_NEW))) { ?>
                        <span class="btn btn-primary process" data-toggle="modal" data-target="#operation-started-modal" data-url="<?php echo Yii::app()->createUrl('spartAdmin/orderProcessor', array('id' => $item->id) ) ?>">"толкнуть"</span>
                    <?php } ?>
                </td>
                <td><?php echo $item->status ?></td>
                <td><?php echo $item->date ?></td>
                <td><?php echo $item->user->last_name . ' ' . $item->user->first_name . ' ' . $item->user->middle_name ?></td>
                <td><?php echo $item->user->email ?></td>
            </tr>
            <?php if( $item->basketOrders ) { ?>
                <tr id="<?php echo $key ?>" style="display: none;">
                    <td colspan="9" style="background:#fff5c6">
                        <table class="table">
                            <tr>
                                <th>#</th>
                                <th>ID меропиятия</th>
                                <th>Номер_TS</th>
                                <th>ID_TS</th>
                                <th>Трибуна</th>
                                <th>Сектор</th>
                                <th>Ряд</th>
                                <th>Место</th>
                                <th>ReserveSeats</th>
                                <th>SetReservationPayed</th>
                                <th>SellReservation</th>
                                <th>Статус</th>
                                <th>Сумма</th>
                            </tr>
                            <?php foreach( $item->basketOrders as $basketOrder ) { ?>
                                <?php if(empty($basketOrder->basket)) {continue;} ?>
                                <tr>
                                    <td style="border-radius: 0;"><?php echo $basketOrder->basket->id  ?></td>
                                    <td><?php echo $basketOrder->basket->event_id  ?></td>
                                    <td><?php echo $basketOrder->basket->reservation_number  ?></td>
                                    <td><?php echo $basketOrder->basket->reservation_id  ?></td>
                                    <td><?php echo $basketOrder->basket->tribune_name  ?></td>
                                    <td><?php echo $basketOrder->basket->sector_name  ?></td>
                                    <td><?php echo $basketOrder->basket->row_name  ?></td>
                                    <td><?php echo $basketOrder->basket->seat_name  ?></td>
                                    <td><?php echo $basketOrder->basket->reservation_result  ?></td>
                                    <td><?php echo $basketOrder->basket->set_reservation_payed  ?></td>
                                    <td><?php echo $basketOrder->basket->sell_reservation_result  ?></td>
                                    <td><?php echo $basketOrder->basket->status  ?></td>
                                    <td style="border-radius: 0;"><?php echo $basketOrder->basket->amount  ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    </tbody>
</table>

<?php if($data) { ?>
    <nav>
        <ul class="pagination">
            <?php for($i = 0; $i <= $pages - 1; $i++) { ?>
                <li class="<?php echo $i == $page ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('spartAdmin/' . $this->action->id, array('status' => ! empty($status) ? $status : '', 'page' => $i)); ?>"><?php echo $i ?></a></li>
            <?php } ?>
        </ul>
    </nav>
<?php } ?>

<?php if(! $data) { ?>
    <p>Ни одного заказа в данной категории</p>
<?php } ?>