<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'SPARTAK TICKETS',
    'language'=>'ru',
//	'preload'=>array('log'),
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.components.widgets.*',
		'application.helpers.*',
        'ext.WkHtmlToPdf',
        'ext.yii-mail.YiiMailMessage',
	),

	'modules'=>array(
	/*	'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1029384756',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
	                'ipFilters'=>array($_SERVER['REMOTE_ADDR']),
		),
	*/
	),

	'components'=>array(
		'user'=>array(
            'loginUrl'=>array('site/login'),
		),

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName' => false,
			'rules'=>array(
                'home'          => 'site/index',
                'login'         => 'site/login',
                'logout'        => 'site/logout',
                'info'          => 'site/info',
                'address'       => 'site/address',

                'stadium'       => 'tickets/stadium',
                'sector'        => 'tickets/sector',
                'reservation'   => 'tickets/reservation',
                'order'         => 'tickets/order',
                'payed'         => 'tickets/payed',
                'result'        => 'tickets/result',

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=tickets',
			'emulatePrepare' => true,
			'username' => 'tickets',
			'password' => '2VwrEGbExavNhzdw',
			'charset' => 'utf8',
		),
		'errorHandler'=>array(
			'errorAction'=>'/error/index',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'clientScript'  => array(
            'packages'    => array(
                'jquery'        => array(
                    'baseUrl'               => '/js/',
                    'js'                    => array( 'jquery-1.8.2.min.js' ),
                    'coreScriptPosition'    => CClientScript::POS_END,
                ),
                'jquery-ui'     => array(
                    'baseUrl'               => '/js/',
                    'js'                    => array( 'jquery-ui.js' ),
                    'coreScriptPosition'    => CClientScript::POS_END,
                    'depend'                => array( 'jquery' ),
                )
            ),
	    ),
        'session' => array (
            'autoStart' => false,
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'dryRun' => false,
        ),
	),

    'params'=>array(
        'adminEmail'        => 'admin@spartak.com',
        'noreplyEmail'      => 'noreply@spartak.com', //from this mail all users receive mails
        'cashierEmail'      => array( 'tickets@stadionspartak.ru', 'gennadiy.mukhay@gmail.com' ), //for notifications
        'alarmEmail'        => array( 'fors_tickets@stadionspartak.ru', 'genam@yandex.ru' ), //on this email sends error messages about rapida check

        'rapida' => array(
            'payTime' => 540, //9 minutes
            'alarmTime' => 1440 //24 minutes
        ),

        'cash24' => array(
            'successUrl'    => '',
            'cancelUrl'     => '',
            'callbackUrl'       => ''
        ),

        'basket' => array(
            'eventsName'            => 'tickets_events',
            'maxTicketsInBasket'    => 9999, //tickets for one event
        ),

        'events' => array(
            'beforeEventStartTime' => 1800, //tickets sale stops 30 min before event start
        ),

        'blocks' => array(
            'stadiumPage'       => false, //block stadium page
            'sectorPage'        => false, //block sector page
            'reservationPage'   => false, //block reservation page

            'useCanLogin'       => true,
        ),

        'banners'   => array(
            'big'   => array(
                'eventId'  => 4953474, //ID for event with big banner
                'img'       => '/i/banners/muse_spartak.jpg' //image link for big banner
            ),
            // 'little'    => array( //little banners, identical big
            //    array(
            //        'eventId'  => 4183540,
            //        'img'       => '/i/banners/main_ural_little.png'
            //    ),
                
                
         //    )
        )
    ),
);