<?php
class rapidaCommand extends Command
{
    public function actionCheckRapidaResponse() {
        $orders = Order::model()->findAll( new CDbCriteria( array(
            'condition' => 't.status = :status AND t.date < :date AND t.response_id IS NULL',
            'params'    => array( ':status' => Order::STATUS_NEW, ':date' => CSite::getNow( -Yii::app()->params['rapida']['payTime'] ) ),
        ) ) );
        if( ! $orders ) {
            Yii::app()->end();
        }

        foreach( $orders as $order ) {
            $rapidaRequest = RapidaRequest::model()->findByPK($order->request_id);
            $params = $rapidaRequest->attributes;
            unset($params['id']);

            SpartakMainHelper::writeLog(
                'oreder: ' . $order->id . PHP_EOL .
                'Request:' . PHP_EOL . print_r( $params, true ),
                date('Y-m-d') . '_' . 'RapidaCheck'
            );

            $response = (array)json_decode(RapidaHelper::checkTest($params));

            SpartakMainHelper::writeLog(
                'oreder: ' . $order->id . PHP_EOL .
                'Response:' . PHP_EOL . print_r( $response, true ),
                date('Y-m-d') . '_' . 'RapidaCheck'
            );

            if (!empty($response) && empty($response['ERROR'])) {
                RapidaHelper::processRapidaResponse($response, $this->tickets);
            } elseif(empty($response)) {
                $order->status = Order::STATUS_CANCELED_RAPIDA;
                $order->save();
            } else {
                echo 'Error. order ID' . $order->id . PHP_EOL ;
            }
        }
    }

    public function actionAlarm() {
        $orders = Order::model()->findAll( new CDbCriteria( array(
            'condition' => 't.status = :status AND t.date < :date AND t.response_id IS NULL AND alarmed = 0',
            'params'    => array( ':status' => Order::STATUS_NEW, ':date' => CSite::getNow( -Yii::app()->params['rapida']['alarmTime'] ) ),
        ) ) );

        if( ! $orders )   {
            Yii::app()->end();
        }

        if( Csite::sendMail( Yii::app()->params['alarmEmail'], $this->renderPartial( '/mails/alarm', array('orders' => $orders) ), 'От банка не пришел ответ!' ) ) {
            foreach( $orders as $order ) {
                $order->alarmed = 1;
                $order->save();
            }
        }
    }
}
